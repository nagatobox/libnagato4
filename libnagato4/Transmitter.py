

class HaruhiTransmitter:

    def transmit(self, user_data):
        for yuki_listener in self._listeners:
            yuki_listener.receive_transmission(user_data)

    def register_listener(self, object_):
        self._listeners.append(object_)

    def __init__(self):
        self._listeners = []
