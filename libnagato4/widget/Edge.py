
from libnagato4.Ux import Unit


class HaruhiEdge:

    EDGES = [
        ["NORTH_WEST", "NORTH", "NORTH_EAST"],
        ["WEST", "CENTER", "EAST"],
        ["SOUTH_WEST", "SOUTH", "SOUTH_EAST"]
        ]
    EDGE_START = Unit(2)
    EDGE_END = Unit(2)
    EDGE_TOP = Unit(2)
    EDGE_BOTTOM = Unit(2)

    def _get_index(self, position, positions):
        for yuki_index in range(2):
            if positions[yuki_index] > position:
                return yuki_index
        return 2

    def _get_x_index(self, position, limit):
        yuki_positions = [self.EDGE_START, limit-self.EDGE_END-1]
        return self._get_index(position, yuki_positions)

    def _get_y_index(self, position, limit):
        yuki_positions = [self.EDGE_TOP, limit-self.EDGE_BOTTOM-1]
        return self._get_index(position, yuki_positions)

    def _get_edge(self, widget, event):
        yuki_size, _ = widget.get_allocated_size()
        yuki_x_index = self._get_x_index(event.x, yuki_size.width)
        yuki_y_index = self._get_y_index(event.y, yuki_size.height)
        return self.EDGES[yuki_y_index][yuki_x_index]
