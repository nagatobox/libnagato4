
from gi.repository import Gtk
from libnagato4.widget.AnyBox import AbstractAnyBox


class AbstractVBox(AbstractAnyBox):

    ORIENTATION = Gtk.Orientation.VERTICAL
    HOMOGENEOUS = False
    EXPAND = True

    def _on_initialize_self(self):
        self.set_vexpand(self.EXPAND)

    def _on_initialize(self):
        raise NotImplementedError
