
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class AbstractAnyBox(Gtk.Box, NagatoObject):

    ORIENTATION = "Override This"
    HOMOGENEOUS = False

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_initialize_self(self):
        raise NotImplementedError

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=self.ORIENTATION)
        self.set_homogeneous(self.HOMOGENEOUS)
        self._on_initialize_self()
        self._on_initialize()
