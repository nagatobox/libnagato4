
from gi.repository import Gtk
from libnagato4.widget.AnyBox import AbstractAnyBox


class AbstractHBox(AbstractAnyBox):

    ORIENTATION = Gtk.Orientation.HORIZONTAL
    HOMOGENEOUS = False
    EXPAND = True

    def _on_initialize_self(self):
        self.set_hexpand(self.EXPAND)

    def _on_initialize(self):
        raise NotImplementedError
