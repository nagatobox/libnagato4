
class HaruhiUniqueId:

    _id = 0

    @classmethod
    def get_id(cls):
        cls._id += 1
        return cls._id
