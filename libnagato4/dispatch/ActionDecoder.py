

class HaruhiActionDecoder:

    def _decode(self, header, key):
        return "{}{}".format(header, key.replace(" ", "_"))

    def _split_arg(self, action):
        if isinstance(action, str):
            return action, None
        return action

    def decode(self, action):
        yuki_action, yuki_data = self._split_arg(action)
        if yuki_action not in self._dispatch:
            return None, None
        return self._dispatch[yuki_action], yuki_data

    def __init__(self, action_dispatch):
        self._dispatch = {}
        for yuki_key in action_dispatch.ACTIONS:
            yuki_method = self._decode(action_dispatch.HEADER, yuki_key)
            self._dispatch[yuki_key] = yuki_method
