
from gi.repository import Gtk
from gi.repository import Gdk
from libnagato4.Object import NagatoObject


class NagatoCssProvider(NagatoObject):

    def set_to_application(self):
        yuki_path = self._enquiry("YUKI.N > local file", "application.css")
        yuki_css_provider = Gtk.CssProvider()
        yuki_css_provider.load_from_path(yuki_path)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            yuki_css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    def __init__(self, parent):
        self._parent = parent
