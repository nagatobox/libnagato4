
from libnagato4.application.Yaml import NagatoYaml


class NagatoArgsYaml(NagatoYaml):

    RESOURCE = ".commandline_args.yaml"
    QUERY_MESSAGE = "YUKI.N > resources"

    def _parse_items(self):
        for yuki_item in self._yaml:
            self._raise("YUKI.N > add argument", yuki_item)

    def _on_initialize(self):
        if self._yaml is not None:
            self._parse_items()
