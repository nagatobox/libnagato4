
from libnagato4.application.Yaml import NagatoYaml
from libnagato4.application.SharedResource import HaruhiSharedResource
from libnagato4.application.LocalResource import NagatoLocalResource

SHARED_RESOURCE = "css_replacements.yaml"
LOCAL_RESOURCE = "additional_css_replacements.yaml"


class NagatoCssYaml(NagatoYaml):

    def _replace(self, css, item):
        yuki_query = "css", item["key"], item["default"]
        yuki_settings = self._enquiry("YUKI.N > settings", yuki_query)
        if yuki_settings == "NAGATO_NULL_SETTINGS":
            return css
        yuki_replacement = item["template"].format(yuki_settings)
        return css.replace(item["target"], yuki_replacement)

    def replace_all(self, css):
        for yuki_item in self._yaml:
            css = self._replace(css, yuki_item)
        return css

    def _get_data(self):
        yuki_shared = HaruhiSharedResource(SHARED_RESOURCE)
        yuki_local = NagatoLocalResource(self, LOCAL_RESOURCE)
        return yuki_shared.get_content()+yuki_local.get_content()
