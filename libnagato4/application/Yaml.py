
import yaml
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoYaml(NagatoObject):

    RESOURCE = "define resource NAME here."
    QUERY_MESSAGE = "define query message here."

    def __getitem__(self, key):
        if key not in self._yaml:
            return None
        return self._yaml[key]

    def _on_initialize(self):
        pass

    def _get_data(self):
        yuki_path = self._enquiry(self.QUERY_MESSAGE, self.RESOURCE)
        _, yuki_contents = GLib.file_get_contents(yuki_path)
        return yuki_contents.decode("utf-8")

    def __init__(self, parent):
        self._parent = parent
        self._yaml = yaml.safe_load(self._get_data())
        self._on_initialize()
