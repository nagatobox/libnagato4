
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagato4.application.KeyFile import HaruhiKeyFile
from libnagato4.application.ArgsLayer import NagatoArgsLayer


class NagatoSettingsLayer(NagatoObject):

    def _yuki_n_register_settings_object(self, object_):
        self._transmitter.register_listener(object_)

    def _yuki_n_settings(self, user_data):
        self._key_file.set(*user_data)
        self._transmitter.transmit(user_data)

    def _inform_settings(self, user_data):
        return self._key_file.get(*user_data)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        yuki_path = self._enquiry("YUKI.N > local file", "application.conf")
        self._key_file = HaruhiKeyFile(yuki_path)
        NagatoArgsLayer(self)
        self._key_file.save_to_file(yuki_path)
