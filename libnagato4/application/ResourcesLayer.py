
from gi.repository import GdkPixbuf
from libnagato4.Object import NagatoObject
from libnagato4.application.Path import NagatoPath
from libnagato4.application.ApplicationYaml import NagatoApplicationYaml
from libnagato4.application.LocalFilesLayer import NagatoLocalFilesLayer


class NagatoResourcesLayer(NagatoObject):

    def _inform_resources(self, name):
        return self._path.get_absolute_from_name(name)

    def _inform_data(self, key):
        return self._yaml[key]

    def _inform_pixbuf(self, name):
        yuki_path = self._path.get_absolute_from_name(name)
        return GdkPixbuf.Pixbuf.new_from_file(yuki_path)

    def __init__(self, parent):
        self._parent = parent
        self._path = NagatoPath(self)
        self._yaml = NagatoApplicationYaml(self)
        NagatoLocalFilesLayer(self)
