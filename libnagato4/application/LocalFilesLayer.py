
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.application.SettingsLayer import NagatoSettingsLayer
from libnagato4.file.copy.Recursive import NagatoRecursive


class NagatoLocalFilesLayer(NagatoObject):

    def _inform_local_file(self, name):
        return GLib.build_filenamev([self._local_directory, name])

    def _ensure(self):
        yuki_source_directory = self._enquiry("YUKI.N > resources", "")
        yuki_id = self._enquiry("YUKI.N > data", "id")
        yuki_config_dir = GLib.get_user_config_dir()
        yuki_names = [yuki_config_dir, yuki_id]
        self._local_directory = GLib.build_filenamev(yuki_names)
        NagatoRecursive(yuki_source_directory, self._local_directory)

    def __init__(self, parent):
        self._parent = parent
        self._ensure()
        NagatoSettingsLayer(self)
