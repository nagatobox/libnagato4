
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoPath(NagatoObject):

    def get_absolute_from_name(self, name):
        return GLib.build_filenamev([self._resources_directory, name])

    def __init__(self, parent):
        self._parent = parent
        yuki_library_directory = self._enquiry("YUKI.N > library directory")
        yuki_names = [yuki_library_directory, "resources"]
        self._resources_directory = GLib.build_filenamev(yuki_names)
