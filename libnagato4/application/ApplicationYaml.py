
from libnagato4.application.Yaml import NagatoYaml


class NagatoApplicationYaml(NagatoYaml):

    RESOURCE = ".application.yaml"
    QUERY_MESSAGE = "YUKI.N > resources"
