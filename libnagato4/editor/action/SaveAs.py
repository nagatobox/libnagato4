
from libnagato4.Object import NagatoObject
from gi.repository import Gtk
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagato4.chooser.context.SaveFileAs import NagatoContext


class NagatoSaveAs(NagatoObject):

    def _get_context(self, page):
        yuki_file = page.request_information("YUKI.N > file location")
        return NagatoContext.new_for_gio_file(self, yuki_file)

    def try_save_as(self):
        yuki_page = self._enquiry("YUKI.N > current page")
        yuki_context = self._get_context(yuki_page)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return False
        yuki_gio_file = yuki_context.get_gio_file()
        yuki_page.raise_message("YUKI.N > file location", yuki_gio_file)
        return True

    def __init__(self, parent):
        self._parent = parent
