
from libnagato4.dispatch.ActionDispatch import NagatoActionDispatch
from libnagato4.editor.action.Open import NagatoOpen


class NagatoNotebookActions(NagatoActionDispatch):

    ACTIONS = ["new", "open", "open file"]

    def _open(self):
        self._action_open.open()

    def _open_file(self, path):
        self._action_open.open_path(path)

    def _new(self):
        self._raise("YUKI.N > file loaded", None)

    def _on_initialize(self):
        self._action_open = NagatoOpen(self)
