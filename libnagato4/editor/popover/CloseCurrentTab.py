
from libnagato4.popover.item.Item import NagatoItem


class NagatoCloseCurrentTab(NagatoItem):

    TITLE = "Close Current Tab"
    MESSAGE = "YUKI.N > action"
    USER_DATA = "close current tab"

    def _on_initialize(self):
        self.props.tooltip_text = "Ctrl+W"
