
from libnagato4.popover.item.IconItem import AbstractIconItem


class NagatoCopyIcon(AbstractIconItem):

    ICON_NAME = "edit-copy-symbolic"
    MESSAGE = "YUKI.N > copy"

    def _on_initialize(self):
        self.set_hexpand(True)
        self.props.tooltip_text = "Copy Ctrl+C"
