
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToRecent(NagatoToStack):

    TITLE = "Recent"
    WHERE_TO_GO = "recent"
