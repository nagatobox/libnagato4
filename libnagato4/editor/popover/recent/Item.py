
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.file import HomeDirectory


class NagatoItem(Gtk.Button, NagatoObject):

    @classmethod
    def new_for_path(cls, parent, path):
        yuki_button = cls(parent)
        yuki_button.set_path(path)

    def _on_clicked(self, button, path):
        self._raise("YUKI.N > action", ("open file", path))
        self._raise("YUKI.N > popdown")

    def set_path(self, path):
        yuki_label = GLib.filename_display_basename(path)
        self.set_label(yuki_label)
        self.props.tooltip_text = HomeDirectory.shorten(path)
        self.connect("clicked", self._on_clicked, path)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        self._raise("YUKI.N > css", (self, "popover-button"))
        self._raise("YUKI.N > add to box", self)
