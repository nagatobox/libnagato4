
from libnagato4.widget.HBox import AbstractHBox
from libnagato4.editor.popover.UndoIcon import NagatoUndoIcon
from libnagato4.editor.popover.RedoIcon import NagatoRedoIcon
from libnagato4.editor.popover.CutIcon import NagatoCutIcon
from libnagato4.editor.popover.CopyIcon import NagatoCopyIcon
from libnagato4.editor.popover.PasteIcon import NagatoPasteIcon
from libnagato4.popover.Separator import NagatoSeparator


class NagatoEditBox(AbstractHBox):

    def _on_initialize(self):
        NagatoUndoIcon(self)
        NagatoRedoIcon(self)
        NagatoSeparator(self)
        NagatoCutIcon(self)
        NagatoCopyIcon(self)
        NagatoPasteIcon(self)
        self._raise("YUKI.N > add to box", self)
