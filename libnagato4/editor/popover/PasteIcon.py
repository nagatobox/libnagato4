
from libnagato4.popover.item.IconItem import AbstractIconItem


class NagatoPasteIcon(AbstractIconItem):

    ICON_NAME = "edit-paste-symbolic"
    MESSAGE = "YUKI.N > paste"

    def _on_initialize(self):
        self.set_hexpand(True)
        self.props.tooltip_text = "Paste Ctrl+V"
