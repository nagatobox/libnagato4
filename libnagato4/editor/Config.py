
from libnagato4.Object import NagatoObject


class AbstractConfig(NagatoObject):

    def append(self, page):
        for yuki_config in self._configs:
            yuki_config.set_current_value(page)

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        self._configs = []
        self._on_initialize()
