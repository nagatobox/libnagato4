
from libnagato4.Object import NagatoObject
from libnagato4.dispatch.SettingsDispatch import HaruhiSettigsDispatch


class NagatoHighlightBrackets(NagatoObject, HaruhiSettigsDispatch):

    SETTING_GROUP = "editor"
    SETTING_KEY = "highlight_matching_brackets"
    SETTING_DEFAULT = False

    def _on_settings_changed(self, value):
        for yuki_page in self._enquiry("YUKI.N > pages"):
            self.set_current_value(yuki_page, value)

    def set_current_value(self, page, value=None):
        if value is None:
            value = self._get_current_settings()
        page.buffer.set_highlight_matching_brackets(value)

    def __init__(self, parent):
        self._parent = parent
        self._bind_settings_dispatch()
