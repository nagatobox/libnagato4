
from libnagato4.editor.config.Config import AbstractConfig


class NagatoIndentWidth(AbstractConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "indent_width"
    SETTING_DEFAULT = 4
    METHOD = "set_indent_width"
