
from libnagato4.editor.config.Config import AbstractConfig


class NagatoHighlightCurrent(AbstractConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "highlight_current_line"
    SETTING_DEFAULT = True
    METHOD = "set_highlight_current_line"
