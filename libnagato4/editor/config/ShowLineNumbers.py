
from libnagato4.editor.config.Config import AbstractConfig


class NagatoShowLineNmbers(AbstractConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "show_line_numbers"
    SETTING_DEFAULT = True
    METHOD = "set_show_line_numbers"
