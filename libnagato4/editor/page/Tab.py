
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from gi.repository import Pango
from libnagato4.editor.page.fileoperation.Dispatch import HaruhiDispatch


class NagatoTab(Gtk.Label, NagatoObject, HaruhiDispatch):

    def _load_finished(self, path):
        self.set_label(GLib.filename_display_basename(path))
        self.props.tooltip_text = path

    def _save_finished(self, path):
        self.set_label(GLib.filename_display_basename(path))
        self.props.tooltip_text = path

    def _progress(self, user_data):
        self.set_label(user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, "Untitled")
        self.set_hexpand(True)
        self.set_ellipsize(Pango.EllipsizeMode.END)
        self.props.tooltip_text = "Untitled"
        self._raise("YUKI.N > register file operation object", self)
