
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.Tab import NagatoTab
from libnagato4.editor.page.PageBox import NagatoPageBox


class NagatoWidgets(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_tab = NagatoTab(self)
        yuki_page_box = NagatoPageBox(self)
        self._raise("YUKI.N > insert page", (yuki_page_box, yuki_tab))
