
TRAILINGS = " ", "\t"


class HaruhiRemoveSpaces:

    def _get_end_iter(self, buffer_, line_index):
        yuki_end_iter = buffer_.get_iter_at_line(line_index)
        yuki_end_iter.forward_to_line_end()
        return yuki_end_iter

    def _get_start_iter(self, end_iter):
        yuki_start_iter = end_iter.copy()
        while yuki_start_iter.backward_char():
            if not yuki_start_iter.get_char() in TRAILINGS:
                yuki_start_iter.forward_char()
                break
        return yuki_start_iter

    def remove(self, buffer_):
        for yuki_line_index in range(buffer_.get_line_count()-1, -1, -1):
            yuki_end_iter = self._get_end_iter(buffer_, yuki_line_index)
            yuki_start_iter = self._get_start_iter(yuki_end_iter)
            buffer_.delete(yuki_start_iter, yuki_end_iter)
