
from gi.repository import GLib


class HaruhiFileOperator:

    METHOD = ""

    def _on_progress(self, progress, length, user_data):
        pass

    def _on_finished(self, file_loader, gio_task, user_data=None):
        pass

    def _get_operation_args(self):
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_source_file = self._enquiry("YUKI.N > source file")
        return yuki_buffer, yuki_source_file

    def _do_async_operation(self, operator):
        yuki_method = getattr(operator, self.METHOD)
        yuki_method(
            GLib.PRIORITY_DEFAULT, None,
            self._on_progress, (None,),
            self._on_finished, None
            )
