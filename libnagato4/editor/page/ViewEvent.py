
from libnagato4.Object import NagatoObject
from libnagato4.editor.popover.Popover import NagatoPopover


class NagatoViewEvent(NagatoObject):

    def _on_button_press(self, view, event):
        if event.button != 3:
            return
        self._popover.popup_for_event(view, event)
        return True

    def _on_key_press(self, view, event):
        if event.keyval == 65383:
            return True

    def __init__(self, parent):
        self._parent = parent
        self._popover = NagatoPopover(self)
        yuki_view = self._enquiry("YUKI.N > view")
        yuki_view.connect("button-press-event", self._on_button_press)
        yuki_view.connect("key-press-event", self._on_key_press)
