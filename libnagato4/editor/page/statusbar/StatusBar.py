
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.statusbar.Label import NagatoLabel
from libnagato4.dispatch.SettingsDispatch import HaruhiSettigsDispatch


class NagatoStatusBar(Gtk.Revealer, NagatoObject, HaruhiSettigsDispatch):

    SETTING_GROUP = "editor"
    SETTING_KEY = "show_status_bar"
    SETTING_DEFAULT = False

    def _yuki_n_add_to_revealer(self, widget):
        self.add(widget)

    def set_data(self, user_data):
        self._label.set_data(user_data)

    def _on_settings_changed(self, value):
        self.set_reveal_child(value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self._bind_settings_dispatch()
        self.set_reveal_child(self._get_current_settings())
        self._label = NagatoLabel(self)
        self._raise("YUKI.N > add to box", self)
