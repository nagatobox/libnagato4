
from libnagato4.editor.page.DestinationFile import NagatoDestinationFile
from libnagato4.editor.page.FileSaver import NagatoFileSaver


class AsakuraSaver:

    def try_save(self):
        if not self._destination_file.try_set_location():
            return False
        self._file_saver.save()
        return True

    def __init__(self, parent):
        self._file_saver = NagatoFileSaver(parent)
        self._destination_file = NagatoDestinationFile(parent)
