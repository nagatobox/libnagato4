
from libnagato4.Object import NagatoObject


class NagatoRegistrationPoint(NagatoObject):

    def raise_message(self, message, user_data=None):
        self._raise(message, user_data)

    def request_information(self, message, user_data=None):
        return self._enquiry(message, user_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register page", self)

    @property
    def view(self):
        return self._enquiry("YUKI.N > view")

    @property
    def buffer(self):
        return self._enquiry("YUKI.N > buffer")
