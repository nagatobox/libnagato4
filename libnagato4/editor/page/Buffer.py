
from gi.repository import GtkSource
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.RemoveTrailing import NagatoRemoveTrailing
from libnagato4.editor.page.ClipboardLayer import NagatoClipboardLayer


class NagatoBuffer(GtkSource.Buffer, NagatoObject):

    def _yuki_n_about_to_save(self):
        self._remove_trailings.remove()

    def _inform_buffer(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        GtkSource.Buffer.__init__(self)
        self._remove_trailings = NagatoRemoveTrailing(self)
        NagatoClipboardLayer(self)
