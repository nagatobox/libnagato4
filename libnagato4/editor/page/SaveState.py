
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.fileoperation.Dispatch import HaruhiDispatch


class NagatoSaveState(NagatoObject, HaruhiDispatch):

    def _on_changed(self, buffer_):
        self._saved = False

    def _load_finished(self, user_data):
        self._saved = True

    def _save_finished(self, user_data):
        self._saved = True

    def get_saved(self):
        return self._saved

    def __init__(self, parent):
        self._parent = parent
        self._saved = True
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_buffer.connect("changed", self._on_changed)
        self._bind_dispatch()
