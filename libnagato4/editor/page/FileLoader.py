
from gi.repository import GtkSource
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.FileOperator import HaruhiFileOperator


class NagatoFileLoader(NagatoObject, HaruhiFileOperator):

    METHOD = "load_async"

    def _on_progress(self, progress, length, user_data):
        yuki_rate = progress/length
        if yuki_rate >= 1:
            return
        yuki_text = "{0:.0%}".format(yuki_rate)
        self._raise("YUKI.N > file operation progress", yuki_text)

    def _on_finished(self, file_loader, gio_task, user_data=None):
        if file_loader.load_finish(gio_task):
            yuki_gio_file = file_loader.get_location()
            self._raise("YUKI.N > load finished", yuki_gio_file.get_path())

    def __init__(self, parent):
        self._parent = parent
        yuki_args = self._get_operation_args()
        yuki_loader = GtkSource.FileLoader.new(*yuki_args)
        self._do_async_operation(yuki_loader)
