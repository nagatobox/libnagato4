
from gi.repository import GtkSource
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.fileoperation.Dispatch import HaruhiDispatch
from libnagato4.editor.page.LoaderLayer import NagatoLoaderLayer


class NagatoLanguage(NagatoObject, HaruhiDispatch):

    def _load_finished(self, path):
        yuki_language = self._language_manager.guess_language(path)
        if yuki_language is None:
            return
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        yuki_buffer.set_language(yuki_language)

    def __init__(self, parent):
        self._parent = parent
        self._language_manager = GtkSource.LanguageManager.get_default()
        self._bind_dispatch()
        NagatoLoaderLayer(self)
