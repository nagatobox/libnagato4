
from gi.repository import GtkSource
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.UniqueIdLayer import NagatoUniqueIdLayer


class NagatoPage(GtkSource.File, NagatoObject):

    def _yuki_n_file_location(self, gio_file):
        self.set_location(gio_file)

    def _inform_source_file(self):
        return self

    def _inform_file_location(self):
        return self.get_location()

    def _inform_has_file_location(self):
        return self.get_location() is not None

    def __init__(self, parent, gio_file=None):
        self._parent = parent
        GtkSource.File.__init__(self)
        self.set_location(gio_file)
        NagatoUniqueIdLayer(self)
