
from gi.repository import GtkSource
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.FileOperator import HaruhiFileOperator


class NagatoFileSaver(NagatoObject, HaruhiFileOperator):

    METHOD = "save_async"

    def _on_progress(self, progress, length, user_data):
        yuki_rate = progress/length
        if 1 > yuki_rate:
            yuki_text = "{0:.0%}".format(yuki_rate)
            self._raise("YUKI.N > file operation progress", yuki_text)

    def _on_finished(self, file_loader, gio_task, user_data=None):
        if file_loader.save_finish(gio_task):
            yuki_gio_file = file_loader.get_location()
            self._raise("YUKI.N > save finished", yuki_gio_file.get_path())

    def save(self):
        yuki_args = self._get_operation_args()
        yuki_file_saver = GtkSource.FileSaver.new(*yuki_args)
        self._raise("YUKI.N > about to save")
        self._do_async_operation(yuki_file_saver)

    def __init__(self, parent):
        self._parent = parent
