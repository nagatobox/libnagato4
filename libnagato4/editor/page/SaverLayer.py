
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.ClosabilityLayer import NagatoClosabilityLayer
from libnagato4.editor.page.Saver import AsakuraSaver


class NagatoSaverLayer(NagatoObject):

    def _yuki_n_save(self):
        return self._saver.try_save()

    def __init__(self, parent):
        self._parent = parent
        self._saver = AsakuraSaver(self)
        NagatoClosabilityLayer(self)
