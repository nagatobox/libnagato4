
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.Closability import AsakuraClosability
from libnagato4.editor.page.CloserLayer import NagatoCloserLayer


class NagatoClosabilityLayer(NagatoObject):

    def _inform_unsaved_until(self):
        return self._closability.get_unsaved_until()

    def _inform_tab_closable(self):
        return self._closability.get_is_closable()

    def __init__(self, parent):
        self._parent = parent
        self._closability = AsakuraClosability(self)
        NagatoCloserLayer(self)
