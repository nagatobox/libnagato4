
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.Buffer import NagatoBuffer
from libnagato4.widget.UniqueId import HaruhiUniqueId


class NagatoUniqueIdLayer(NagatoObject):

    def _inform_page_id(self):
        return self._id

    def __init__(self, parent):
        self._parent = parent
        self._id = HaruhiUniqueId.get_id()
        NagatoBuffer(self)
