
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.Widgets import NagatoWidgets
from libnagato4.editor.closetab.CloseTab import NagatoCloseTab


class NagatoCloserLayer(NagatoObject):

    def _yuki_n_closing_confirmed(self):
        yuki_id = self._enquiry("YUKI.N > page id")
        self._raise("YUKI.N > remove page", yuki_id)

    def _yuki_n_try_close_tab(self):
        self._close_tab.try_close_tab()

    def __init__(self, parent):
        self._parent = parent
        self._close_tab = NagatoCloseTab(self)
        NagatoWidgets(self)
