
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.FileOperationLayer import NagatoFileOperationLayer


class NagatoUndoLayer(NagatoObject):

    def _yuki_n_undo(self):
        return self._buffer.undo()

    def _yuki_n_redo(self):
        return self._buffer.redo()

    def __init__(self, parent):
        self._parent = parent
        self._buffer = self._enquiry("YUKI.N > buffer")
        NagatoFileOperationLayer(self)
