

DISPATCH = {
    "load finished": "_load_finished",
    "save finished": "_save_finished",
    "progress": "_progress"
    }


class HaruhiDispatch:

    def receive_transmission(self, user_data):
        yuki_type, yuki_data = user_data
        if DISPATCH[yuki_type] in dir(self):
            yuki_method = getattr(self, DISPATCH[yuki_type])
            yuki_method(yuki_data)

    def _bind_dispatch(self):
        self._raise("YUKI.N > register file operation object", self)
