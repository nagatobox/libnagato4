
from libnagato4.Object import NagatoObject
from libnagato4.editor.notebook.PageInserter import NagatoPageInserter
from libnagato4.editor.notebook.PageRemover import NagatoPageRemover
from libnagato4.editor.notebook.FilesLayer import NagatoFilesLayer


class NagatoPagesLayer(NagatoObject):

    def _yuki_n_insert_page(self, user_data):
        self._inserter.insert_at_most_left(user_data)

    def _yuki_n_remove_page(self, target_id):
        self._remover.remove_for_page_id(target_id)
        self._raise("YUKI.N > remove page for id", target_id)
        if self._enquiry("YUKI.N > number of pages") == 0:
            self._raise("YUKI.N > force quit application")

    def __init__(self, parent):
        self._parent = parent
        self._inserter = NagatoPageInserter(self)
        self._remover = NagatoPageRemover(self)
        NagatoFilesLayer(self)
