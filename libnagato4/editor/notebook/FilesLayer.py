
from libnagato4.Object import NagatoObject
from libnagato4.editor.notebook.Files import NagatoFiles
from libnagato4.editor.notebook.Actions import AsakuraActions
from libnagato4.editor.page.Page import NagatoPage


class NagatoFilesLayer(NagatoObject):

    def _yuki_n_file_loaded(self, gio_file=None):
        NagatoPage(self, gio_file)

    def __init__(self, parent):
        self._parent = parent
        NagatoFiles(self)
        AsakuraActions(self)
