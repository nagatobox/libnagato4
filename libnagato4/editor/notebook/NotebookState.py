
from gi.repository import Gtk


class HaruhiNotebookState:

    def __init__(self, notebook):
        self._notebook = notebook
        notebook.set_hexpand(True)
        notebook.set_vexpand(True)
        notebook.set_scrollable(True)
        notebook.set_property("expand", True)
        notebook.set_property("resize-mode", Gtk.ResizeMode.QUEUE)
