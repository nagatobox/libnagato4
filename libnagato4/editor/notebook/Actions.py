
from libnagato4.editor.action.PageActions import NagatoPageActions
from libnagato4.editor.action.NotebookActions import NagatoNotebookActions


class AsakuraActions:

    def __init__(self, parent):
        NagatoNotebookActions(parent)
        NagatoPageActions(parent)
