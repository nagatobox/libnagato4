
from libnagato4.Object import NagatoObject


class NagatoPageInserter(NagatoObject):

    def insert_at_most_left(self, user_data):
        yuki_content_area, yuki_tab = user_data
        yuki_notebook = self._enquiry("YUKI.N > notebook")
        yuki_notebook.insert_page(yuki_content_area, yuki_tab, -1)
        yuki_notebook.set_tab_reorderable(yuki_content_area, True)
        yuki_notebook.show_all()
        yuki_notebook.set_current_page(-1)

    def __init__(self, parent):
        self._parent = parent
