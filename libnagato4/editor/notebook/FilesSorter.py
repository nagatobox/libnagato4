
from libnagato4.Object import NagatoObject
from libnagato4.file.queue import Priority

MESSAGE = {
    Priority.UNSUPPORTED: "_yuki_n_sorted_to_unsupported",
    Priority.READ_ONLY: "_yuki_n_sorted_to_read_only",
    Priority.WRITABLE: "_yuki_n_sorted_to_writable"
    }


class NagatoFilesSorter(NagatoObject):

    def sort_to_message(self):
        while True:
            yuki_data = self._enquiry("YUKI.N > arg file")
            if yuki_data is None:
                break
            yuki_priority, yuki_gio_file = yuki_data
            self._raise(MESSAGE[yuki_priority], yuki_gio_file)

    def __init__(self, parent):
        self._parent = parent
