
from libnagato4.Object import NagatoObject
from libnagato4.editor.notebook.FilesSorter import NagatoFilesSorter


class NagatoFiles(NagatoObject):

    def _yuki_n_sorted_to_unsupported(self, gio_file):
        pass

    def _yuki_n_sorted_to_read_only(self, gio_file):
        print("read only", gio_file)

    def _yuki_n_sorted_to_writable(self, gio_file):
        self._raise("YUKI.N > file loaded", gio_file)

    def __init__(self, parent):
        self._parent = parent
        yuki_files_sorter = NagatoFilesSorter(self)
        yuki_files_sorter.sort_to_message()
        if self._enquiry("YUKI.N > number of pages") == 0:
            self._raise("YUKI.N > file loaded", None)
