
from libnagato4.Object import NagatoObject
from libnagato4.editor.closetab.Unsaved import NagatoUnsaved
from libnagato4.dialog.saveuntitled.SaveUntitled import NagatoSaveUntitled


class NagatoUntitled(NagatoObject):

    def _yuki_n_dialog_response_save(self):
        if self._raise("YUKI.N > save"):
            self._raise("YUKI.N > closing confirmed")

    def _yuki_n_dialog_response_discard(self):
        self._raise("YUKI.N > closing confirmed")

    def try_save(self):
        if self._enquiry("YUKI.N > has file location"):
            self._unsaved.try_save()
        else:
            NagatoSaveUntitled.get_response_as_message(self)

    def __init__(self, parent):
        self._parent = parent
        self._unsaved = NagatoUnsaved(self)
