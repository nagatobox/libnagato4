
from libnagato4.Object import NagatoObject
from libnagato4.dialog.saveunsaved.SaveUnsaved import NagatoSaveUnsaved


class NagatoUnsaved(NagatoObject):

    def _yuki_n_dialog_response_save(self):
        if self._raise("YUKI.N > save"):
            self._raise("YUKI.N > closing confirmed")

    def _yuki_n_dialog_response_discard(self):
        self._raise("YUKI.N > closing confirmed")

    def try_save(self):
        NagatoSaveUnsaved.get_response_as_message(self)

    def __init__(self, parent):
        self._parent = parent
