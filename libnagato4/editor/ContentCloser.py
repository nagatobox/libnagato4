
from libnagato4.Object import NagatoObject
from libnagato4.dialog.contentsnotsaved.Dialog import NagatoDialog


class NagatoContentCloser(NagatoObject):

    def closable_all(self):
        for yuki_page in self._enquiry("YUKI.N > pages"):
            if not yuki_page.request_information("YUKI.N > tab closable"):
                return False
        return True

    def _yuki_n_dialog_response_save(self):
        for yuki_page in self._enquiry("YUKI.N > pages"):
            yuki_page.raise_message("YUKI.N > save")

    def _yuki_n_dialog_response_discard(self):
        self._raise("YUKI.N > force quit application")

    def try_close_all(self):
        NagatoDialog.get_response_as_message(self)
        return True

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > loopback set content closer", self)
