
from libnagato4.editor.Config import AbstractConfig
from libnagato4.editor.config.ShowLineNumbers import NagatoShowLineNmbers
from libnagato4.editor.config.ShowRightMargin import NagatoShowRightMargin
from libnagato4.editor.config.AutoIndent import NagatoAutoIndent
from libnagato4.editor.config.HighlightCurrent import NagatoHighlightCurrent
from libnagato4.editor.config.SmartBackspace import NagatoSmartBackspace
from libnagato4.editor.config.SpacesAsTabs import NagatoSpacesAsTabs
from libnagato4.editor.config.WrapMode import NagatoWrapMode
from libnagato4.editor.config.IndentWidth import NagatoIndentWidth
from libnagato4.editor.config.RightMargin import NagatoRightMargin


class NagatoViewConfig(AbstractConfig):

    def _on_initialize(self):
        self._configs.append(NagatoShowLineNmbers(self))
        self._configs.append(NagatoShowRightMargin(self))
        self._configs.append(NagatoAutoIndent(self))
        self._configs.append(NagatoHighlightCurrent(self))
        self._configs.append(NagatoSmartBackspace(self))
        self._configs.append(NagatoSpacesAsTabs(self))
        self._configs.append(NagatoWrapMode(self))
        self._configs.append(NagatoIndentWidth(self))
        self._configs.append(NagatoRightMargin(self))
