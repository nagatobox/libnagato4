
from gi.repository import GLib
from libnagato4.header.label.TimeOut import NagatoTimeOut


class NagatoInfo(NagatoTimeOut):

    INTERVAL = 100
    VALID_TYPES = ["info"]

    def _get_cursor(self, message):
        if len(message) >= self._count:
            return "_"
        return "_" if (self._count % 8) >= 4 else ""

    def _timeout(self, message):
        yuki_substring = GLib.utf8_substring(message, 0, self._count)
        yuki_cursor = self._get_cursor(message)
        self._raise("YUKI.N > text", "YUKI.N > "+yuki_substring+yuki_cursor)
        self._count += 1
        yuki_ongoing = (len(message)+40 > self._count)
        if not yuki_ongoing:
            self._raise("YUKI.N > finished")
        return yuki_ongoing
