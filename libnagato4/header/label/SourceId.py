
from gi.repository import GLib


class HaruhiSourceId:

    def _try_remove_source(self, id_):
        if id_ == self._id:
            return
        if self._id != -1:
            GLib.Source.remove(self._id)

    def switch_to(self, id_):
        self._try_remove_source(id_)
        self._id = id_

    def remove_current(self):
        self._try_remove_source(-1)
        self._id = -1

    def __init__(self):
        self._id = -1  # impossible number for default.
