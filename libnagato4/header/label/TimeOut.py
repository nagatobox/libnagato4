
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoTimeOut(NagatoObject):

    INTERVAL = 200
    VALID_TYPES = ["foo", "bar", "baz"]

    def _timeout(self, message):
        raise NotImplementedError

    def _start(self, message="YUKI.N > ..."):
        self._count = 0
        yuki_id = GLib.timeout_add(self.INTERVAL, self._timeout, message)
        self._raise("YUKI.N > started", yuki_id)

    def receive_transmission(self, user_data):
        yuki_type, yuki_message = user_data
        if yuki_type in self.VALID_TYPES:
            self._start(yuki_message)

    def __init__(self, parent):
        self._parent = parent
        self._count = 0
        self._raise("YUKI.N > register header notifier object", self)
