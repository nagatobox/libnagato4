
from gi.repository import Gdk
from libnagato4.widget.LeftButton import NagatoLeftButton
from libnagato4.Ux import Unit

EDGE = Gdk.WindowEdge


class NagatoLeftButton(NagatoLeftButton):

    EDGES = [
        [EDGE.NORTH_WEST, EDGE.NORTH, None],
        [EDGE.WEST, None, None],
        [EDGE.WEST, None, None]
        ]
    EDGE_START = Unit(1)
    EDGE_END = Unit(4)
    EDGE_TOP = Unit(1)
    EDGE_BOTTOM = Unit(1)
