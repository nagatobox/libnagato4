
from libnagato4.widget.Cursor import NagatoCursor
from libnagato4.Ux import Unit


class NagatoCursor(NagatoCursor):

    EDGES = [
        ["nw-resize", "n-resize", "default"],
        ["w-resize", "default", "default"],
        ["w-resize", "default", "default"]
        ]
    EDGE_START = Unit(1)
    EDGE_END = Unit(4)
    EDGE_TOP = Unit(1)
    EDGE_BOTTOM = Unit(1)
