
from gi.repository import Gdk
from libnagato4.Object import NagatoObject

FULLSCREEN = Gdk.WindowState.FULLSCREEN


class NagatoWindowState(NagatoObject):

    def _set_revealed_for_state(self, state):
        yuki_fullscreen = (FULLSCREEN == state.new_window_state & FULLSCREEN)
        if yuki_fullscreen:
            self._raise("YUKI.N > switch stack to", ("invisible", 0))
        if not yuki_fullscreen:
            self._raise("YUKI.N > restore")

    def _on_window_state(self, window, state):
        if FULLSCREEN == state.changed_mask & FULLSCREEN:
            self._set_revealed_for_state(state)
            return True

    def _on_realize(self, widget):
        yuki_toplevel = widget.get_toplevel()
        yuki_toplevel.connect("window-state-event", self._on_window_state)

    def __init__(self, parent):
        self._parent = parent
        yuki_revealer = self._enquiry("YUKI.N > stack")
        yuki_revealer.connect("realize", self._on_realize)
