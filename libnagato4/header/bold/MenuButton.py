
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin


class NagatoMenuButton(Gtk.Button, NagatoObject):

    NAME = "open-menu-symbolic"
    SIZE = Gtk.IconSize.BUTTON

    def _on_clicked(self, button):
        self._popover.set_relative_to(self)
        self._popover.show_all()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        yuki_image = Gtk.Image.new_from_icon_name(self.NAME, self.SIZE)
        self.set_image(yuki_image)
        self.set_can_focus(False)
        self._popover = self._enquiry("YUKI.N > main popover")
        self._raise("YUKI.N > css", (self, "popover-button"))
        self.connect("clicked", self._on_clicked)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self._raise("YUKI.N > add to box", self)
