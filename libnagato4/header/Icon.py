
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin
from libnagato4.dispatch.SettingsDispatch import HaruhiSettigsDispatch


class AbstractIcon(Gtk.Image, NagatoObject, HaruhiSettigsDispatch):

    SETTING_GROUP = "header"
    SETTING_KEY = "show_icon"
    SETTING_DEFAULT = True
    ICON_SIZE = "define icon size here"

    def _on_settings_changed(self, show_icon):
        if show_icon:
            yuki_icon_name = self._enquiry("YUKI.N > data", "icon-name")
            self.set_from_icon_name(yuki_icon_name, self.ICON_SIZE)
        else:
            self.set_from_pixbuf(None)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self._raise("YUKI.N > add to box", self)
        self._bind_settings_dispatch()
