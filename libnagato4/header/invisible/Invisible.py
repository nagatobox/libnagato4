
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoInvisible(Gtk.EventBox, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventBox.__init__(self)
        self._raise("YUKI.N > add named", (self, "invisible"))
