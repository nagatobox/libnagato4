
from gi.repository import Gtk
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagato4.header.thin.Cursor import NagatoCursor
from libnagato4.header.thin.LeftButton import NagatoLeftButton
from libnagato4.header.thin.DummyLabel import NagatoDummyLabel


class NagatoThin(Gtk.EventBox, NagatoObject):

    def _yuki_n_add_to_event_box(self, widget):
        self.add(widget)

    def _inform_event_source(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventBox.__init__(self)
        self.add_events(Gdk.EventMask.POINTER_MOTION_MASK)
        NagatoCursor(self)
        NagatoLeftButton(self)
        NagatoDummyLabel(self)
        self._raise("YUKI.N > add named", (self, "thin"))
