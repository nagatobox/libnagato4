
from libnagato4.dialog.Label import AbstractLabel
from libnagato4.dialog.contentsnotsaved.Paths import NagatoPaths

MARKUP = ""\
    "<span size='large' underline='single'>WARNING !!</span>\n"\
    "\n"\
    "Following file(s) NOT SAVED\n"\
    "\n"\
    "{}"\
    "\n"\
    "Select 'Start Saving' to start saving sequence.\n"


class NagatoLabel(AbstractLabel):

    CSS = "dialog-warning"

    def _get_markup(self):
        yuki_paths = NagatoPaths(self)
        return MARKUP.format(yuki_paths.get_formated())
