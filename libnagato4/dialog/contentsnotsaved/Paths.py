
from libnagato4.Object import NagatoObject
from libnagato4.file import HomeDirectory


class NagatoPaths(NagatoObject):

    def _get_name(self, page):
        yuki_source_file = page.request_information("YUKI.N > source file")
        yuki_file = yuki_source_file.get_location()
        if yuki_file is None:
            return "Untitled"
        else:
            return HomeDirectory.shorten(yuki_file.get_path())

    def get_formated(self):
        yuki_paths = ""
        for yuki_page in self._enquiry("YUKI.N > pages"):
            if yuki_page.request_information("YUKI.N > tab closable"):
                continue
            yuki_path = self._get_name(yuki_page)
            yuki_paths += yuki_path+"\n"
        return yuki_paths

    def __init__(self, parent):
        self._parent = parent
