
from gi.repository import Gtk
from libnagato4.dialog.Image import AbstractImage


class NagatoImage(AbstractImage):

    CSS = "dialog-warning"

    def _set_image(self):
        self.set_from_icon_name(
            "dialog-error-symbolic",
            Gtk.IconSize.DIALOG
            )
