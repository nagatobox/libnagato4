
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit


class AbstractLabel(Gtk.Label, NagatoObject):

    def _get_markup(self):
        return None

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_justify(Gtk.Justification.CENTER)
        self.set_vexpand(True)
        self.set_padding(Unit(4), Unit(4))
        self.set_margin_top(0)
        self.set_margin_bottom(Unit(2))
        self.set_margin_start(Unit(2))
        self.set_margin_end(Unit(2))
        self.set_markup(self._get_markup())
        self._raise("YUKI.N > add to dialog", self)
