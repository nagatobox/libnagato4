
from libnagato4.Ux import Unit
from libnagato4.dialog.Dialog import AbstractDialog
from libnagato4.dialog.about.Image import NagatoImage
from libnagato4.dialog.about.Label import NagatoLabel
from libnagato4.dialog.about.ButtonBox import NagatoButtonBox


class NagatoAbout(AbstractDialog):

    SIZE = Unit(45), Unit(75)
    CSS = "dialog-label-about"

    def _yuki_n_loopback_add_content(self, parent):
        NagatoImage(parent)
        NagatoLabel(parent)
        NagatoButtonBox(parent)
