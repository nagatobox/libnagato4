
from gi.repository import Gtk
from libnagato4.dialog.Image import AbstractImage


class NagatoImage(AbstractImage):

    def _set_image(self):
        yuki_icon_name = self._enquiry("YUKI.N > data", "icon-name")
        self.set_from_icon_name(yuki_icon_name, Gtk.IconSize.DIALOG)
