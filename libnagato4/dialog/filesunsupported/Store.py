
from libnagato4.Object import NagatoObject
from libnagato4.dialog.filesunsupported.Dialog import NagatoDialog


class NagatoStore(NagatoObject):

    def _inform_files(self):
        return self._list

    def show_dialog(self):
        if self._list:
            NagatoDialog.get_response(self)

    def append(self, gio_file):
        self._list.append(gio_file)

    def __init__(self, parent):
        self._parent = parent
        self._list = []
