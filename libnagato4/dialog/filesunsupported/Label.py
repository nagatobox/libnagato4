
from libnagato4.dialog.Label import AbstractLabel
from libnagato4.file import HomeDirectory

MARKUP = ""\
    "<span size='large' underline='single'>ERROR !!</span>\n"\
    "\n"\
    "{} CAN NOT HANDLE following file(s).\n"\
    "\n"\
    "{}"


class NagatoLabel(AbstractLabel):

    def _get_markup(self):
        yuki_application_name = self._enquiry("YUKI.N > data", "name")
        yuki_names = ""
        for yuki_file in self._enquiry("YUKI.N > files"):
            yuki_name = "{}\n".format(yuki_file.get_path())
            yuki_names += HomeDirectory.shorten(yuki_name)
        return MARKUP.format(yuki_application_name, yuki_names)
