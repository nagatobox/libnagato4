
from libnagato4.dialog.Dialog import AbstractDialog
from libnagato4.dialog.filesnotfound.Image import NagatoImage
from libnagato4.dialog.filesunsupported.Label import NagatoLabel
from libnagato4.dialog.filesnotfound.ButtonBox import NagatoButtonBox


class NagatoDialog(AbstractDialog):

    CSS = "dialog-error"

    def _yuki_n_loopback_add_content(self, parent):
        NagatoImage(parent)
        NagatoLabel(parent)
        NagatoButtonBox(parent)
