
from libnagato4.dialog.Label import AbstractLabel

MARKUP = ""\
    "<span size='large' underline='single'>WARNING</span>\n"\
    "\n"\
    "Do you really want to close {} ?\n"


class NagatoLabel(AbstractLabel):

    CSS = "dialog-warning"

    def _get_markup(self):
        yuki_name = self._enquiry("YUKI.N > data", "name")
        return MARKUP.format(yuki_name)
