
from libnagato4.dialog.Dialog import AbstractDialog
from libnagato4.dialog.warning.Image import NagatoImage
from libnagato4.dialog.quit.Label import NagatoLabel
from libnagato4.dialog.quit.ButtonBox import NagatoButtonBox


class NagatoQuit(AbstractDialog):

    CSS = "dialog-warning"

    def _yuki_n_loopback_add_content(self, parent):
        NagatoImage(parent)
        NagatoLabel(parent)
        NagatoButtonBox(parent)
