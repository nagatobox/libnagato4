
from gi.repository import Gtk
from libnagato4.dialog.Button import AbstractButton


class NagatoCloseButton(AbstractButton):

    RESPONSE = Gtk.ResponseType.CLOSE
    TITLE = "Close Application"
    CSS = "button-warning"
