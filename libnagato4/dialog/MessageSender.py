
from libnagato4.Object import NagatoObject
from libnagato4.dialog import ResponseType

MESSAGE = {
    ResponseType.SAVE: "_yuki_n_dialog_response_save",
    ResponseType.DISCARD: "_yuki_n_dialog_response_discard",
    }


class NagatoMessageSender(NagatoObject):

    def send_message(self, response):
        if response in MESSAGE:
            self._raise(MESSAGE[response])

    def __init__(self, parent):
        self._parent = parent
