
from libnagato4.dialog.Button import AbstractButton
from libnagato4.dialog import ResponseType


class NagatoCloseButton(AbstractButton):

    RESPONSE = ResponseType.DISCARD
    TITLE = "Close Without Save"
    CSS = "button-warning"
