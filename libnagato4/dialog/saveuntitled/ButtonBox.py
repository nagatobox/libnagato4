
from libnagato4.dialog.ButtonBox import AbstractButtonBox
from libnagato4.dialog.quit.CancelButton import NagatoCancelButton
from libnagato4.dialog.saveuntitled.SaveButton import NagatoSaveButton
from libnagato4.dialog.saveuntitled.CloseButton import NagatoCloseButton


class NagatoButtonBox(AbstractButtonBox):

    def _on_initialize(self):
        NagatoCancelButton(self)
        NagatoSaveButton(self)
        NagatoCloseButton(self)
