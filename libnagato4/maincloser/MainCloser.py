
from libnagato4.Object import NagatoObject
from libnagato4.maincloser.MainLoopCloser import NagatoMainLoopCloser


class AbstractMainCloser(NagatoObject):

    def _yuki_n_force_quit_application(self):
        self._main_loop_closer.force_close()

    def _standard_sequence(self):
        return self._main_loop_closer.close_for_dialog_respose()

    def _on_delete(self, widget, event):
        return self._standard_sequence()

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        self._main_loop_closer = NagatoMainLoopCloser(self)
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_window.connect("delete-event", self._on_delete)
        self._on_initialize()
