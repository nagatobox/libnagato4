
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.header.Header import NagatoHeader


class AbstractHeaderLayer(Gtk.Box, NagatoObject):

    def _yuki_n_add_extra_header_widget(self, parent):
        pass

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        NagatoHeader(self)
        self._on_initialize()
        self._raise("YUKI.N > add to window", self)
