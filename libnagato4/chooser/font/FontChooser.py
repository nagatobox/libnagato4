
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin


class NagatoFontChooser(Gtk.FontChooserWidget, NagatoObject):

    def _on_font_activated(self, chooser, font_name):
        self._raise("YUKI.N > response", Gtk.ResponseType.APPLY)

    def set_selection(self):
        yuki_font_desc = self.get_font_desc()
        self._raise("YUKI.N > selection changed", [yuki_font_desc])

    def __init__(self, parent):
        self._parent = parent
        Gtk.FontChooserWidget.__init__(self)
        Margin.set_with_unit(self, 2, 2, 2, 2)
        self._raise("YUKI.N > add to content area", self)
        self.connect("font-activated", self._on_font_activated)
        self._raise("YUKI.N > selection changed", [])
