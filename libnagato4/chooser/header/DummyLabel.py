
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoDummyLabel(Gtk.Label, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_label = self._enquiry("YUKI.N > chooser config", "title")
        Gtk.Label.__init__(self, yuki_label)
        self.set_hexpand(True)
        self._raise("YUKI.N > add to box", self)
