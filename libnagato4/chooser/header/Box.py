
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.header.DummyLabel import NagatoDummyLabel
from libnagato4.chooser.header.CancelButton import NagatoCancelButton
from libnagato4.chooser.header.SelectButton import NagatoSelectButton


class NagatoBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        NagatoCancelButton(self)
        NagatoDummyLabel(self)
        NagatoSelectButton(self)
        self._raise("YUKI.N > css", (self, "chooser-header"))
        self._raise("YUKI.N > add to event box", self)
