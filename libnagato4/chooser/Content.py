
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagato4.chooser.Widgets import NagatoWidgets


class NagatoContent(NagatoObject):

    def _yuki_n_register_selection_object(self, object_):
        self._transmitter.register_listener(object_)

    def _yuki_n_selection_changed(self, selection):
        self._context.select(selection)
        self._transmitter.transmit("selection-changed")

    def _inform_chooser_config(self, key):
        return self._context[key]

    def __init__(self, parent, context):
        self._parent = parent
        self._context = context
        self._transmitter = HaruhiTransmitter()
        NagatoWidgets(self)
