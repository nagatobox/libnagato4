
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.Attributes import HaruhiAttributes
from libnagato4.chooser.Content import NagatoContent


class AbstractDialog(Gtk.Dialog, NagatoObject):

    def _yuki_n_loopback_add_chooser_widget(self, parent):
        raise NotImplementedError

    def _yuki_n_response(self, response):
        self.response(response)

    def _yuki_n_add_to_content_area(self, widget):
        yuki_content_area = self.get_content_area()
        yuki_content_area.add(widget)

    def get_response(self, context):
        NagatoContent(self, context)
        self.show_all()
        return self.run()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Dialog.__init__(self, "", Gtk.Window(), Gtk.ResponseType.CANCEL)
        HaruhiAttributes(self)
