
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.filter.Filters import NagatoFilters
from libnagato4.chooser.selectfile.ui.Paned import NagatoPaned


class NagatoFilterModelLayer(NagatoObject):

    def _visible_func(self, model, tree_iter, user_data=None):
        return self._filters.matches(model[tree_iter])

    def _inform_filter_model(self):
        return self._model

    def _inform_show_hidden(self):
        return self._show_hidden

    def _yuki_n_toggle_show_hidden(self):
        self._show_hidden = not self._show_hidden
        self._model.refilter()

    def _yuki_n_new_filter(self, filter_):
        self._filters.set_filter(filter_)
        self._model.refilter()

    def __init__(self, parent):
        self._parent = parent
        self._show_hidden = False
        self._filters = NagatoFilters(self)
        yuki_base_model = self._enquiry("YUKI.N > base model")
        self._model = yuki_base_model.filter_new()
        self._model.set_visible_func(self._visible_func)
        NagatoPaned(self)
