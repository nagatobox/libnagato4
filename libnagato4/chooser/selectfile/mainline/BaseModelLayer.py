
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.model.BaseModel import NagatoBaseModel
from libnagato4.chooser.selectfile.mainline.FilterModelLayer import (
    NagatoFilterModelLayer
    )


class NagatoBaseModelLayer(NagatoObject):

    def _inform_loaded(self):
        return self._loaded

    def _yuki_n_set_loaded(self, loaded):
        self._loaded = loaded

    def _yuki_n_force_sort_column(self, user_data):
        self._base_model.set_sort_column_id(*user_data)

    def _inform_base_model(self):
        return self._base_model

    def __init__(self, parent):
        self._parent = parent
        self._loaded = False
        self._base_model = NagatoBaseModel(self)
        NagatoFilterModelLayer(self)
