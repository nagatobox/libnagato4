
from libnagato4.Object import NagatoObject


class NagatoKeywords(NagatoObject):

    def _matches(self, display_name):
        for yuki_keyword in self._keywords:
            if yuki_keyword in display_name:
                return True
        return False

    def _show_hidden(self, display_name):
        if self._enquiry("YUKI.N > show hidden"):
            return True
        return not display_name.startswith(".")

    def matches(self, display_name):
        if self._keywords == []:
            return self._show_hidden(display_name)
        return self._matches(display_name)

    def set_keywrds(self, filter_):
        self._keywords = filter_.split()

    def __init__(self, parent):
        self._parent = parent
        self._keywords = []
