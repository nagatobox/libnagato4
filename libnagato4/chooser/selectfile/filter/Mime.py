
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoMime(NagatoObject):

    def _matches(self, content_type):
        for yuki_type in self._types.values():
            if GLib.regex_match_simple(yuki_type, content_type, 0, 0):
                return True
        return False

    def matches(self, content_type):
        if self._types == {"all": "*/*"}:
            return True
        if content_type == "inode/directory":
            return True
        return self._matches(content_type)

    def __init__(self, parent):
        self._parent = parent
        self._types = self._enquiry("YUKI.N > chooser config", "mime-types")
