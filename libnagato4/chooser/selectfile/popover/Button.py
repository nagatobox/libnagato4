
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin

LABEL = "Create Directory"


class NagatoButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, *args):
        self._raise("YUKI.N > try make directory")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, LABEL, relief=Gtk.ReliefStyle.NONE)
        self.set_relief(False)
        self.set_sensitive(False)
        self.props.tooltip_text = LABEL
        Margin.set_with_unit(self, 1, 1, 0, 1)
        self._raise("YUKI.N > add to box", self)
        self.connect("clicked", self._on_clicked)
