
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoActivatable(NagatoObject):

    def make_directry(self):
        if self._activatable:
            yuki_gio_file = Gio.File.new_for_path(self._path)
            yuki_gio_file.make_directory(None)
            self._raise("YUKI.N > created")
            self._raise("YUKI.N > directory moved", yuki_gio_file.get_path())

    def get_activatable(self, text):
        yuki_directory = self._enquiry("YUKI.N > directory")
        self._path = GLib.build_filenamev([yuki_directory, text])
        if text == "":
            self._activatable = False
        else:
            yuki_exists = GLib.file_test(self._path, GLib.FileTest.EXISTS)
            self._activatable = not yuki_exists
        return self._activatable

    def __init__(self, parent):
        self._parent = parent
        self._activatable = False
