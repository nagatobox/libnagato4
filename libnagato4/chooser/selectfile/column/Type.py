
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.model import Types
from libnagato4.treeview.renderer.Text import HaruhiRenderer
from libnagato4.treeview.column.SortableZebra import HaruhiSortableZebra


class NagatoType(Gtk.TreeViewColumn, NagatoObject, HaruhiSortableZebra):

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer.new()
        Gtk.TreeViewColumn.__init__(
            self,
            title="Type",
            cell_renderer=yuki_renderer,
            text=Types.CONTENT_TYPE,
            )
        self.set_expand(False)
        self._raise("YUKI.N > insert column", self)
        self._bind_sortable_zebra(yuki_renderer, Types.CONTENT_TYPE)
