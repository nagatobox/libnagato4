
from gi.repository import Gtk
from gi.repository import Pango
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.model import Types
from libnagato4.treeview.renderer.Text import HaruhiRenderer
from libnagato4.treeview.column.SortableZebra import HaruhiSortableZebra


class NagatoName(Gtk.TreeViewColumn, NagatoObject, HaruhiSortableZebra):

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer.new(Pango.EllipsizeMode.MIDDLE)
        Gtk.TreeViewColumn.__init__(
            self,
            title="Name",
            cell_renderer=yuki_renderer,
            text=Types.DISPLAY_NAME,
            )
        self.set_expand(True)
        self._raise("YUKI.N > insert column", self)
        self._bind_sortable_zebra(yuki_renderer, Types.SORT_NAME)
