
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.model import Types
from libnagato4.chooser.selectfile.model.Observers import NagatoObservers
from libnagato4.chooser.selectfile.append.Append import HaruhiAppend
from libnagato4.chooser.selectfile.model.Delete import HaruhiDelete


class NagatoBaseModel(Gtk.ListStore, NagatoObject):

    def _yuki_n_append(self, user_data):
        yuki_directory, yuki_file_info = user_data
        self._append.make(yuki_directory, yuki_file_info)

    def _yuki_n_delete_fullpath(self, fullpath):
        self._delete.delete_fullpath(fullpath)

    def _yuki_n_clear_model(self):
        self.clear()

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *Types.TYPES)
        self.set_sort_column_id(Types.SORT_NAME, Gtk.SortType.ASCENDING)
        self._append = HaruhiAppend(self)
        self._delete = HaruhiDelete(self)
        NagatoObservers(self)
