
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.model.AppendEvent import NagatoApendEvent
from libnagato4.chooser.selectfile.model.DeleteEvent import NagatoDeleteEvent

FLAG = Gio.FileMonitorFlags.WATCH_MOVES


class NagatoObserver(NagatoObject):

    def _on_changed(self, monitor, file_alfa, file_bravo, event, directory):
        yuki_data = directory, file_alfa, file_bravo, event
        self._delete_event.dispatch(*yuki_data)
        self._append_event.dispatch(*yuki_data)

    def move_directory(self, directory):
        if self._monitor is not None:
            self._monitor.cancel()
        yuki_gio_file = Gio.File.new_for_path(directory)
        self._monitor = yuki_gio_file.monitor_directory(FLAG, None)
        self._monitor.connect("changed", self._on_changed, directory)

    def __init__(self, parent):
        self._parent = parent
        self._monitor = None
        self._append_event = NagatoApendEvent(self)
        self._delete_event = NagatoDeleteEvent(self)
