
from gi.repository import Gio
from libnagato4.Object import NagatoObject


DISPATCH = {
    Gio.FileMonitorEvent.MOVED_OUT: "_deleted",
    Gio.FileMonitorEvent.DELETED: "_deleted",
    Gio.FileMonitorEvent.RENAMED: "_renamed"
    }


class NagatoDeleteEvent(NagatoObject):

    def _deleted(self, directory, file_alfa, file_bravo=None):
        if directory == file_alfa.get_path():
            self._raise("YUKI.N > directory lost")
        else:
            self._raise("YUKI.N > delete fullpath", file_alfa.get_path())

    def _renamed(self, directory, file_alfa, file_bravo):
        self._raise("YUKI.N > delete fullpath", file_alfa.get_path())

    def dispatch(self, directory, file_alfa, file_bravo, event):
        if event in DISPATCH:
            yuki_method = getattr(self, DISPATCH[event])
            yuki_method(directory, file_alfa, file_bravo)

    def __init__(self, parent):
        self._parent = parent
