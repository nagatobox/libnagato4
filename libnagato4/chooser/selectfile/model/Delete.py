
from libnagato4.chooser.selectfile.model import Types


class HaruhiDelete:

    def delete_fullpath(self, fullpath):
        for yuki_tree_row in self._model:
            if yuki_tree_row[Types.FULLPATH] == fullpath:
                self._model.remove(yuki_tree_row.iter)
                break

    def __init__(self, model):
        self._model = model
