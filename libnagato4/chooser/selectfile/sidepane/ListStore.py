
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.sidepane import Types
from libnagato4.chooser.selectfile.sidepane import XdgUserDirs
from libnagato4.chooser.selectfile.sidepane.TreeView import NagatoTreeView
from libnagato4.chooser.selectfile.sidepane import Gtk3Bookmark


class NagatoListStore(Gtk.ListStore, NagatoObject):

    def _inform_model(self):
        return self

    def _add_xdg_user_dirs(self):
        for yuki_data_row in XdgUserDirs.parse():
            self.append(yuki_data_row)

    def _add_gtk3_bookmarks(self):
        xdg_user_dirs_paths = XdgUserDirs.get_paths()
        for yuki_path in Gtk3Bookmark.read():
            if yuki_path in xdg_user_dirs_paths:
                continue
            yuki_basename = GLib.filename_display_basename(yuki_path)
            yuki_data_row = "folder-symbolic", yuki_basename, yuki_path
            self.append(yuki_data_row)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *Types.COLUMNS)
        self._add_xdg_user_dirs()
        self._add_gtk3_bookmarks()
        NagatoTreeView(self)
