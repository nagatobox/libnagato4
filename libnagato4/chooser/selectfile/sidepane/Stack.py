
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.sidepane.ListStore import NagatoListStore
from libnagato4.preview.Previewers import NagatoPreviewers


class NagatoStack(Gtk.Stack, NagatoObject):

    def _yuki_n_add_to_stack_named(self, user_data):
        yuki_widget, yuki_name = user_data
        self.add_named(yuki_widget, yuki_name)

    def _yuki_n_switch_stack_to(self, name):
        self.set_visible_child_name(name)
        self._raise("YUKI.N > preview shown")
        if name != "web-view":
            self._previewers.stop_media()

    def back_to_bookmark(self):
        self._previewers.stop_media()
        self.set_visible_child_name("bookmark")

    def show_preview(self, fullpath):
        self._previewers.set_path(fullpath)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        self._raise("YUKI.N > add to box", self)
        NagatoListStore(self)
        self._previewers = NagatoPreviewers(self)
