
from gi.repository import GLib

ICON_NAMES = {
    0: "user-desktop-symbolic",
    1: "folder-documents-symbolic",
    2: "folder-download-symbolic",
    3: "folder-music-symbolic",
    4: "folder-pictures-symbolic",
    5: "folder-publicshare-symbolic",
    6: "folder-templates-symbolic",
    7: "folder-videos-symbolic",
}


def parse():
    for yuki_index in range(8):
        yuki_fullpath = GLib.get_user_special_dir(yuki_index)
        yuki_basename = GLib.filename_display_basename(yuki_fullpath)
        yuki_icon_name = ICON_NAMES[yuki_index]
        yield [yuki_icon_name, yuki_basename, yuki_fullpath]


def get_paths():
    yuki_list = []
    for yuki_index in range(8):
        yuki_list.append(GLib.get_user_special_dir(yuki_index))
    return yuki_list.copy()
