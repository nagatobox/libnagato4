
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.widget import Margin
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.ui.SidePane import NagatoSidePane
from libnagato4.chooser.selectfile.ui.FilerBox import NagatoFilerBox


class NagatoPaned(Gtk.Paned, NagatoObject):

    def _yuki_n_path_selected(self, fullpath):
        self._side_pane.show_preview(fullpath)

    def _yuki_n_add_to_paned_left(self, widget):
        self.add1(widget)

    def _yuki_n_add_to_paned_right(self, widget):
        self.add2(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Paned.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(Unit(120), Unit(60))
        self.set_wide_handle(True)
        Margin.set_with_unit(self, 2, 2, 2, 2)
        self._side_pane = NagatoSidePane(self)
        NagatoFilerBox(self)
        self._raise("YUKI.N > add to content area", self)
        self._raise("YUKI.N > css", (self, "content-area"))
