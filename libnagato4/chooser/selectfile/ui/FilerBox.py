
from libnagato4.widget.VBox import AbstractVBox
from libnagato4.chooser import Types
from libnagato4.chooser.selectfile.filename.Box import NagatoBox as FileName
from libnagato4.chooser.selectfile.header.Box import NagatoBox as Header
from libnagato4.chooser.selectfile.treeview.Window import NagatoWindow


class NagatoFilerBox(AbstractVBox):

    def _on_initialize(self):
        yuki_type = self._enquiry("YUKI.N > chooser config", "type")
        if Types.SAVE_FILE == yuki_type:
            FileName(self)
        Header(self)
        NagatoWindow(self)
        self._raise("YUKI.N > add to paned right", self)
