

def make(file_info):
    yuki_date_time = file_info.get_modification_date_time()
    yuki_age = yuki_date_time.to_unix()
    yuki_age_readable = yuki_date_time.format("%F")
    return yuki_age, yuki_age_readable
