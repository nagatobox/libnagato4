
from gi.repository import GLib


def make(is_directory, file_info):
    yuki_size = file_info.get_size()
    if is_directory:
        yuki_size_readable = ""
    else:
        yuki_size_readable = GLib.format_size(yuki_size)
    return yuki_size, yuki_size_readable
