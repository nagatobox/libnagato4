
from libnagato4.chooser.selectfile.append import NameFactory
from libnagato4.chooser.selectfile.append import AgeFactory
from libnagato4.chooser.selectfile.append import SizeFactory


class HaruhiAppend:

    def make(self, directory, file_info):
        yuki_content_type = file_info.get_content_type()
        yuki_is_directory = (yuki_content_type == "inode/directory")
        yuki_data = [
            file_info.get_icon(),           # gio icon
            *NameFactory.make(directory, yuki_is_directory, file_info),
            yuki_content_type,              # content type
            yuki_is_directory,              # is directory
            *AgeFactory.make(file_info),
            *SizeFactory.make(yuki_is_directory, file_info)
            ]
        self._model.append(yuki_data)

    def __init__(self, model):
        self._model = model
