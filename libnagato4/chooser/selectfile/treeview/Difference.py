
from libnagato4.Object import NagatoObject
from libnagato4.chooser import Types


class NagatoDifference(NagatoObject):

    def _try_raise_signal(self, new_list):
        if self._active:
            self._raise("YUKI.N > selection changed", new_list)

    def set_new(self, new_list):
        self._try_raise_signal(new_list)
        yuki_diff_set = set(new_list) - set(self._old_list)
        if yuki_diff_set:
            yuki_path = list(yuki_diff_set)[0]
            self._raise("YUKI.N > path selected", yuki_path)
        self._old_list = new_list

    def __init__(self, parent):
        self._parent = parent
        self._old_list = []
        yuki_type = self._enquiry("YUKI.N > chooser config", "type")
        self._active = yuki_type in [Types.SELECT_FILE, Types.SELECT_FILES]
