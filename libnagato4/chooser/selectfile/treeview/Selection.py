
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.model import Types
from libnagato4.chooser.selectfile.treeview.Difference import NagatoDifference


class NagatoSelection(NagatoObject):

    def _on_changed(self, tree_selection):
        if not self._enquiry("YUKI.N > loaded"):
            return
        yuki_selection = []
        yuki_model, yuki_tree_paths = tree_selection.get_selected_rows()
        for yuki_tree_path in yuki_tree_paths:
            yuki_tree_row = yuki_model[yuki_tree_path]
            yuki_selection.append(yuki_tree_row[Types.FULLPATH])
        self._difference.set_new(yuki_selection)

    def __init__(self, parent):
        self._parent = parent
        self._difference = NagatoDifference(self)
        yuki_tree_view = self._enquiry("YUKI.N > tree view")
        yuki_mode = self._enquiry("YUKI.N > chooser config", "selection-mode")
        yuki_selection = yuki_tree_view.get_selection()
        yuki_selection.set_mode(yuki_mode)
        yuki_selection.connect("changed", self._on_changed)
