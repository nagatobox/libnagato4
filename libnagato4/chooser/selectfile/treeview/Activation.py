
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.model import Types


class NagatoActivation(NagatoObject):

    def _on_row_activated(self, tree_view, tree_path, column):
        yuki_model = tree_view.get_model()
        yuki_tree_row = yuki_model[tree_path]
        if yuki_tree_row[Types.IS_DIRECTORY]:
            yuki_fullpath = yuki_tree_row[Types.FULLPATH]
            self._raise("YUKI.N > directory moved", yuki_fullpath)
        elif self._enquiry("YUKI.N > chooser config", "selectable"):
            self._raise("YUKI.N > response", Gtk.ResponseType.APPLY)

    def __init__(self, parent):
        self._parent = parent
        yuki_tree_view = self._enquiry("YUKI.N > tree view")
        yuki_tree_view.connect("row-activated", self._on_row_activated)
