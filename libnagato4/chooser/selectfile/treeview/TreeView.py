
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.column.Columns import AsakuraColumns
from libnagato4.chooser.selectfile.treeview.Selection import NagatoSelection
from libnagato4.chooser.selectfile.treeview.Activation import NagatoActivation


class NagatoTreeView(Gtk.TreeView, NagatoObject):

    def _yuki_n_insert_column(self, column):
        self.insert_column(column, -1)

    def _inform_tree_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        yuki_model = self._enquiry("YUKI.N > filter model")
        Gtk.TreeView.__init__(self, model=yuki_model)
        self._selection = NagatoSelection(self)
        NagatoActivation(self)
        AsakuraColumns(self)
        self._raise("YUKI.N > add to scrolled window", self)
