
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.treeview.TreeView import NagatoTreeView


class NagatoWindow(Gtk.ScrolledWindow, NagatoObject):

    def _yuki_n_add_to_scrolled_window(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        self.set_vexpand(True)
        NagatoTreeView(self)
        self._raise("YUKI.N > add to box", self)
