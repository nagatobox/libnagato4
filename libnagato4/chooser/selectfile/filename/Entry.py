
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.filename.Grab import NagatoGrab
from libnagato4.chooser.selectfile.filename.Event import NagatoEvent


class NagatoEntry(Gtk.Entry, NagatoObject):

    def _inform_entry(self):
        return self

    def _inform_entry_text(self):
        return self.get_text()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self)
        self.set_hexpand(True)
        NagatoGrab(self)
        NagatoEvent(self)
        self._raise("YUKI.N > add to box", self)
