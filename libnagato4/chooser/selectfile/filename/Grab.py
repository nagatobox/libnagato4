
from libnagato4.Object import NagatoObject


class NagatoGrab(NagatoObject):

    def _on_realize(self, entry):
        yuki_text = self._enquiry("YUKI.N > chooser config", "current-name")
        entry.set_text(yuki_text)
        yuki_list = yuki_text.split(".")
        if len(yuki_list) == 1:
            yuki_position = -1
        else:
            yuki_position = len(yuki_text) - len(yuki_list[-1]) - 1
        entry.grab_focus()
        entry.select_region(0, yuki_position)

    def __init__(self, parent):
        self._parent = parent
        yuki_entry = self._enquiry("YUKI.N > entry")
        yuki_entry.connect("realize", self._on_realize)
