
from gi.repository import Gio
from gi.repository import GLib


def _get_valid_files(directory, file_name):
    if file_name == "" or "/" in file_name:
        return []
    else:
        yuki_path = GLib.build_filenamev([directory, file_name])
        return [Gio.File.new_for_path(yuki_path)]


def get_valid_files(directory, file_name):
    if not GLib.file_test(directory, GLib.FileTest.IS_DIR):
        return []
    return _get_valid_files(directory, file_name)
