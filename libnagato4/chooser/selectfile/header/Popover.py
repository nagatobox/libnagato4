
from gi.repository import Gtk
from libnagato4.chooser import Types
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.SingleStack import AbstractSingleStack
from libnagato4.chooser.selectfile.popover.HBox import NagatoHBox
from libnagato4.chooser.selectfile.popover.ShowHidden import NagatoShowHidden


class NagatoPopover(AbstractSingleStack):

    def _inform_directory(self):
        return self._directory

    def popup_for_directory(self, relative_widget, directory):
        self._directory = directory
        self.set_relative_to(relative_widget)
        self.show_all()

    def _add_to_single_stack(self):
        self.set_position(Gtk.PositionType.BOTTOM)
        NagatoShowHidden(self)
        yuki_type = self._enquiry("YUKI.N > chooser config", "type")
        if yuki_type in [Types.SELECT_DIRECTORY, Types.SAVE_FILE]:
            NagatoSeparator(self)
            NagatoHBox(self)
