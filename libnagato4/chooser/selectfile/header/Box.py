
from libnagato4.Ux import Unit
from libnagato4.widget.HBox import AbstractHBox
from libnagato4.chooser.selectfile.header.HomeButton import NagatoHomeButton
from libnagato4.chooser.selectfile.header.UpButton import NagatoUpButton
from libnagato4.chooser.selectfile.header.Entry import NagatoEntry
from libnagato4.chooser.selectfile.header.MenuButton import NagatoMenuButton


class NagatoBox(AbstractHBox):

    def _on_initialize(self):
        NagatoHomeButton(self)
        NagatoUpButton(self)
        NagatoEntry(self)
        NagatoMenuButton(self)
        self.set_margin_bottom(Unit(1))
        self._raise("YUKI.N > css", (self, "toolbar"))
        self._raise("YUKI.N > add to box", self)
