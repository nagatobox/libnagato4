
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Object import NagatoObject

ICON_NAME = "go-home-symbolic"
ICON_SIZE = Gtk.IconSize.LARGE_TOOLBAR


class NagatoHomeButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, *args):
        self._raise("YUKI.N > directory moved", GLib.get_home_dir())

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        yuki_image = Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE)
        self.set_image(yuki_image)
        self.props.tooltip_text = "Go to Home"
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > css", (self, "toolbar-button"))
