
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.header.FilterMaker import NagatoFilterMaker


class NagatoEntry(Gtk.Entry, NagatoObject):

    def receive_transmission(self, directory):
        self.handler_block(self._handler_id)
        yuki_visible_path = "/" if directory == "/" else directory+"/"
        self.set_text(yuki_visible_path)
        self._filter_maker.set_directory(directory)
        self.handler_unblock(self._handler_id)

    def _on_changed(self, entry):
        self._filter_maker.make(entry.get_text())

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self)
        self.set_hexpand(True)
        self._filter_maker = NagatoFilterMaker(self)
        self._handler_id = self.connect("changed", self._on_changed)
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > register navigation object", self)
