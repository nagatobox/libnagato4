
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin


class NagatoColorSelection(NagatoObject):

    def _on_color_changed(self, color_selection):
        if color_selection.is_adjusting():
            return
        yuki_rgba = color_selection.get_current_rgba()
        self._raise("YUKI.N > selection changed", yuki_rgba)

    def __init__(self, parent):
        self._parent = parent
        yuki_selection = Gtk.ColorSelection.new()
        yuki_selection.set_has_opacity_control(False)
        yuki_rgba = self._enquiry("YUKI.N > chooser config", "default_rgba")
        yuki_selection.set_current_rgba(yuki_rgba)
        Margin.set_with_unit(yuki_selection, 2, 2, 2, 2)
        self._raise("YUKI.N > add to content area", yuki_selection)
        yuki_selection.connect("color-changed", self._on_color_changed)
