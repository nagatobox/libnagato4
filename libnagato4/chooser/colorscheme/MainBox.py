
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin
from libnagato4.chooser.colorscheme.ColorConfig import NagatoColorConfig


class NagatoMainBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)
        if len(self.get_children()) % 2 == 0:
            self._raise("YUKI.N > css", (widget, "config-box-even"))
        else:
            self._raise("YUKI.N > css", (widget, "config-box-odd"))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        NagatoColorConfig(self, "primary_fg_color")
        NagatoColorConfig(self, "primary_bg_color")
        NagatoColorConfig(self, "secondary_fg_color")
        NagatoColorConfig(self, "secondary_bg_color")
        NagatoColorConfig(self, "highlight_fg_color")
        NagatoColorConfig(self, "highlight_bg_color")
        Margin.set_with_unit(self, 2, 2, 2, 2)
        self._raise("YUKI.N > add to content area", self)
