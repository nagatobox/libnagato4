
import cairo
from gi.repository import Gtk
from gi.repository import Gdk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject

FORMAT = cairo.Format.ARGB32
WIDTH = Unit(6)
HEIGHT = Unit(4)


class NagatoColorImage(Gtk.Image, NagatoObject):

    def _set_color(self, rgba):
        yuki_surface = cairo.ImageSurface(FORMAT, WIDTH, HEIGHT)
        yuki_context = cairo.Context(yuki_surface)
        Gdk.cairo_set_source_rgba(yuki_context, rgba)
        yuki_context.rectangle(0, 0, WIDTH, HEIGHT)
        yuki_context.fill()
        self.set_from_surface(yuki_surface)

    def receive_transmission(self, user_data):
        self._set_color(self._enquiry("YUKI.N > rgba"))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self)
        self._set_color(self._enquiry("YUKI.N > rgba"))
        self._raise("YUKI.N > register selection object", self)
