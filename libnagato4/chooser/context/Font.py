
from libnagato4.Object import NagatoObject


class NagatoContext(NagatoObject):

    def select(self, selection):
        self._props["selection"] = selection

    def __getitem__(self, key):
        return self._props[key]

    def __init__(self, parent):
        self._parent = parent
        self._props = {}
        self._props["selectable"] = True
        self._props["title"] = "Select Font"
        self._props["selection"] = []
