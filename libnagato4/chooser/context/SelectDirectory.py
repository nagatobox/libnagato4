
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.chooser import Types
from libnagato4.chooser.context.Context import AbstractContext


class NagatoContext(AbstractContext):

    @classmethod
    def new_for_directory(cls, parent, directory=GLib.get_home_dir()):
        yuki_context = cls(parent)
        yuki_context._props["directory"] = directory
        return yuki_context

    def select(self, selection):
        self._props["selection"] = selection
        self._props["selectable"] = True

    def _on_initialize(self):
        self._props["selectable"] = False
        self._props["title"] = "Select Directory"
        self._props["type"] = Types.SELECT_DIRECTORY
        self._props["selection"] = ""
        self._props["selection-mode"] = Gtk.SelectionMode.BROWSE
        self._props["mime-types"] = {"all": "*/*"}
