
from libnagato4.Object import NagatoObject
from libnagato4.file import XdgUserDir


class NagatoSaveDestination(NagatoObject):

    def get_directory(self):
        if self._gio_file is None:
            yuki_query = "YUKI.N > data", "default-directory"
            return getattr(XdgUserDir, self._enquiry(*yuki_query))
        else:
            return self._gio_file.get_parent().get_path()

    def get_basename(self):
        if self._gio_file is None:
            yuki_query = "YUKI.N > data", "default-suffix"
            return "Untitled."+self._enquiry(*yuki_query)
        else:
            return self._gio_file.get_basename()

    def __init__(self, parent, gio_file=None):
        self._parent = parent
        self._gio_file = gio_file
