
from gi.repository import Gtk
from libnagato4.chooser import Types
from libnagato4.chooser.context.Context import AbstractContext


class AbstractAnySaveFile(AbstractContext):

    TITLE = "define header bar title here."

    def select(self, selection):
        self._props["selection"] = selection
        self._props["selectable"] = len(selection) > 0

    def get_gio_file(self):
        return self._props["selection"][0]

    def _get_mimetype(self):
        return self._enquiry("YUKI.N > data", "mime-types")

    def _on_initialize(self):
        self._props["selectable"] = False
        self._props["title"] = self.TITLE
        self._props["type"] = Types.SAVE_FILE
        self._props["selection"] = []
        self._props["selection-mode"] = Gtk.SelectionMode.BROWSE
        self._props["mime-types"] = self._get_mimetype()
