
from libnagato4.chooser.context.AnySaveFile import AbstractAnySaveFile
from libnagato4.chooser.context.SaveDestination import NagatoSaveDestination


class NagatoContext(AbstractAnySaveFile):

    TITLE = "Save File"

    @classmethod
    def new(cls, parent, gio_file=None):
        yuki_context = cls(parent)
        yuki_destination = NagatoSaveDestination(parent, gio_file)
        yuki_context["directory"] = yuki_destination.get_directory()
        yuki_context["current-name"] = yuki_destination.get_basename()
        return yuki_context
