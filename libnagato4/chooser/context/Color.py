
from libnagato4.chooser.context.Context import AbstractContext


class NagatoContext(AbstractContext):

    @classmethod
    def new(cls, parent, title, RGBA):
        yuki_context = cls(parent)
        yuki_context.set_props(title, RGBA)
        return yuki_context

    def get_rgba_string(self):
        yuki_rgba = self._props["selection"]
        return yuki_rgba.to_string()

    def select(self, selection):
        self._props["selection"] = selection

    def set_props(self, title, rgba):
        self._props["title"] = title
        self._props["default_rgba"] = rgba
        self._props["selection"] = rgba

    def _on_initialize(self):
        self._props["selectable"] = True
