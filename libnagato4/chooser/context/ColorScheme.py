
from libnagato4.Object import NagatoObject


class NagatoContext(NagatoObject):

    def select(self, selection):
        yuki_key, yuki_rgba = selection
        self._props["selection"][yuki_key] = yuki_rgba

    def get_changed(self):
        return self._props["selection"]

    def __getitem__(self, key):
        return self._props[key]

    def __init__(self, parent):
        self._parent = parent
        self._props = {}
        self._props["selectable"] = True
        self._props["title"] = "Color Scheme"
        self._props["selection"] = {}
