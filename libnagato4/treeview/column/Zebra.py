
from libnagato4.Ux import Unit


class HaruhiZebra:

    def _addtional_data_func(self, renderer, tree_row):
        pass

    def _data_func(self, column, renderer, model, tree_iter, user_data=None):
        yuki_tree_path = model.get_path(tree_iter)
        yuki_color = "White" if yuki_tree_path[0] % 2 == 0 else "LightGrey"
        renderer.set_property("height", Unit(5))
        renderer.set_property("cell-background", yuki_color)
        self._addtional_data_func(renderer, model[yuki_tree_path])

    def _bind_zebra_column(self, renderer):
        self.set_cell_data_func(renderer, self._data_func)
