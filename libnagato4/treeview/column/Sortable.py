

class HaruhiSortable:

    def _on_clicked(self, tree_column):
        self._order += 1
        yuki_order = self._order % 2
        yuki_data = self._sort_column_id, yuki_order
        self._raise("YUKI.N > force sort column", yuki_data)

    def _bind_sortable_column(self, sort_column_id):
        self._sort_column_id = sort_column_id
        self._order = 0
        self.set_sort_column_id(sort_column_id)
        self.connect("clicked", self._on_clicked)
