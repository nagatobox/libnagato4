
from gi.repository import Gtk
from gi.repository import Pango
from libnagato4.Ux import Unit


class HaruhiRenderer(Gtk.CellRendererText):

    @classmethod
    def new(cls, ellipsize=Pango.EllipsizeMode.NONE, xalign=0):
        yuki_renderer = cls()
        yuki_renderer.set_property("xpad", Unit(1))
        yuki_renderer.set_property("ellipsize", ellipsize)
        yuki_renderer.set_property("xalign", xalign)
        return yuki_renderer

    def __init__(self):
        Gtk.CellRendererText.__init__(self)
