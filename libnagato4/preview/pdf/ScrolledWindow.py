
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.preview.pdf.IconView import NagatoIconView


class NagatoScrolledWindow(Gtk.ScrolledWindow, NagatoObject):

    def _yuki_n_add_to_scrolled_window(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        NagatoIconView(self)
        self._raise("YUKI.N > add to stack named", (self, "pdf-viewer"))
