
from gi.repository import Gdk

MASK = Gdk.EventMask.SCROLL_MASK|Gdk.EventMask.BUTTON_PRESS_MASK


class HaruhiWebViewZoom:

    def _on_scroll(self, webview, event):
        yuki_zoom = webview.props.zoom_level
        if event.direction == Gdk.ScrollDirection.UP:
            webview.props.zoom_level = yuki_zoom+0.1
        if event.direction == Gdk.ScrollDirection.DOWN:
            webview.props.zoom_level = max(0.1, yuki_zoom-0.1)

    def _on_button_press(self, webview, event):
        if event.button == 1:
            webview.props.zoom_level = 1
        return True

    def _on_realize(self, webview):
        yuki_gdk_window = webview.get_window()
        yuki_gdk_window.set_events(MASK)
        webview.connect("scroll-event", self._on_scroll)
        webview.connect("button-press-event", self._on_button_press)

    def __init__(self, webview):
        webview.connect("realize", self._on_realize)
