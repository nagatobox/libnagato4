
from gi.repository import Gio
from gi.repository import GLib

MARKUP = ""\
    "<span size='large' underline='single'>{}</span>\n\n"\
    "contains following item(s).\n\n"


class HaruhiDirectoryMarkUp:

    def _get_markup(self, markup, fullpath):
        yuki_count = 0
        yuki_gio_file = Gio.File.new_for_path(fullpath)
        for yuki_file_info in yuki_gio_file.enumerate_children("*", 0):
            if 10 >= yuki_count:
                markup += yuki_file_info.get_name()+"\n"
            yuki_count += 1
        return markup, yuki_count

    def _set_items(self, markup, fullpath):
        yuki_markup, yuki_count = self._get_markup(markup, fullpath)
        if yuki_count > 10:
            yuki_markup += "\n\n...and more {} item(s)".format(yuki_count-10)
        return yuki_markup

    def get_for_fullpath(self, fullpath):
        yuki_basename = GLib.filename_display_basename(fullpath)
        yuki_markup = MARKUP.format(yuki_basename)
        return self._set_items(yuki_markup, fullpath)
