
import magic


def is_bad_file(fullpath, type_):
    yuki_magic = magic.from_file(fullpath, mime=True)
    yuki_magic_header = yuki_magic.split("/")[0]
    yuki_type_header = type_.split("/")[0]
    return yuki_magic_header != yuki_type_header
