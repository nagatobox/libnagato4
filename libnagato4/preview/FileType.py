
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagato4.preview import FileMagic

QUERY = "standard::content-type"


class NagatoFileType(NagatoObject):

    def _dispatch(self, fullpath, type_):
        if type_.startswith("text"):
            self._raise("YUKI.N > file found", ("text-view", fullpath))
        elif type_ == "application/pdf":
            self._raise("YUKI.N > file found", ("pdf-viewer", fullpath))
        else:
            self._raise("YUKI.N > file found", ("web-view", fullpath))

    def _check_magic(self, fullpath, type_):
        if FileMagic.is_bad_file(fullpath, type_):
            self._raise("YUKI.N > selection changed", ["invalid"])
            self._raise("YUKI.N > file found", ("bad-file-view", fullpath))
        else:
            self._dispatch(fullpath, type_)

    def dispatch(self, fullpath):
        yuki_gio_file = Gio.File.new_for_path(fullpath)
        yuki_file_info = yuki_gio_file.query_info(QUERY, 0)
        yuki_type = yuki_file_info.get_attribute_as_string(QUERY)
        if yuki_type == "inode/directory":
            self._raise("YUKI.N > file found", ("directory-view", fullpath))
        else:
            self._check_magic(fullpath, yuki_type)

    def __init__(self, parent):
        self._parent = parent
