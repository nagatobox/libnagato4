
from gi.repository import Gtk
from gi.repository import GtkSource
from libnagato4.Object import NagatoObject


class NagatoTextViewer(Gtk.ScrolledWindow, NagatoObject):

    def _on_populate_popup(self, widget, popup):
        popup.destroy()
        # print("popup ?", popup)
        return True

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        self._view = GtkSource.View.new_with_buffer(yuki_buffer)
        self._view.set_property("wrap-mode", Gtk.WrapMode.WORD_CHAR)
        self._view.props.populate_all = False
        self._view.connect("populate-popup", self._on_populate_popup)
        self.add(self._view)
        self._raise("YUKI.N > add to stack named", (self, "text-viewer"))
