
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.preview.FileType import NagatoFileType
from libnagato4.preview.WebView import NagatoWebView
from libnagato4.preview.BadFileView import NagatoBadFileView
from libnagato4.preview.TextModel import NagatoTextModel
from libnagato4.preview.DirectoryView import NagatoDirectoryView
from libnagato4.preview.pdf.PdfViewer import NagatoPdfViewer


class NagatoPreviewers(NagatoObject):

    def _yuki_n_file_found(self, user_data):
        yuki_viewer_name, yuki_fullpath = user_data
        yuki_viewer = self._viewers[yuki_viewer_name]
        yuki_viewer.load_path_async(yuki_fullpath)

    def set_path(self, fullpath):
        self._file_type.dispatch(fullpath)

    def stop_media(self):
        self._web_view.load_uri(GLib.filename_to_uri(GLib.get_home_dir()))

    def __init__(self, parent):
        self._parent = parent
        self._file_type = NagatoFileType(self)
        self._viewers = {}
        self._web_view = NagatoWebView(self)
        self._viewers["web-view"] = self._web_view
        self._viewers["directory-view"] = NagatoDirectoryView(self)
        self._viewers["text-view"] = NagatoTextModel(self)
        self._viewers["bad-file-view"] = NagatoBadFileView(self)
        self._viewers["pdf-viewer"] = NagatoPdfViewer(self)
