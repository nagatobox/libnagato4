
from gi.repository import Gtk
from gi.repository import Pango
from libnagato4.Object import NagatoObject
from libnagato4.preview.DirectoryMarkUp import HaruhiDirectoryMarkUp


class NagatoDirectoryLabel(Gtk.Label, NagatoObject):

    def reset(self, fullpath):
        self.set_markup(self._markup.get_for_fullpath(fullpath))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_justify(Gtk.Justification.CENTER)
        self.props.wrap = True
        self.props.wrap_mode = Pango.WrapMode.WORD_CHAR
        self.set_vexpand(True)
        self._markup = HaruhiDirectoryMarkUp()
        self._raise("YUKI.N > add to box", self)
