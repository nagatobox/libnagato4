
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.preview.BadFileIcon import NagatoBadFileIcon
from libnagato4.preview.BadFileLabel import NagatoBadFileLabel


class NagatoBadFileView(Gtk.Box, NagatoObject):

    def load_path_async(self, fullpath):
        self._raise("YUKI.N > switch stack to", "bad-file-view")
        self._label.reset(fullpath)

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        NagatoBadFileIcon(self)
        self._label = NagatoBadFileLabel(self)
        self.set_vexpand(True)
        self._raise("YUKI.N > add to stack named", (self, "bad-file-view"))
        self._raise("YUKI.N > css", (self, "dialog-warning"))
