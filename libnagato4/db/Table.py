
from libnagato4.Object import NagatoObject

COLUMNS_WITH_PRIMARY_KEY = "(id INTEGER PRIMARY KEY AUTOINCREMENT, {})"
CREATE_TABLE = "CREATE TABLE IF NOT EXISTS {} {}"


class NagatoTable(NagatoObject):

    TABLE_NAME = "YUKI.N > define tabel name here."
    FORMAT = "YUKI.N > define format here."
    """
    for example:
    FORMAT = "category_id INTEGER, title TEXT, url TEXT"
    """

    def _initialize(self):
        # add default data here.
        pass

    def __init__(self, parent):
        self._parent = parent
        yuki_columns = COLUMNS_WITH_PRIMARY_KEY.format(self.FORMAT)
        yuki_command = CREATE_TABLE.format(self.TABLE_NAME, yuki_columns)
        self._raise("YUKI.N > db execute", (yuki_command,))
        if self._enquiry("YUKI.N > created"):
            self._initialize()
