
import sqlite3
from libnagato4.Object import NagatoObject


class NagatoConnection(NagatoObject):

    def _execute(self, query, data=()):
        self._cursor.execute(query, data)
        self._connect.commit()

    def execute(self, user_data):
        self._execute(*user_data)

    def select_all(self, user_data):
        self._execute(*user_data)
        return self._cursor.fetchall()

    def select_one(self, user_data):
        self._execute(*user_data)
        return self._cursor.fetchone()

    def close(self):
        self._connect.close()

    def __init__(self, parent):
        self._parent = parent
        self._connect = sqlite3.connect(self._enquiry("YUKI.N > address"))
        self._connect.execute("VACUUM")
        self._cursor = self._connect.cursor()
