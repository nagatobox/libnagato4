
from gi.repository import GLib
from libnagato4.Object import NagatoObject

DB_NAME = "application.sqlite3"


class NagatoAddress(NagatoObject):

    def get_address(self):
        return self._address

    def get_created(self):
        return self._created

    def _ensure_address(self):
        yuki_directory = GLib.build_filenamev(
            [GLib.get_user_config_dir(), self._enquiry("YUKI.N > data", "id")]
            )
        self._address = GLib.build_filenamev([yuki_directory, DB_NAME])

    def __init__(self, parent):
        self._parent = parent
        self._ensure_address()
        self._created = not GLib.file_test(self._address, GLib.FileTest.EXISTS)
