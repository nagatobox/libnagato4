
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.main.BackToMain import NagatoBackToMain
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.colorscheme.Header import AsakuraHeader
from libnagato4.popover.colorscheme.Highlight import AsakuraHighlight


class NagatoColorSchemeBox(NagatoBox):

    NAME = "color_scheme"

    def _on_initialize(self):
        NagatoBackToMain(self)
        NagatoSeparator(self)
        AsakuraHeader(self)
        NagatoSeparator(self)
        AsakuraHighlight(self)
