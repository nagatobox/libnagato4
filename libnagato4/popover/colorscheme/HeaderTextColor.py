
from libnagato4.popover.colorscheme.SelectColor import NagatoSelectColor


class NagatoHeaderTextColor(NagatoSelectColor):

    TITLE = "Header Text Color"
    USER_DATA = "css", "header_foreground_color"
