
from libnagato4.popover.colorscheme.SelectColor import NagatoSelectColor


class NagatoHeaderColor(NagatoSelectColor):

    TITLE = "Header Color"
    USER_DATA = "css", "header_background_color"
