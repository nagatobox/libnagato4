
from libnagato4.popover.item.StaticLabel import NagatoStaticLabel
from libnagato4.popover.colorscheme.HighlightColor import NagatoHighlightColor
from libnagato4.popover.colorscheme.HighlightTextColor import (
    NagatoHighlightTextColor
    )


class AsakuraHighlight:

    def __init__(self, parent):
        NagatoStaticLabel.new_with_label(parent, "Highlight: ")
        NagatoHighlightColor(parent)
        NagatoHighlightTextColor(parent)
