
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoStack(Gtk.Stack, NagatoObject):

    def _yuki_n_add_named(self, user_data):
        yuki_widget, yuki_name = user_data
        self.add_named(yuki_widget, yuki_name)

    def _yuki_n_switch_stack_to(self, user_data):
        yuki_child_name, yuki_transition_type = user_data
        self.set_visible_child_full(yuki_child_name, yuki_transition_type)

    def _on_initialize(self):
        self._raise("YUKI.N > loopback add popover groups", self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        self.set_transition_duration(250)
        self.set_homogeneous(False)
        self._on_initialize()
        self._raise("YUKI.N > add to popover", self)
