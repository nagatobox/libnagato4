
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoPopover(Gtk.Popover, NagatoObject):

    def _yuki_n_add_to_popover(self, widget):
        self.add(widget)

    def _yuki_n_popdown(self):
        self.popdown()

    def _on_initialize(self):
        raise NotImplementedError

    def _on_button_press(self, popover, event):
        self.popdown()

    def _on_show(self, *args):
        self._raise("YUKI.N > enable global key shortcuts", False)

    def _on_popdown(self, *args):
        self._raise("YUKI.N > enable global key shortcuts", True)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Popover.__init__(self)
        self.connect("show", self._on_show)
        self.connect("closed", self._on_popdown)
        self.connect("button-press-event", self._on_button_press)
        self._on_initialize()
