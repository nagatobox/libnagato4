
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToConfig(NagatoToStack):

    TITLE = "Settings"
    WHERE_TO_GO = "config"
