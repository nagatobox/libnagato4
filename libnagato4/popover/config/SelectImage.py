
from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagato4.chooser.context.BackgroundImage import NagatoContext


class NagatoSelectImage(NagatoItem):

    TITLE = "Select Background Image"
    MESSAGE = "YUKI.N > settings"
    USER_DATA = "css", "window_background_image"

    def _select_background(self):
        yuki_context = NagatoContext(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return
        yuki_data = self.USER_DATA+(yuki_context.get_selected(),)
        self._raise(self.MESSAGE, yuki_data)

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        self._select_background()
