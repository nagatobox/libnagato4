
from libnagato4.popover.item.StaticLabel import NagatoStaticLabel
from libnagato4.popover.config.SelectImage import NagatoSelectImage
from libnagato4.popover.config.BackgroundOpacity import NagatoBackgroundOpacity


class AsakuraBackgroundSettings:

    def __init__(self, parent):
        NagatoStaticLabel.new_with_label(parent, "Background Settings:")
        NagatoSelectImage(parent)
        NagatoBackgroundOpacity(parent)
