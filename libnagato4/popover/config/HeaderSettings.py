
from libnagato4.popover.item.StaticLabel import NagatoStaticLabel
from libnagato4.popover.config.HeaderOpacity import NagatoHeaderOpacity
from libnagato4.popover.config.ShowIcon import NagatoShowIcon
from libnagato4.popover.config.ToHeaderStyle import NagatoToHeaderStyle


class AsakuraHeaderSettings:

    def __init__(self, parent):
        NagatoStaticLabel.new_with_label(parent, "Header Settings:")
        NagatoToHeaderStyle(parent)
        NagatoShowIcon(parent)
        NagatoHeaderOpacity(parent)
