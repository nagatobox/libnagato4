
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.main.BackToMain import NagatoBackToMain
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.config.HeaderTypeItem import NagatoHeaderTypeItem


class NagatoHeaderStyleBox(NagatoBox):

    NAME = "header_style"

    def _set_header_type_items(self):
        for yuki_type in ["bold", "medium", "thin", "invisible"]:
            NagatoHeaderTypeItem.new_for_type(self, yuki_type)

    def _on_initialize(self):
        NagatoBackToMain(self)
        NagatoSeparator(self)
        self._set_header_type_items()
