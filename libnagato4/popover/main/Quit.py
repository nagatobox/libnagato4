
from libnagato4.popover.item.Item import NagatoItem


class NagatoQuit(NagatoItem):

    TITLE = "Quit"
    MESSAGE = "YUKI.N > action"
    USER_DATA = "quit"

    def _on_initialize(self):
        self.props.tooltip_text = "Ctrl+Q"
