
from libnagato4.popover.item.Item import NagatoItem


class NagatoToggleMaximize(NagatoItem):

    TITLE = "Toggle Maximized"
    MESSAGE = "YUKI.N > action"
    USER_DATA = "toggle maximized"

    def _on_initialize(self):
        self.props.tooltip_text = "Shift+F11"
