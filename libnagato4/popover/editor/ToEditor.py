
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToEditor(NagatoToStack):

    TITLE = "Editor"
    WHERE_TO_GO = "editor"
