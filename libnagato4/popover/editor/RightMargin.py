
from libnagato4.popover.spinitem.SpinItem import AbstractSpinItem


class NagatoRightMargin(AbstractSpinItem):

    SETTING_GROUP = "editor"
    SETTING_KEY = "right_margin_position"
    SETTING_DEFAULT = 80
    LABEL = "Right Margin"
    RANGE = 0, 400, 1
