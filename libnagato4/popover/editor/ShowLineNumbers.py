
from libnagato4.popover.editor.BooleanConfig import AbstractBooleanConfig


class NagatoShowLineNmbers(AbstractBooleanConfig):

    TITLE = "Show Line Numbers"
    KEY = "show_line_numbers"
    DEFAULT = False
