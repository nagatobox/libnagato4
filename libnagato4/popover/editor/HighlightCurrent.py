
from libnagato4.popover.editor.BooleanConfig import AbstractBooleanConfig


class NagatoHighlightCurrent(AbstractBooleanConfig):

    TITLE = "Hilight Current Line"
    KEY = "highlight_current_line"
    DEFAULT = True
