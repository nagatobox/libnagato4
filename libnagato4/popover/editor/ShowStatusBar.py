
from libnagato4.popover.editor.BooleanConfig import AbstractBooleanConfig


class NagatoShowStatusBar(AbstractBooleanConfig):

    TITLE = "Show Status Bar"
    KEY = "show_status_bar"
    DEFAULT = False
