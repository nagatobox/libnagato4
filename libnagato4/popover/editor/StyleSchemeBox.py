
from gi.repository import GtkSource
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.editor.BackToEditor import NagatoBackToEditor
from libnagato4.popover.editor.StyleSchemeItem import NagatoStyleSchemeItem


class NagatoStyleSchemeBox(NagatoBox):

    NAME = "style_scheme"

    def _add_style_scheme_items(self):
        yuki_manager = GtkSource.StyleSchemeManager.get_default()
        for yuki_id in yuki_manager.get_scheme_ids():
            yuki_scheme = yuki_manager.get_scheme(yuki_id)
            NagatoStyleSchemeItem.new_for_scheme(self, yuki_scheme)

    def _on_initialize(self):
        NagatoBackToEditor(self)
        NagatoSeparator(self)
        self._add_style_scheme_items()
