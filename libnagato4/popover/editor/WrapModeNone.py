
from libnagato4.popover.editor.WrapMode import AbstractWrapMode


class NagatoWrapModeNone(AbstractWrapMode):

    TITLE = "None"
    MATCH_TARGET = 0
