
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.editor.ToStyleScheme import NagatoToStyleScheme
from libnagato4.popover.editor.ToWrapMode import NagatoToWrapMode
from libnagato4.popover.editor.ToIndent import NagatoToIndent
from libnagato4.popover.editor.ToView import NagatoToView
from libnagato4.popover.editor.Font import NagatoFont
from libnagato4.popover.editor.SmartBackspace import NagatoSmartBackspace
from libnagato4.popover.editor.RemoveTrailing import NagatoRemoveTrailing
from libnagato4.popover.editor.ShowStatusBar import NagatoShowStatusBar


class AsakuraItems:

    def __init__(self, parent):
        NagatoToStyleScheme(parent)
        NagatoToWrapMode(parent)
        NagatoToIndent(parent)
        NagatoToView(parent)
        NagatoFont(parent)
        NagatoSeparator(parent)
        NagatoRemoveTrailing(parent)
        NagatoSmartBackspace(parent)
        NagatoShowStatusBar(parent)
