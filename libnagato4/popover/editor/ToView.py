
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToView(NagatoToStack):

    TITLE = "View"
    WHERE_TO_GO = "view"
