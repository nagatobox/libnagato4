
from libnagato4.popover.editor.BooleanConfig import AbstractBooleanConfig


class NagatoAutoIndent(AbstractBooleanConfig):

    TITLE = "Auto Indent"
    KEY = "auto_indent"
    DEFAULT = True
