
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToWrapMode(NagatoToStack):

    TITLE = "Wrap Mode"
    WHERE_TO_GO = "wrap_mode"
