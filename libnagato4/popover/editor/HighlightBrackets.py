
from libnagato4.popover.editor.BooleanConfig import AbstractBooleanConfig


class NagatoHighlightBrackets(AbstractBooleanConfig):

    TITLE = "Highlight Brackets"
    KEY = "highlight_matching_brackets"
    DEFAULT = False
