
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.popover.item.ItemEvent import HaruhiItemEvent


class AbstractIconItem(Gtk.Button, NagatoObject, HaruhiItemEvent):

    ICON_NAME = "define icon name here"
    SIZE = Gtk.IconSize.BUTTON

    def _on_initialize(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        yuki_image = Gtk.Image.new_from_icon_name(self.ICON_NAME, self.SIZE)
        self.set_image(yuki_image)
        self._bind_events()
        self._raise("YUKI.N > css", (self, "popover-button"))
        self._on_initialize()
        self._raise("YUKI.N > add to box", self)
