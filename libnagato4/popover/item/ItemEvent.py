

class HaruhiItemEvent:

    MESSAGE = "YUKI.N > ignore"
    USER_DATA = None

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        self._raise(self.MESSAGE, self.USER_DATA)

    def _on_map(self, *args):
        pass

    def _bind_events(self):
        self.connect("clicked", self._on_clicked)
        self.connect("map", self._on_map)
