
from gi.repository import Gtk
from libnagato4.widget import Margin
from libnagato4.Object import NagatoObject


class NagatoStaticLabel(Gtk.Label, NagatoObject):

    @classmethod
    def new_with_label(cls, parent, label):
        yuki_label = cls(parent)
        yuki_label.set_label(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_use_markup(True)
        self.set_hexpand(True)
        self.set_xalign(0)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self._raise("YUKI.N > add to box", self)
