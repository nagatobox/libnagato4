
from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem
from libnagato4.popover.item.CheckIcon import HaruhiCheckIcon
from libnagato4.dispatch.SettingsDispatch import HaruhiSettigsDispatch


class AbstractMatchConfig(NagatoItem, HaruhiSettigsDispatch):

    MATCH_TARGET = "define match target here."

    def _on_clicked(self, button):
        yuki_data = self.SETTING_GROUP, self.SETTING_KEY, self.MATCH_TARGET
        self._raise("YUKI.N > settings", yuki_data)

    def _on_settings_changed(self, value):
        self._check_icon.set_icon(value)

    def _on_initialize(self):
        self._bind_settings_dispatch()
        self._check_icon = HaruhiCheckIcon(self.MATCH_TARGET)
        self._check_icon.set_icon(self._get_current_settings())
        self.set_image(self._check_icon)
        self.set_image_position(Gtk.PositionType.LEFT)
