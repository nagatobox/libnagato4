
from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem
from libnagato4.popover.item.CheckIcon import HaruhiCheckIcon
from libnagato4.dispatch.SettingsDispatch import HaruhiSettigsDispatch


class NagatoCheckIconItem(NagatoItem, HaruhiSettigsDispatch):

    TITLE = "Define Title Here."
    SETTING_GROUP = "Define Settings Group Here."
    SETTING_KEY = "Define Settings Key Here"
    SETTING_DEFAULT = True
    MATCH_TARGET = True

    def _on_settings_changed(self, value):
        self._check_icon.set_icon(self._get_current_settings())

    def _on_initialize(self):
        self._bind_settings_dispatch()
        self._check_icon = HaruhiCheckIcon(self.MATCH_TARGET)
        self._check_icon.set_icon(self._get_current_settings())
        self.set_image(self._check_icon)
        self.set_image_position(Gtk.PositionType.LEFT)
