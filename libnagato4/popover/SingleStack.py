
from gi.repository import Gtk
from libnagato4.popover.Popover import NagatoPopover


class AbstractSingleStack(NagatoPopover):

    def _yuki_n_add_to_box(self, widget):
        self._box.add(widget)

    def _add_to_single_stack(self):
        raise NotImplementedError

    def _on_initialize(self):
        self._box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.add(self._box)
        self._add_to_single_stack()
