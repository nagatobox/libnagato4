
from gi.repository import Gdk

IGNORE = [Gdk.EventType._2BUTTON_PRESS, Gdk.EventType._3BUTTON_PRESS]


class HaruhiClickCenceller:

    def _on_button_press(self, spin_button, event):
        if event.type in IGNORE:
            return True

    def __init__(self, button):
        button.connect("button-press-event", self._on_button_press)
