
from libnagato4.Ux import Unit

LINE_WIDTH = Unit(1)/4
OFFSET = Unit(2)


class HaruhiLines:

    def _paint_line(self, cairo_context, y, value):
        cairo_context.set_line_width(LINE_WIDTH)
        cairo_context.move_to(OFFSET, y)
        cairo_context.line_to(OFFSET+value, y)
        cairo_context.stroke()

    def paint(self, cairo_context, width, height, value):
        yuki_y = height/2
        self._paint_line(cairo_context, yuki_y, width-OFFSET*2)
