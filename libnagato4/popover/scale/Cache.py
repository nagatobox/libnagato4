
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter


class NagatoCache(NagatoObject):

    def register_data_object(self, object_):
        self._transmitter.register_listener(object_)

    def try_set_cache(self, rate):
        if self._rate != rate:
            self._rate = rate
            self._transmitter.transmit("rate changed")

    def reset(self):
        yuki_rate = float(self._enquiry("YUKI.N > progress"))
        self.try_set_cache(yuki_rate)

    def __init__(self, parent):
        self._parent = parent
        self._rate = float(self._enquiry("YUKI.N > progress"))
        self._transmitter = HaruhiTransmitter()

    @property
    def progress(self):
        return self._rate
