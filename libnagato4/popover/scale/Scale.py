
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagato4.popover.scale.Data import NagatoData


class AbstractScale(NagatoObject):

    SIZE = Unit(30), Unit(3)
    MESSAGE = "YUKI.N > settings"
    CONFIG_QUERY = "css", "window_opacity"
    LABEL_FORMAT = "Opacity: {:.0%}"

    def _yuki_n_progress(self, progress):
        yuki_data = self.CONFIG_QUERY+(str(progress),)
        self._raise(self.MESSAGE, yuki_data)

    def _inform_progress(self):
        return self._enquiry(self.MESSAGE, self.CONFIG_QUERY+(0.9,))

    def _inform_label_format(self):
        return self.LABEL_FORMAT

    def _inform_size(self):
        return self.SIZE

    def __init__(self, parent):
        self._parent = parent
        NagatoData(self)
