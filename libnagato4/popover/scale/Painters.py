
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagato4.popover.scale.RGBA import NagatoRGBA
from libnagato4.popover.scale.Arc import HaruhiArc
from libnagato4.popover.scale.Lines import HaruhiLines

OFFSET = Unit(2)


class NagatoPainters(NagatoObject):

    def _on_draw(self, drawing_area, cairo_context):
        self._rgba.reset(cairo_context)
        yuki_width = drawing_area.get_allocated_width()
        yuki_height = drawing_area.get_allocated_height()
        yuki_rate = self._enquiry("YUKI.N > rate")
        yuki_value = (yuki_width-OFFSET*2)*yuki_rate
        self._lines.paint(cairo_context, yuki_width, yuki_height, yuki_value)
        self._arc.paint(cairo_context, yuki_height, yuki_value)

    def __init__(self, parent):
        self._parent = parent
        self._rgba = NagatoRGBA(self)
        self._lines = HaruhiLines()
        self._arc = HaruhiArc()
        yuki_drawing_area = self._enquiry("YUKI.N > drawing area")
        yuki_drawing_area.connect("draw", self._on_draw)
