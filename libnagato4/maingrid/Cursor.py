
from gi.repository import Gdk
from libnagato4.widget.Cursor import NagatoCursor


class NagatoCursor(NagatoCursor):

    EDGES = [
        ["nw-resize", "default", "ne-resize"],
        ["default", "default", "default"],
        ["sw-resize", "default", "se-resize"]
        ]

    def _is_void(self):
        yuki_gdk_window, _, _ = Gdk.Window.at_pointer()
        if yuki_gdk_window is None or len(yuki_gdk_window.get_children()) == 0:
            return True
        return False

    def _on_motion_notify(self, widget, event):
        if self._is_void():
            yuki_edge = "default"
        else:
            yuki_edge = self._get_edge(widget, event)
        yuki_gdk_window = widget.get_window()
        yuki_display = Gdk.Display.get_default()
        yuki_cursor = Gdk.Cursor.new_from_name(yuki_display, yuki_edge)
        yuki_gdk_window.set_cursor(yuki_cursor)
