
from libnagato4.widget.EventControl import AbstractEventControl
from libnagato4.maingrid.Cursor import NagatoCursor
from libnagato4.maingrid.LeftButton import NagatoLeftButton
from libnagato4.maingrid.RightButton import NagatoRightButton


class NagatoEventControl(AbstractEventControl):

    def _on_initialize(self):
        NagatoCursor(self)
        NagatoLeftButton(self)
        NagatoRightButton(self)
