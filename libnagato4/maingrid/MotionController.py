
from gi.repository import Gtk


class HaruhiMotionController:

    def _on_motion(self, controller_motion, x, y):
        print("motion", x, y)

    def _on_leave(self, controller_motion):
        print("leave")

    def __init__(self, grid):
        yuki_motion_controller = Gtk.EventControllerMotion.new(grid)
        print(yuki_motion_controller)
        yuki_motion_controller.connect("enter", self._on_motion)
        yuki_motion_controller.connect("leave", self._on_leave)
        yuki_motion_controller.connect("motion", self._on_motion)
