
from gi.repository import Gdk
from libnagato4.widget.LeftButton import NagatoLeftButton

EDGE = Gdk.WindowEdge


class NagatoLeftButton(NagatoLeftButton):

    EDGES = [
        [EDGE.NORTH_WEST, None, EDGE.NORTH_EAST],
        [None, None, None],
        [EDGE.SOUTH_WEST, None, EDGE.SOUTH_EAST]
        ]

    def _on_button_press(self, widget, event):
        yuki_gdk_window, yuki_x, yuki_y = Gdk.Window.at_pointer()
        if len(yuki_gdk_window.get_children()) == 0:
            return True
        if event.button == 1:
            self._on_left_click(widget, event)
