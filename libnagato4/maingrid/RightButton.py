
from gi.repository import Gdk
from libnagato4.Object import NagatoObject


class NagatoRightButton(NagatoObject):

    def _on_right_click(self, widget, event):
        yuki_rectangle = Gdk.Rectangle()
        yuki_rectangle.x = event.x
        yuki_rectangle.y = event.y
        self._popover.set_relative_to(widget)
        self._popover.set_pointing_to(yuki_rectangle)
        self._popover.show_all()

    def _on_button_press(self, widget, event):
        yuki_gdk_window, yuki_x, yuki_y = Gdk.Window.at_pointer()
        if len(yuki_gdk_window.get_children()) == 0:
            return True
        if event.button == 3:
            self._on_right_click(widget, event)

    def __init__(self, parent):
        self._parent = parent
        self._popover = self._enquiry("YUKI.N > main popover")
        yuki_source = self._enquiry("YUKI.N > event source")
        yuki_source.connect("button-press-event", self._on_button_press)
