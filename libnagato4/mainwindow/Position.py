
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoPosition(NagatoObject):

    def save(self):
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_x, yuki_y = yuki_window.get_position()
        yuki_position_data = "window", "position", [yuki_x, yuki_y]
        self._raise("YUKI.N > settings", yuki_position_data)

    def __init__(self, parent):
        self._parent = parent
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_query = "window", "position", ["center", "center"]
        yuki_position = self._enquiry("YUKI.N > settings", yuki_query)
        if yuki_position[0] == "center":
            yuki_window.set_position(Gtk.WindowPosition.CENTER)
        else:
            yuki_window.move(yuki_position[0], yuki_position[1])
