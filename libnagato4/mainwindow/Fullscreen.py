
from gi.repository import Gdk
from libnagato4.Object import NagatoObject

STATE = Gdk.WindowState.FULLSCREEN


class NagatoFullscreen(NagatoObject):

    def toggle(self):
        yuki_toggled = self._enquiry("YUKI.N > gdk window state", STATE)
        yuki_method_name = "unfullscreen" if yuki_toggled else "fullscreen"
        self._raise("YUKI.N > call gdk window", yuki_method_name)

    def __init__(self, parent):
        self._parent = parent
