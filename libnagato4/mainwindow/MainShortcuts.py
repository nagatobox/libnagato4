
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.mainwindow.MainShortcutsYaml import NagatoMainShortcutsYaml


class NagatoMainShortcuts(NagatoObject):

    def _on_key_press(self, window, event):
        if not self._enquiry("YUKI.N > enable global key shortcuts"):
            return False
        yuki_label = Gtk.accelerator_get_label(event.keyval, event.state)
        yuki_action = self._yaml.get_action_for_label(yuki_label)
        if yuki_action is not None:
            self._raise("YUKI.N > action", yuki_action)
            # return True for key input interception.
            return True

    def __init__(self, parent):
        self._parent = parent
        self._yaml = NagatoMainShortcutsYaml(self)
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_window.connect("key-press-event", self._on_key_press)
