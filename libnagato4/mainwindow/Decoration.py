
from libnagato4.Object import NagatoObject


class NagatoDecoration(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_icon_name = self._enquiry("YUKI.N > data", "icon-name")
        yuki_window.set_icon_name(yuki_icon_name)
        yuki_window.set_decorated(False)
