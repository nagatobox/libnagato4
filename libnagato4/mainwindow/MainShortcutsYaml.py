
from libnagato4.application.Yaml import NagatoYaml


class NagatoMainShortcutsYaml(NagatoYaml):

    RESOURCE = "global_shortcuts.yaml"
    QUERY_MESSAGE = "YUKI.N > local file"

    def get_action_for_label(self, label):
        if label in self._binds:
            return self._binds[label]
        return None

    def _on_initialize(self):
        self._binds = {}
        for yuki_item in self._yaml:
            self._binds[yuki_item["keybind"]] = yuki_item["action"]
