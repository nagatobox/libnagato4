
from libnagato4.Object import NagatoObject
from libnagato4.mainwindow.PosixSignal import HaruhiPosixSignal
from libnagato4.mainwindow.MainShortcuts import NagatoMainShortcuts


class NagatoEventControl(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        HaruhiPosixSignal()
        NagatoMainShortcuts(self)
