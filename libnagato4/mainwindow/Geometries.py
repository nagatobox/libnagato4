
from libnagato4.Object import NagatoObject
from libnagato4.mainwindow.Size import NagatoSize
from libnagato4.mainwindow.Position import NagatoPosition


class NagatoGeometries(NagatoObject):

    def _on_delete(self, widget, event):
        self._size.save()
        self._position.save()

    def __init__(self, parent):
        self._parent = parent
        self._size = NagatoSize(self)
        self._position = NagatoPosition(self)
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_window.connect("delete-event", self._on_delete)
