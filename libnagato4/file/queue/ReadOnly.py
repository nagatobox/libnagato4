
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagato4.file.queue import Priority

ATTRIBUTE = "access::can-write"
FLAG = Gio.FileQueryInfoFlags.NONE


class NagatoReadOnly(NagatoObject):

    def filetest(self, gio_file):
        yuki_file_info = gio_file.query_info(ATTRIBUTE, FLAG, None)
        if yuki_file_info.get_attribute_boolean(ATTRIBUTE):
            self._raise("YUKI.N > append", (Priority.WRITABLE, gio_file))
        else:
            self._raise("YUKI.N > append", (Priority.READ_ONLY, gio_file))

    def __init__(self, parent):
        self._parent = parent
