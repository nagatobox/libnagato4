
from gi.repository import GLib


def shorten(directory):
    yuki_home = GLib.get_home_dir()
    if not directory.startswith(yuki_home):
        return directory
    if directory == yuki_home:
        return "~/"
    return "~"+directory[len(yuki_home):]


def lengthen(directory):
    yuki_home = GLib.get_home_dir()
    if not directory.startswith("~/"):
        return directory
    return yuki_home+directory[1:]
