
from gi.repository import GLib

DESKTOP = GLib.get_user_special_dir(0)
DOCUMENTS = GLib.get_user_special_dir(1)
DOWNLOAD = GLib.get_user_special_dir(2)
MUSIC = GLib.get_user_special_dir(3)
PICTURES = GLib.get_user_special_dir(4)
PUBLIC_SHARE = GLib.get_user_special_dir(5)
TEMPLATES = GLib.get_user_special_dir(6)
VIDEOS = GLib.get_user_special_dir(7)
