
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.file.copy.ParseDirectory import NagatoParseDirectory


class NagatoEnsureDirectory(NagatoObject):

    def ensure(self, fullpath):
        yuki_target = self._enquiry("YUKI.N > target path", fullpath)
        if not GLib.file_test(yuki_target, GLib.FileTest.EXISTS):
            yuki_gio_file = Gio.File.new_for_path(yuki_target)
            yuki_gio_file.make_directory(None)
        self._parse_directory.parse(fullpath)

    def __init__(self, parent):
        self._parent = parent
        self._parse_directory = NagatoParseDirectory(self)
