
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.file.copy.Validation import NagatoValidation


class NagatoRecursive(NagatoObject):

    def _inform_source_path(self, file_basename):
        return GLib.build_filenamev([self._source, file_basename])

    def _inform_target_path(self, source_fullpath):
        yuki_path = source_fullpath[len(self._source):]
        return GLib.build_filenamev([self._target, yuki_path])

    def __init__(self, source, target):
        self._parent = None
        self._source = source
        self._target = target
        NagatoValidation.new_with_source_path(self, source)
