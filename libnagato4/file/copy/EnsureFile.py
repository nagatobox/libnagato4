
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoEnsureFile(NagatoObject):

    def ensure(self, fullpath):
        yuki_target = self._enquiry("YUKI.N > target path", fullpath)
        if GLib.file_test(yuki_target, GLib.FileTest.EXISTS):
            return
        yuki_source_file = Gio.File.new_for_path(fullpath)
        yuki_target_file = Gio.File.new_for_path(yuki_target)
        yuki_source_file.copy(yuki_target_file, Gio.FileCopyFlags.NONE)

    def __init__(self, parent):
        self._parent = parent
