
from libnagato4.Object import NagatoObject


class AbstractReplacement(NagatoObject):

    def _get_old(self):
        raise NotImplementedError

    def _get_new(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        yuki_old = self._get_old()
        yuki_new = self._get_new()
        self._raise("YUKI.N > add replacement", (yuki_old, yuki_new))
