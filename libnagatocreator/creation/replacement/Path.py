
from libnagatocreator.creation.app.App import AsakuraApp
from libnagatocreator.creation.replacement.ApplicationTemplate import (
    NagatoApplicationTemplate
    )
from libnagatocreator.creation.replacement.Replacements import (
    AbstractReplacements
    )


class NagatoReplacements(AbstractReplacements):

    def _on_start_iteration(self):
        AsakuraApp(self)
        NagatoApplicationTemplate(self)
