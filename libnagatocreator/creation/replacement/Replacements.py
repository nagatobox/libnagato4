
from libnagato4.Object import NagatoObject


class AbstractReplacements(NagatoObject):

    def _on_start_iteration(self):
        raise NotImplementedError

    def _yuki_n_add_replacement(self, user_data):
        self._replacements.append(user_data)

    def __iter__(self):
        self._replacements.clear()
        self._on_start_iteration()
        return self

    def __next__(self):
        if len(self._replacements) == 0:
            raise StopIteration
        return self._replacements.pop()

    def __init__(self, parent):
        self._parent = parent
        self._replacements = []
