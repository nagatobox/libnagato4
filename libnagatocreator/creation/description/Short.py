
from libnagatocreator.model import Keys
from libnagatocreator.creation.replacement.Replacement import (
    AbstractReplacement
    )


class NagatoShort(AbstractReplacement):

    def _get_old(self):
        return "SHORT_DESCRIPTION"

    def _get_new(self):
        yuki_query = Keys.PAGE_ID_DESCRIPTION, Keys.DESCRIPTION_SHORT
        return self._enquiry("YUKI.N > model data", yuki_query)
