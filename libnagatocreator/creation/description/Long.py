
from libnagatocreator.model import Keys
from libnagatocreator.creation.replacement.Replacement import (
    AbstractReplacement
    )


class NagatoLong(AbstractReplacement):

    def _get_old(self):
        return "LONG_DESCRIPTION"

    def _get_new(self):
        yuki_query = Keys.PAGE_ID_DESCRIPTION, Keys.DESCRIPTION_LONG
        yuki_description = self._enquiry("YUKI.N > model data", yuki_query)
        yuki_description = yuki_description.replace("\n", "\n ")
        yuki_description = yuki_description.replace("\n \n", "\n .\n")
        return yuki_description
