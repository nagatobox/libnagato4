
from libnagatocreator.creation.release.DateYear import NagatoDateYear
from libnagatocreator.creation.release.DateFull import NagatoDateFull


class AsakuraRelease:

    def __init__(self, parent):
        NagatoDateYear(parent)
        NagatoDateFull(parent)
