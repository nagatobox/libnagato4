
from libnagato4.Object import NagatoObject
from libnagatocreator.creation.image.Icon import NagatoIcon
from libnagatocreator.creation.image.Wallpaper import NagatoWallpaper


class NagatoImage(NagatoObject):

    def copy(self):
        self._icon.copy()
        self._wallpaper.copy()

    def __init__(self, parent):
        self._parent = parent
        self._icon = NagatoIcon(self)
        self._wallpaper = NagatoWallpaper(self)
