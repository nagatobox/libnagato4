
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatocreator.model import Keys


class AbstractCopy(NagatoObject):

    SOURCE_KEY = "YUKI.N > do not look at me."

    def _get_target(self, child_path):
        yuki_query = Keys.PAGE_ID_APP, Keys.APP_DIRECTORY
        yuki_directory = self._enquiry("YUKI.N > model data", yuki_query)
        yuki_path = GLib.build_filenamev([yuki_directory, child_path])
        return Gio.File.new_for_path(yuki_path)

    def _get_source(self):
        yuki_query = Keys.PAGE_ID_VISUAL, self.SOURCE_KEY
        yuki_path = self._enquiry("YUKI.N > model data", yuki_query)
        return Gio.File.new_for_path(yuki_path)

    def __init__(self, parent):
        self._parent = parent
