
from gi.repository import Gio
from libnagatocreator.model import Keys
from libnagatocreator.creation.image.Copy import AbstractCopy


class NagatoIcon(AbstractCopy):

    SOURCE_KEY = Keys.VISUAL_ICON_URI

    def _get_app_id(self):
        yuki_query = Keys.PAGE_ID_APP, Keys.APP_ID
        return self._enquiry("YUKI.N > model data", yuki_query)

    def copy(self):
        yuki_source = self._get_source()
        yuki_target = self._get_target(self._get_app_id()+".svg")
        yuki_source.copy(yuki_target, Gio.FileCopyFlags.NONE)
