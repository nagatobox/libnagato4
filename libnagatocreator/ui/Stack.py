
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatocreator.model import Keys
from libnagatocreator.page.Pages import AsakuraPages


class NagatoStack(Gtk.Stack, NagatoObject):

    def _yuki_n_switch_stack_to(self, name):
        self.set_visible_child_name(name)

    def _yuki_n_add_named(self, user_data):
        yuki_widget, yuki_name = user_data
        self.add_named(yuki_widget, yuki_name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        self.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        AsakuraPages(self)
        self._raise("YUKI.N > attach main widget", self)
        self.set_visible_child_name(Keys.PAGE_ID_AUTHOR)
