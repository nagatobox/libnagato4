
from gi.repository import GLib
from libnagatocreator.model import Keys
from libnagatocreator.model.Model import AbstractModel


class NagatoAuthor(AbstractModel):

    ID = Keys.PAGE_ID_AUTHOR

    def _set_default(self, key, default):
        yuki_query = self.ID, key, default
        yuki_default = self._enquiry("YUKI.N > settings", yuki_query)
        self._data[key] = yuki_default

    def _on_initialize(self):
        self._set_default(Keys.AUTHOR_NAME, GLib.get_user_name())
        self._set_default(Keys.AUTHOR_MAIL_ADDRESS, "")
        self._set_default(Keys.AUTHOR_WEB_SITE, "")
        self._set_default(Keys.AUTHOR_ID_HEADER, "")
