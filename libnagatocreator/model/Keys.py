
PAGE_ID_AUTHOR = "group-author"
PAGE_ID_APP = "group-app"
PAGE_ID_VISUAL = "group-visual"
PAGE_ID_DESCRIPTION = "group-description"

AUTHOR_NAME = "author-name"
AUTHOR_MAIL_ADDRESS = "author-mail-address"
AUTHOR_WEB_SITE = "author-web-site"
AUTHOR_ID_HEADER = "author-id-header"

APP_DIRECTORY = "app-directory"
APP_NAME = "app-name"
APP_ID = "app-id"
APP_WEB_SITE = "app-web-site"

VISUAL_ICON_URI = "visual-icon-uri"
VISUAL_WALLPAPER_URI = "visual-wallpaper-uri"

DESCRIPTION_SHORT = "description-short"
DESCRIPTION_LONG = "description-long"
