
from libnagatocreator.model import Keys
from libnagatocreator.model.Model import AbstractModel


class NagatoVisual(AbstractModel):

    ID = Keys.PAGE_ID_VISUAL

    def _on_initialize(self):
        self._data[Keys.VISUAL_ICON_URI] = ""
        self._data[Keys.VISUAL_WALLPAPER_URI] = ""
