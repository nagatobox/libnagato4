
from libnagato4.dialog.Label import AbstractLabel
from libnagatocreator.model import Keys

TEMPLATE = \
    "your new project\n"\
    "<span size='x-large'>{}</span>\n"\
    "\n"\
    "is created at\n"\
    "\n"\
    "{}"


class NagatoLabel(AbstractLabel):

    def _get_app_name(self):
        yuki_query = Keys.PAGE_ID_APP, Keys.APP_NAME
        return self._enquiry("YUKI.N > model data", yuki_query)

    def _get_target_path(self):
        yuki_query = Keys.PAGE_ID_APP, Keys.APP_DIRECTORY
        return self._enquiry("YUKI.N > model data", yuki_query)

    def _get_markup(self):
        return TEMPLATE.format(self._get_app_name(), self._get_target_path())
