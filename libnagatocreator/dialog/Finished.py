
from libnagato4.Ux import Unit
from libnagato4.dialog.Dialog import AbstractDialog
from libnagatocreator.dialog.Image import NagatoImage
from libnagatocreator.dialog.Label import NagatoLabel
from libnagatocreator.dialog.ButtonBox import NagatoButtonBox


class NagatoFinished(AbstractDialog):

    SIZE = Unit(45), Unit(75)
    CSS = "dialog-label-about"

    def _yuki_n_loopback_add_content(self, parent):
        NagatoImage(parent)
        NagatoLabel(parent)
        NagatoButtonBox(parent)
