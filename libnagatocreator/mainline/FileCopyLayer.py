
from libnagato4.Object import NagatoObject
from libnagatocreator.ui.Stack import NagatoStack
from libnagatocreator.creation.Create import NagatoCreate
from libnagatocreator.dialog.Finished import NagatoFinished


class NagatoFileCopyLayer(NagatoObject):

    def _yuki_n_create_project(self):
        self._create.create_project()
        yuki_response = NagatoFinished.get_response(self)
        self._raise("YUKI.N > force quit application")

    def __init__(self, parent):
        self._parent = parent
        self._create = NagatoCreate(self)
        NagatoStack(self)
