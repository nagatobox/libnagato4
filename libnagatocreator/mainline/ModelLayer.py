
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagatocreator.model.Models import NagatoModels
from libnagatocreator.mainline.SaveLayer import NagatoSaveLayer


class NagatoModelLayer(NagatoObject):

    def _yuki_n_model_changed(self, user_data):
        self._transmitter.transmit(user_data)

    def _yuki_n_model_data(self, user_data):
        self._models.set_data(user_data)

    def _inform_model_data(self, user_data):
        return self._models.get_data(user_data)

    def _inform_model_appliable(self, page_id):
        return self._models.get_appliable(page_id)

    def _yuki_n_register_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._models = NagatoModels(self)
        NagatoSaveLayer(self)
