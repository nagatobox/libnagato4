
from libnagatocreator.model import Keys
from libnagatocreator.page.Page import AbstractPage
from libnagatocreator.page.visual.HeaderLabel import NagatoHeaderLabel
from libnagatocreator.page.visual.IconBox import NagatoIconBox
from libnagatocreator.page.visual.BackgroundBox import NagatoBackgroundBox
from libnagatocreator.page.Spacer import NagatoSpacer
from libnagatocreator.page.visual.ButtonBox import NagatoButtonBox


class NagatoVisualPage(AbstractPage):

    PAGE_ID = Keys.PAGE_ID_VISUAL

    def _yuki_n_loopback_set_widgets(self, parent):
        NagatoHeaderLabel(parent)
        NagatoIconBox(parent)
        NagatoBackgroundBox(parent)
        NagatoSpacer(parent)
        NagatoButtonBox(parent)
