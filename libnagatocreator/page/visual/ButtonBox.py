
from libnagatocreator.page.ButtonBox import AbstractButtonBox
from libnagatocreator.page.ButtonBoxSpacer import NagatoButtonBoxSpacer
from libnagatocreator.page.visual.BackButton import NagatoBackButton
from libnagatocreator.page.visual.NextButton import NagatoNextButton


class NagatoButtonBox(AbstractButtonBox):

    def _on_initialize(self):
        NagatoBackButton(self)
        NagatoButtonBoxSpacer(self)
        NagatoNextButton(self)
