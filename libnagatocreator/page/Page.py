
from libnagato4.Object import NagatoObject
from libnagatocreator.page.Box import NagatoBox


class AbstractPage(NagatoObject):

    PAGE_ID = ""

    def _inform_page_id(self):
        return self.PAGE_ID

    def _yuki_n_loopback_set_widgets(self, parent):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        NagatoBox(self)
