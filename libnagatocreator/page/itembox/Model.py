
from libnagato4.Object import NagatoObject
from libnagatocreator.page.itembox.Box import NagatoBox


class AbstractModel(NagatoObject):

    LABEL_TEXT = "YUKI.N > Don't look at me."
    CSS = "config-box-odd"
    DEFAULT_KEY = "YUKI.N > don't look at me."

    def _yuki_n_loopback_set_value_widget(self, parent):
        raise NotImplementedError

    def _inform_label_text(self):
        return self.LABEL_TEXT

    def _inform_default_key(self):
        return self.DEFAULT_KEY

    def _inform_css(self):
        return self.CSS

    def __init__(self, parent):
        self._parent = parent
        NagatoBox(self)
