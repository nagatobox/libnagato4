
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin


class AbstractEntry(Gtk.Entry, NagatoObject):

    def _on_initialize(self):
        pass

    def _on_changed(self, editable):
        yuki_group = self._enquiry("YUKI.N > page id")
        yuki_key = self._enquiry("YUKI.N > default key")
        yuki_data = yuki_group, yuki_key, self.get_text()
        self._raise("YUKI.N > model data", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self)
        self.connect("changed", self._on_changed)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self._on_initialize()
        self._raise("YUKI.N > add to box", self)
