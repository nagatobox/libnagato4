
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject
from libnagatocreator.page.itembox.Label import NagatoLabel


class NagatoBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_homogeneous(True)
        self.set_size_request(-1, Unit(5))
        yuki_css = self._enquiry("YUKI.N > css")
        self._raise("YUKI.N > css", (self, yuki_css))
        NagatoLabel(self)
        self._raise("YUKI.N > loopback set value widget", self)
        self._raise("YUKI.N > add to box", self)
