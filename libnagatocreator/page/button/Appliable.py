

class ShamisenAppliable:

    def receive_transmission(self, user_data):
        yuki_group, yuki_key, yuki_value, yuki_appliable = user_data
        yuki_id = self._enquiry("YUKI.N > page id")
        if yuki_id == yuki_group:
            self.set_sensitive(yuki_appliable)

    def _bind_appliable(self, default_sensitive=False):
        self._raise("YUKI.N > register model object", self)
        self.set_sensitive(default_sensitive)
