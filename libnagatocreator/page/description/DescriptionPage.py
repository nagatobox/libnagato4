
from libnagatocreator.model import Keys
from libnagatocreator.page.Page import AbstractPage
from libnagatocreator.page.description.HeaderLabel import NagatoHeaderLabel
from libnagatocreator.page.description.Long import NagatoLong
from libnagatocreator.page.Spacer import NagatoSpacer
from libnagatocreator.page.description.ButtonBox import NagatoButtonBox
from libnagatocreator.page.description.ShortDescription import (
    NagatoShortDescription
    )


class NagatoDescriptionPage(AbstractPage):

    PAGE_ID = Keys.PAGE_ID_DESCRIPTION

    def _yuki_n_loopback_set_widgets(self, parent):
        NagatoHeaderLabel(parent)
        NagatoShortDescription(parent)
        NagatoLong(parent)
        NagatoSpacer(parent)
        NagatoButtonBox(parent)
