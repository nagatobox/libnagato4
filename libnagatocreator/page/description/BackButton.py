
from gi.repository import Gtk
from libnagato4.widget import Margin
from libnagato4.Object import NagatoObject
from libnagatocreator.model import Keys


class NagatoBackButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        self._raise("YUKI.N > switch stack to", Keys.PAGE_ID_VISUAL)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, "Back (Visual)")
        self.connect("clicked", self._on_clicked)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self._raise("YUKI.N > add to box", self)
