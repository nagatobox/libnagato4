
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin
from libnagatocreator.page.description.TextView import NagatoTextView


class NagatoScrolledWindow(Gtk.ScrolledWindow, NagatoObject):

    def _yuki_n_add_to_scrolled_window(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        self.set_size_request(-1, Unit(20))
        Margin.set_with_unit(self, 1, 1, 1, 1)
        NagatoTextView(self)
        self._raise("YUKI.N > add to box", self)
