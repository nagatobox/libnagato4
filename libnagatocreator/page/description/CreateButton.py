
from gi.repository import Gtk
from libnagato4.widget import Margin
from libnagato4.Object import NagatoObject
from libnagatocreator.page.button.Appliable import ShamisenAppliable


class NagatoCreateButton(Gtk.Button, NagatoObject, ShamisenAppliable):

    def _on_clicked(self, button):
        self._raise("YUKI.N > save model data")
        self._raise("YUKI.N > create project")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, "Create Application")
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > css", (self, "action-button"))
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self._bind_appliable()
        self._raise("YUKI.N > add to box", self)
