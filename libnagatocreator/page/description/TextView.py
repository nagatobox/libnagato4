
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoTextView(Gtk.TextView, NagatoObject):

    def _on_changed(self, buffer_):
        yuki_group = self._enquiry("YUKI.N > page id")
        yuki_key = self._enquiry("YUKI.N > default key")
        yuki_data = yuki_group, yuki_key, buffer_.props.text
        self._raise("YUKI.N > model data", yuki_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.TextView.__init__(self)
        yuki_buffer = self.get_buffer()
        yuki_buffer.connect("changed", self._on_changed)
        self._raise("YUKI.N > add to scrolled window", self)
