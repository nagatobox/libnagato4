
from libnagatocreator.model import Keys
from libnagatocreator.page.textbox.Model import AbstractModel
from libnagatocreator.page.description.ScrolledWindow import (
    NagatoScrolledWindow
    )


class NagatoLong(AbstractModel):

    CSS = "config-box-even"
    LABEL_TEXT = "Long Description"
    DEFAULT_KEY = Keys.DESCRIPTION_LONG

    def _yuki_n_loopback_add_text_widget(self, parent):
        NagatoScrolledWindow(parent)
