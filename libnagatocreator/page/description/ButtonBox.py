
from libnagatocreator.page.ButtonBox import AbstractButtonBox
from libnagatocreator.page.ButtonBoxSpacer import NagatoButtonBoxSpacer
from libnagatocreator.page.description.BackButton import NagatoBackButton
from libnagatocreator.page.description.CreateButton import NagatoCreateButton


class NagatoButtonBox(AbstractButtonBox):

    def _on_initialize(self):
        NagatoBackButton(self)
        NagatoButtonBoxSpacer(self)
        NagatoCreateButton(self)
