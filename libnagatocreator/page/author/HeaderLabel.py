
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject


class NagatoHeaderLabel(Gtk.Label, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, "Author")
        self.set_size_request(-1, Unit(5))
        self.set_margin_bottom(Unit(5))
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > css", (self, "config-box-odd"))
