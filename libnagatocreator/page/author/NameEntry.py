
from libnagatocreator.model import Keys
from libnagatocreator.page.entrybox.Model import AbstractModel


class NagatoNameEntry(AbstractModel):

    CSS = "config-box-odd"
    LABEL_TEXT = "Name"
    DEFAULT_KEY = Keys.AUTHOR_NAME
