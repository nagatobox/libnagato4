
from libnagatocreator.model import Keys
from libnagatocreator.page.entrybox.Model import AbstractModel


class NagatoEmailEntry(AbstractModel):

    CSS = "config-box-even"
    LABEL_TEXT = "E-Mail"
    DEFAULT_KEY = Keys.AUTHOR_MAIL_ADDRESS
