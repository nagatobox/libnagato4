
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatocreator.page.textbox.Label import NagatoLabel


class NagatoBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.set_homogeneous(False)
        yuki_css = self._enquiry("YUKI.N > css")
        self._raise("YUKI.N > css", (self, yuki_css))
        NagatoLabel(self)
        self._raise("YUKI.N > loopback add text widget", self)
        self._raise("YUKI.N > add to box", self)
