
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject


class AbstractButtonBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1, Unit(7))
        self._on_initialize()
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > css", (self, "config-box-odd"))
