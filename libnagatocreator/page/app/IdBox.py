
from libnagatocreator.model import Keys
from libnagatocreator.page.entrybox.Model import AbstractModel
from libnagatocreator.page.app.IdEntry import NagatoIdEntry


class NagatoIdBox(AbstractModel):

    CSS = "config-box-odd"
    LABEL_TEXT = "Application ID"
    DEFAULT_KEY = Keys.APP_ID

    def _yuki_n_loopback_set_value_widget(self, parent):
        NagatoIdEntry(parent)
