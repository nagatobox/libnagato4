
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin
from libnagato4.Ux import Unit
from libnagatocreator.page.app.ButtonImage import HaruhiButtonImage
from libnagatocreator.page.app.DataLink import NagatoDataLink


class NagatoDirectoryButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        self._raise("YUKI.N > button clicked")

    def _yuki_n_new_basename(self, basename):
        self.set_label(basename)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, "None")
        HaruhiButtonImage(self)
        NagatoDataLink(self)
        self.set_size_request(-1, Unit(5))
        self._raise("YUKI.N > css", (self, "action-button"))
        self.connect("clicked", self._on_clicked)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self._raise("YUKI.N > add to box", self)
