
from libnagatocreator.model import Keys
from libnagatocreator.page.itembox.Model import AbstractModel
from libnagatocreator.page.app.NameEntry import NagatoNameEntry


class NagatoNameBox(AbstractModel):

    CSS = "config-box-even"
    LABEL_TEXT = "Application Name"
    DEFAULT_KEY = Keys.APP_NAME

    def _yuki_n_loopback_set_value_widget(self, parent):
        NagatoNameEntry(parent)
