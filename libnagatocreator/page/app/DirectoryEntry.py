
from gi.repository import Gtk
from libnagato4.chooser.context.SelectDirectory import NagatoContext
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagatocreator.model import Keys
from libnagatocreator.page.itembox.Model import AbstractModel
from libnagatocreator.page.app.DirectoryButton import NagatoDirectoryButton


class NagatoDirectoryEntry(AbstractModel):

    CSS = "config-box-odd"
    LABEL_TEXT = "Application Directory"
    DEFAULT_KEY = Keys.APP_DIRECTORY

    def _yuki_n_loopback_set_value_widget(self, parent):
        NagatoDirectoryButton(parent)

    def _yuki_n_button_clicked(self):
        yuki_context = NagatoContext.new_for_directory(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return
        yuki_value = yuki_context["selection"]
        yuki_data = Keys.PAGE_ID_APP, self.DEFAULT_KEY, yuki_value
        self._raise("YUKI.N > model data", yuki_data)
        self._raise("YUKI.N > model data", yuki_data)
