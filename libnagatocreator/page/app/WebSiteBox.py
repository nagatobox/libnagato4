
from libnagatocreator.model import Keys
from libnagatocreator.page.entrybox.Model import AbstractModel
from libnagatocreator.page.app.WebSiteEntry import NagatoWebSiteEntry


class NagatoWebSiteBox(AbstractModel):

    CSS = "config-box-even"
    LABEL_TEXT = "Application Web Site"
    DEFAULT_KEY = Keys.APP_WEB_SITE

    def _yuki_n_loopback_set_value_widget(self, parent):
        NagatoWebSiteEntry(parent)
