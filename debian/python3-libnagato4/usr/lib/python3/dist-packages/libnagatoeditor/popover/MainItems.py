
from libnagato4.popover.action.New import NagatoNew
from libnagato4.popover.action.Open import NagatoOpen
from libnagato4.popover.action.Save import NagatoSave
from libnagato4.popover.action.SaveAs import NagatoSaveAs
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.editor.ToEditor import NagatoToEditor
from libnagato4.editor.popover.recent.ToRecent import NagatoToRecent


class AsakuraMainItems:

    def __init__(self, parent_box):
        NagatoNew(parent_box)
        NagatoOpen(parent_box)
        NagatoToRecent(parent_box)
        NagatoSave(parent_box)
        NagatoSaveAs(parent_box)
        NagatoSeparator(parent_box)
        NagatoToEditor(parent_box)
