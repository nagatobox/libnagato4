
from libnagato4.popover.editor.EditorBox import NagatoEditorBox
from libnagato4.editor.popover.recent.RecentBox import NagatoRecentBox


class AsakuraSubGroups:

    def __init__(self, parent_stack):
        NagatoEditorBox(parent_stack)
        NagatoRecentBox(parent_stack)
