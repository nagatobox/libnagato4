
from libnagato4.treeview.column.Zebra import HaruhiZebra
from libnagato4.treeview.column.Sortable import HaruhiSortable


class HaruhiSortableZebra(HaruhiZebra, HaruhiSortable):

    def _bind_sortable_zebra(self, renderer, sort_column_id):
        self._bind_zebra_column(renderer)
        self._bind_sortable_column(sort_column_id)
