
from gi.repository import Gtk
from libnagato4.Ux import Unit


class HaruhiRenderer(Gtk.CellRendererPixbuf):

    def __init__(self):
        Gtk.CellRendererPixbuf.__init__(self)
        self.set_property("xpad", Unit(1))
