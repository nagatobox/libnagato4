
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit


class NagatoGrid(Gtk.Grid, NagatoObject):

    def _yuki_n_attach_main_widget(self, widget):
        self.attach(widget, 0, 0, 1, 1)

    def _yuki_n_attach_to_grid(self, user_data):
        yuki_widget, yuki_geometries = user_data
        self.attach(yuki_widget, *yuki_geometries)

    def _initialize_grid(self):
        self.set_border_width(Unit("grid-spacing"))
        self.set_column_spacing(Unit("grid-spacing"))
        self.set_row_spacing(Unit("grid-spacing"))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(self)
        self._initialize_grid()
        self._raise("YUKI.N > add to event box", self)
        self._raise("YUKI.N > loopback add main widget", self)
        self._raise("YUKI.N > css", (self, "main-grid"))
