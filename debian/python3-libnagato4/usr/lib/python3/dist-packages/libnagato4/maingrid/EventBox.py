
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.maingrid.Grid import NagatoGrid
from libnagato4.maingrid.EventControl import NagatoEventControl
from libnagato4.maingrid.MotionController import HaruhiMotionController


class NagatoEventBox(Gtk.EventBox, NagatoObject):

    def _yuki_n_add_to_event_box(self, widget):
        self.add(widget)

    def _inform_event_box(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventBox.__init__(self)
        NagatoGrid(self)
        NagatoEventControl(self)
        self._raise("YUKI.N > add to box", self)
