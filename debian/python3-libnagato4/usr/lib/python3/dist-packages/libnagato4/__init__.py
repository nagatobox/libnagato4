
import gi
import gettext
import locale

gi.require_version('Gtk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('WebKit2', '4.0')
gi.require_version('GtkSource', '3.0')
gi.require_version('Poppler', '0.18')

locale.setlocale(locale.LC_ALL, None)
yuki_names = ('gettext', 'ngettext')
gettext.install("libnagato", "/usr/share/locale", names=yuki_names)
