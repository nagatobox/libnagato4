
from libnagato4.Object import NagatoObject
from libnagato4.popover.MainPopover import NagatoMainPopover


class AbstractMainPopoverLayer(NagatoObject):

    def _yuki_n_loopback_add_popover_item(self, parent_box):
        pass

    def _yuki_n_loopback_add_popover_group(self, parent_stack):
        pass

    def _inform_main_popover(self):
        return NagatoMainPopover(self)

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
