
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter


class AbstractNotificationLayer(NagatoObject):

    def _yuki_n_header_notify(self, user_data):
        self._transmitter.transmit(user_data)

    def _yuki_n_register_header_notifier_object(self, object_):
        self._transmitter.register_listener(object_)

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        self._on_initialize()
