
import heapq
from libnagato4.Object import NagatoObject
from libnagato4.file.queue.Parser import NagatoParser


class NagatoQueue(NagatoObject):

    def _yuki_n_append(self, user_data):
        heapq.heappush(self._priority_list, user_data)

    def pop_file(self):
        if self._priority_list:
            return heapq.heappop(self._priority_list)
        return None

    def queue(self):
        self._parser.queue()

    def __init__(self, parent):
        self._parent = parent
        self._priority_list = []
        self._parser = NagatoParser(self)
