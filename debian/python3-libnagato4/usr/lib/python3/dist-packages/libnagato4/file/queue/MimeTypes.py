
from gi.repository import Gio
from gi.repository import GLib

ATTRIBUTE = "standard::content-type"
FLAG = Gio.FileQueryInfoFlags.NONE


class HaruhiMimeTypes:

    def _get_attribute(self, gio_file):
        yuki_file_info = gio_file.query_info(ATTRIBUTE, FLAG, None)
        return yuki_file_info.get_attribute_as_string(ATTRIBUTE)

    def matches(self, gio_file):
        yuki_mime_type = self._get_attribute(gio_file)
        for _, yuki_pattern in self._mime_types.items():
            if GLib.regex_match_simple(yuki_pattern, yuki_mime_type, 0, 0):
                return True
        return False

    def __init__(self, mime_types):
        self._mime_types = mime_types
