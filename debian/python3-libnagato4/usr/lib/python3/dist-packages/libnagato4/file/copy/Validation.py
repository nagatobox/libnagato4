
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.file.copy.FileType import NagatoFileType


class NagatoValidation(NagatoObject):

    @classmethod
    def new_with_source_path(cls, parent, source_path):
        yuki_validation = NagatoValidation(parent)
        yuki_validation.check_file_validation(source_path)

    def check_file_validation(self, fullpath):
        yuki_basename = GLib.filename_display_basename(fullpath)
        if yuki_basename.startswith("."):
            return
        if GLib.file_test(fullpath, GLib.FileTest.EXISTS):
            self._file_type.check_file_type(fullpath)

    def _yuki_n_file_found(self, fullpath):
        self.check_file_validation(fullpath)

    def __init__(self, parent):
        self._parent = parent
        self._file_type = NagatoFileType(self)
