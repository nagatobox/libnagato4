
from gi.repository import Gio
from libnagato4.Object import NagatoObject

FLAG = Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS


class NagatoParseDirectory(NagatoObject):

    def parse(self, fullpath):
        yuki_gio_file = Gio.File.new_for_path(fullpath)
        yuki_enumerator = yuki_gio_file.enumerate_children("*", FLAG)
        while True:
            _, _, yuki_child = yuki_enumerator.iterate()
            if yuki_child is None:
                break
            self._raise("YUKI.N > file found", yuki_child.get_path())

    def __init__(self, parent):
        self._parent = parent
