
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.file.copy.EnsureDirectory import NagatoEnsureDirectory
from libnagato4.file.copy.EnsureFile import NagatoEnsureFile


class NagatoFileType(NagatoObject):

    def check_file_type(self, fullpath):
        if GLib.file_test(fullpath, GLib.FileTest.IS_DIR):
            self._ensure_directory.ensure(fullpath)
        else:
            self._ensure_file.ensure(fullpath)

    def __init__(self, parent):
        self._parent = parent
        self._ensure_directory = NagatoEnsureDirectory(self)
        self._ensure_file = NagatoEnsureFile(self)
