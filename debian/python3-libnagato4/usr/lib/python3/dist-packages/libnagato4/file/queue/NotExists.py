
from libnagato4.Object import NagatoObject
from libnagato4.file.queue.Unsupported import NagatoUnspported
from libnagato4.dialog.filesnotfound.FilesNotFound import NagatoFilesNotFound


class NagatoNotExists(NagatoObject):

    def _inform_files(self):
        return self._files

    def show_dialog(self):
        if self._files:
            NagatoFilesNotFound.get_response(self)
        self._unsupported.show_dialog()

    def filetest(self, gio_file):
        if not gio_file.query_exists(None):
            self._files.append(gio_file)
        else:
            self._unsupported.filetest(gio_file)

    def __init__(self, parent):
        self._parent = parent
        self._files = []
        self._unsupported = NagatoUnspported(self)
