
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagato4.file.queue.NotExists import NagatoNotExists


class NagatoParser(NagatoObject):

    def queue(self):
        for yuki_path in self._enquiry("YUKI.N > args", "files"):
            yuki_gio_file = Gio.File.new_for_path(yuki_path)
            self._not_exists.filetest(yuki_gio_file)
        self._not_exists.show_dialog()

    def __init__(self, parent):
        self._parent = parent
        self._not_exists = NagatoNotExists(self)
