
from libnagato4.Object import NagatoObject
from libnagato4.file.queue import Priority
from libnagato4.file.queue.ReadOnly import NagatoReadOnly
from libnagato4.file.queue.MimeTypes import HaruhiMimeTypes
from libnagato4.dialog.filesunsupported.Store import NagatoStore


class NagatoUnspported(NagatoObject):

    def show_dialog(self):
        self._store.show_dialog()

    def filetest(self, gio_file):
        if self._mime_types.matches(gio_file):
            self._read_only.filetest(gio_file)
        else:
            self._store.append(gio_file)
            self._raise("YUKI.N > append", (Priority.UNSUPPORTED, gio_file))

    def __init__(self, parent):
        self._parent = parent
        self._store = NagatoStore(self)
        yuki_mime_types = self._enquiry("YUKI.N > data", "mime-types")
        self._mime_types = HaruhiMimeTypes(yuki_mime_types)
        self._read_only = NagatoReadOnly(self)
