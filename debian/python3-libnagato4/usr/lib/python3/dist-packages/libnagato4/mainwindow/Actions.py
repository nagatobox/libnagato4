
from libnagato4.mainwindow.Fullscreen import NagatoFullscreen
from libnagato4.mainwindow.Maximized import NagatoMaximized
from libnagato4.dispatch.ActionDispatch import NagatoActionDispatch


class NagatoActions(NagatoActionDispatch):

    ACTIONS = ["toggle fullscreen", "toggle maximized", "quit"]

    def _toggle_fullscreen(self):
        self._fullscreen.toggle()

    def _toggle_maximized(self):
        self._maximized.toggle()

    def _quit(self):
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_window.close()

    def _on_initialize(self):
        self._fullscreen = NagatoFullscreen(self)
        self._maximized = NagatoMaximized(self)
