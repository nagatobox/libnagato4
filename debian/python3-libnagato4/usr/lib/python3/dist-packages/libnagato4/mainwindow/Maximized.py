
from gi.repository import Gdk
from libnagato4.Object import NagatoObject

STATE = Gdk.WindowState.MAXIMIZED


class NagatoMaximized(NagatoObject):

    def toggle(self):
        yuki_toggled = self._enquiry("YUKI.N > gdk window state", STATE)
        yuki_method_name = "unmaximize" if yuki_toggled else "maximize"
        self._raise("YUKI.N > call gdk window", yuki_method_name)

    def __init__(self, parent):
        self._parent = parent
