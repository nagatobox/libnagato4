
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.mainwindow.Attributes import NagatoAttributes
from libnagato4.mainwindow.GdkWindow import NagatoGdkWindow


class AbstractMainWindow(Gtk.Window, NagatoObject):

    def _yuki_n_add_to_window(self, widget):
        self.add(widget)

    def _inform_window(self):
        return self

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Window.__init__(self)
        self._on_initialize()
        NagatoAttributes(self)
        NagatoGdkWindow(self)
        self.show_all()
        Gtk.main()
