
from libnagato4.Object import NagatoObject
from libnagato4.mainwindow.Decoration import NagatoDecoration
from libnagato4.mainwindow.EventControl import NagatoEventControl
from libnagato4.mainwindow.Geometries import NagatoGeometries


class NagatoAttributes(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        NagatoGeometries(self)
        NagatoDecoration(self)
        NagatoEventControl(self)
