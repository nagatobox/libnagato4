
from libnagato4.Object import NagatoObject
from libnagato4.mainwindow.Actions import NagatoActions


class NagatoGdkWindow(NagatoObject):

    def _yuki_n_call_gdk_window(self, method_name):
        yuki_method = getattr(self._gdk_window, method_name)
        yuki_method()

    def _inform_gdk_window_state(self, query_state):
        yuki_state = self._gdk_window.get_state()
        return (yuki_state & query_state == query_state)

    def _on_realize(self, gtk_window):
        self._gdk_window = gtk_window.get_window()
        NagatoActions(self)

    def __init__(self, parent):
        self._parent = parent
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_window.connect("realize", self._on_realize)
