
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit


class NagatoSize(NagatoObject):

    def save(self):
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_w, yuki_h = yuki_window.get_size()
        yuki_size_data = "window", "size", [yuki_w, yuki_h]
        self._raise("YUKI.N > settings", yuki_size_data)

    def __init__(self, parent):
        self._parent = parent
        yuki_window = self._enquiry("YUKI.N > window")
        yuki_query = "window", "size", [Unit(100), Unit(80)]
        yuki_size = self._enquiry("YUKI.N > settings", yuki_query)
        yuki_window.set_default_size(yuki_size[0], yuki_size[1])
