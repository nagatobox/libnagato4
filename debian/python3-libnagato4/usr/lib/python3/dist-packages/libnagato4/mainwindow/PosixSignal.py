
import signal
from gi.repository import Gtk
from gi.repository import GLib


class HaruhiPosixSignal:

    PRIORITY = GLib.PRIORITY_DEFAULT
    SIGNAL = signal.SIGINT

    def _on_catch_signal(self):
        print("application killed by SIGINT")
        Gtk.main_quit()

    def __init__(self):
        GLib.unix_signal_add(
            self.PRIORITY,
            self.SIGNAL,
            self._on_catch_signal
            )
