
from libnagato4.Ux import Unit


def set_with_unit(widget, start, end, top, bottom):
    widget.set_margin_top(Unit(start))
    widget.set_margin_bottom(Unit(end))
    widget.set_margin_start(Unit(top))
    widget.set_margin_end(Unit(end))
