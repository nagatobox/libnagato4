
from libnagato4.widget.Edge import HaruhiEdge
from libnagato4.Object import NagatoObject


class NagatoLeftButton(NagatoObject, HaruhiEdge):

    def _on_left_click(self, widget, event):
        yuki_edge = self._get_edge(widget, event)
        yuki_window = widget.get_toplevel()
        yuki_drag_args = event.button, event.x_root, event.y_root, event.time
        if yuki_edge is None:
            yuki_window.begin_move_drag(*yuki_drag_args)
        else:
            yuki_window.begin_resize_drag(yuki_edge, *yuki_drag_args)

    def _on_button_press(self, widget, event):
        if event.button == 1:
            self._on_left_click(widget, event)

    def __init__(self, parent):
        self._parent = parent
        yuki_source = self._enquiry("YUKI.N > event source")
        yuki_source.connect("button-press-event", self._on_button_press)
