
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagato4.widget.Edge import HaruhiEdge


class NagatoCursor(NagatoObject, HaruhiEdge):

    EDGES = [
        ["nw-resize", "default", "ne-resize"],
        ["default", "default", "default"],
        ["sw-resize", "default", "se-resize"]
        ]

    def _on_motion_notify(self, widget, event):
        yuki_edge = self._get_edge(widget, event)
        yuki_gdk_window = widget.get_window()
        yuki_display = Gdk.Display.get_default()
        yuki_cursor = Gdk.Cursor.new_from_name(yuki_display, yuki_edge)
        yuki_gdk_window.set_cursor(yuki_cursor)

    def __init__(self, parent):
        self._parent = parent
        yuki_source = self._enquiry("YUKI.N > event source")
        yuki_source.connect("motion-notify-event", self._on_motion_notify)
