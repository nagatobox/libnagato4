
from gi.repository import Gdk
from libnagato4.Object import NagatoObject

EVENTS = Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.POINTER_MOTION_MASK


class AbstractEventControl(NagatoObject):

    EVENT_SOURCE_QUERY = "YUKI.N > event box"

    def _inform_event_source(self):
        return self._event_source

    def _on_initialize(self):
        raise NotImplementedError

    def _on_realize(self, widget):
        yuki_gdk_window = widget.get_window()
        yuki_gdk_window.set_events(EVENTS)
        self._on_initialize()

    def __init__(self, parent):
        self._parent = parent
        self._event_source = self._enquiry(self.EVENT_SOURCE_QUERY)
        self._event_source.connect("realize", self._on_realize)
