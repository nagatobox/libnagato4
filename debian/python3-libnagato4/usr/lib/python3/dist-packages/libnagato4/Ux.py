
from gi.repository import Gdk

BASE_UNIT = int(Gdk.Screen.get_default().get_resolution() / 12)
NAMES = {
    "button-width": 10,
    "grid-spacing": 2,
    "padding": 1,
    "margin": 0.5
    }


def Unit(arg):
    if isinstance(arg, int):
        return BASE_UNIT * arg
    elif arg in NAMES:
        return int(BASE_UNIT * NAMES[arg])
    else:
        return 0
