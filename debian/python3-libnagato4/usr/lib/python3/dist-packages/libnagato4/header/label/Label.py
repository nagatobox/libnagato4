
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagato4.header.label.Loader import NagatoLoader


class NagatoLabel(Gtk.Label, NagatoObject):

    HEIGHT = Unit(6)

    def _yuki_n_text(self, text):
        self.set_halign(Gtk.Align.START)
        self.set_label(text)

    def _yuki_n_text_center(self, text):
        self.set_halign(Gtk.Align.CENTER)
        self.set_label(text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_size_request(-1, self.HEIGHT)
        self.set_halign(Gtk.Align.CENTER)
        self.set_margin_start(Unit(2))
        self.set_hexpand(True)
        self._raise("YUKI.N > add to box", self)
        NagatoLoader(self)
