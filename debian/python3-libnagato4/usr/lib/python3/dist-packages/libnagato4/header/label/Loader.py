
from libnagato4.Object import NagatoObject
from libnagato4.header.label.SourceId import HaruhiSourceId
from libnagato4.header.label.Info import NagatoInfo
from libnagato4.header.label.Progress import NagatoProgress
from libnagato4.header.label.ApplicationName import NagatoApplicationName


class NagatoLoader(NagatoObject):

    def _yuki_n_started(self, id_):
        self._source_id.switch_to(id_)
        self._raise("YUKI.N > switch stack to", ("bold", 0))

    def _yuki_n_finished(self):
        self._source_id.remove_current()
        self._application_name.start()
        self._raise("YUKI.N > restore")

    def __init__(self, parent):
        self._parent = parent
        self._source_id = HaruhiSourceId()
        self._application_name = NagatoApplicationName(self)
        NagatoInfo(self)
        NagatoProgress(self)
        self._raise("YUKI.N > header notify", ("info", "...Ready ?"))
