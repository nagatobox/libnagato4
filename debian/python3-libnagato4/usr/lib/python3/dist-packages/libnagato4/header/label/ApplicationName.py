
from libnagato4.Object import NagatoObject


class NagatoApplicationName(NagatoObject):

    def start(self):
        yuki_name = self._enquiry("YUKI.N > data", "name")
        self._raise("YUKI.N > text center", yuki_name)

    def __init__(self, parent):
        self._parent = parent
