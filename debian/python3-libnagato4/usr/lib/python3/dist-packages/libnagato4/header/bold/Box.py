
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.header.bold.Icon import NagatoIcon
from libnagato4.header.bold.Label import NagatoLabel
from libnagato4.header.bold.MenuButton import NagatoMenuButton


class NagatoBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        NagatoIcon(self)
        NagatoLabel(self)
        NagatoMenuButton(self)
        self._raise("YUKI.N > add to event box", self)
        self._raise("YUKI.N > css", (self, "nagato-headerbar"))
