
from libnagato4.header.label.TimeOut import NagatoTimeOut


class NagatoProgress(NagatoTimeOut):

    INTERVAL = 500
    VALID_TYPES = ["progress"]

    def _timeout(self, message):
        yuki_string = "YUKI.N > "+message+"."*(self._count % 5)
        self._raise("YUKI.N > text", yuki_string)
        self._count += 1
        return True
