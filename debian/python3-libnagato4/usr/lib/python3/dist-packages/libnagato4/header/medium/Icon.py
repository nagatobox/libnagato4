
from gi.repository import Gtk
from libnagato4.header.Icon import AbstractIcon


class NagatoIcon(AbstractIcon):

    ICON_SIZE = Gtk.IconSize.SMALL_TOOLBAR
