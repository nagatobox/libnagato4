
from libnagato4.Object import NagatoObject
from libnagato4.header.bold.Bold import NagatoBold
from libnagato4.header.medium.Medium import NagatoMedium
from libnagato4.header.thin.Thin import NagatoThin
from libnagato4.header.invisible.Invisible import NagatoInvisible
from libnagato4.header.WindowState import NagatoWindowState
from libnagato4.header.Settings import NagatoSettings


class NagatoBoxes(NagatoObject):

    def _yuki_n_restore(self):
        self._settings.restore()

    def _initialize_boxes(self):
        NagatoBold(self)
        NagatoMedium(self)
        NagatoThin(self)
        NagatoInvisible(self)

    def __init__(self, parent):
        self._parent = parent
        self._initialize_boxes()
        NagatoWindowState(self)
        self._settings = NagatoSettings(self)
