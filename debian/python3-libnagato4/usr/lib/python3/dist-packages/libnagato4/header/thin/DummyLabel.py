
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit


class NagatoDummyLabel(Gtk.Label, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_size_request(-1, Unit(2))
        self._raise("YUKI.N > css", (self, "nagato-headerbar"))
        self._raise("YUKI.N > add to event box", self)
