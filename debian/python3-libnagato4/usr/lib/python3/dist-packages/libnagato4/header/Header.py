
from gi.repository import Gtk
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagato4.header.Boxes import NagatoBoxes

TRANSITION_TYPE = Gtk.StackTransitionType.SLIDE_UP


class NagatoHeader(Gtk.Stack, NagatoObject):

    def _yuki_n_add_named(self, user_data):
        yuki_widget, yuki_name = user_data
        self.add_named(yuki_widget, yuki_name)

    def _yuki_n_switch_stack_to(self, user_data):
        yuki_child_name, _ = user_data
        self.set_visible_child_full(yuki_child_name, TRANSITION_TYPE)

    def _inform_stack(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        self.set_transition_duration(250)
        self.set_homogeneous(False)
        NagatoBoxes(self)
        self._raise("YUKI.N > add to box", self)
