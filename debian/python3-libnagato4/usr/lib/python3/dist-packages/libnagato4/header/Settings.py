
from libnagato4.Object import NagatoObject
from libnagato4.dispatch.SettingsDispatch import HaruhiSettigsDispatch


class NagatoSettings(NagatoObject, HaruhiSettigsDispatch):

    SETTING_GROUP = "header"
    SETTING_KEY = "type"
    SETTING_DEFAULT = "bold"

    def _restore(self, type_):
        self._raise("YUKI.N > switch stack to", (type_, 0))

    def _on_settings_changed(self, type_):
        self._restore(type_)

    def restore(self):
        self._restore(self._get_current_settings())

    def __init__(self, parent):
        self._parent = parent
        self._bind_settings_dispatch()
        self._restore(self._get_current_settings())
