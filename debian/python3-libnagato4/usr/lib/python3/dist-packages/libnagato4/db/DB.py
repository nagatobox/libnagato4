
from libnagato4.Object import NagatoObject
from libnagato4.db.Address import NagatoAddress
from libnagato4.db.Connection import NagatoConnection


class NagatoDB(NagatoObject):

    def close(self):
        self._connection.close()

    def execute(self, user_data):
        self._connection.execute(user_data)

    def select_all(self, user_data):
        return self._connection.select_all(user_data)

    def select_one(self, user_data):
        return self._connection.select_one(user_data)

    def _inform_created(self):
        return self._address.get_created()

    def _inform_address(self):
        return self._address.get_address()

    def _initialize_connection(self):
        self._connection = NagatoConnection(self)

    def __init__(self, parent):
        self._parent = parent
        self._address = NagatoAddress(self)
        self._initialize_connection()
