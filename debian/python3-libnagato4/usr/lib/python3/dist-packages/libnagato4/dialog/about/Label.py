
from libnagato4.dialog.Label import AbstractLabel

TEMPLATE = \
    "<span size='x-large'>{}</span>\n"\
    "version: {}\n"\
    "\n"\
    "{}"\
    "\n"\
    "{}"


class NagatoLabel(AbstractLabel):

    def _get_markup(self):
        return TEMPLATE.format(
            self._enquiry("YUKI.N > data", "name"),
            self._enquiry("YUKI.N > data", "version"),
            self._enquiry("YUKI.N > data", "long-description"),
            self._enquiry("YUKI.N > data", "authors"),
            )
