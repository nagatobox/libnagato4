
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit


class AbstractButtonBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_homogeneous(True)
        self.set_spacing(Unit(1))
        self.set_margin_bottom(Unit(1))
        self.set_margin_start(Unit(1))
        self.set_margin_end(Unit(1))
        self._on_initialize()
        self._raise("YUKI.N > add to dialog", self)
