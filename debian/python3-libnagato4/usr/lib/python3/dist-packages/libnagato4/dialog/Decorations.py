

class HaruhiDecorations:

    def __init__(self, dialog):
        dialog.set_size_request(*dialog.SIZE)
        dialog.set_decorated(False)
        dialog.set_skip_taskbar_hint(True)
        dialog.set_skip_pager_hint(True)
        dialog.set_keep_above(True)
