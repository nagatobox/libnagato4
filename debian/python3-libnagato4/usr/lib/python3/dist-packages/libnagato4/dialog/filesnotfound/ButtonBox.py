
from libnagato4.dialog.ButtonBox import AbstractButtonBox
from libnagato4.dialog.filesnotfound.CloseButton import NagatoCloseButton


class NagatoButtonBox(AbstractButtonBox):

    def _on_initialize(self):
        NagatoCloseButton(self)
