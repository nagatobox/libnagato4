
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit


class AbstractImage(Gtk.Image, NagatoObject):

    def _set_image(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self)
        self._set_image()
        self.set_padding(Unit(2), Unit(2))
        self.set_margin_top(Unit(2))
        self.set_margin_bottom(0)
        self.set_margin_start(Unit(2))
        self.set_margin_end(Unit(2))
        self._raise("YUKI.N > add to dialog", self)
