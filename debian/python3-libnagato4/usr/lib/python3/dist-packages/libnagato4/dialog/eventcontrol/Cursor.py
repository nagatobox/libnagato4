
from libnagato4.widget.Cursor import NagatoCursor
from libnagato4.Ux import Unit


class NagatoCursor(NagatoCursor):

    EDGES = [
        ["nw-resize", "n-resize", "ne-resize"],
        ["w-resize", "default", "e-resize"],
        ["sw-resize", "s-resize", "se-resize"]
        ]
    EDGE_START = Unit(1)
    EDGE_END = Unit(1)
    EDGE_TOP = Unit(1)
    EDGE_BOTTOM = Unit(1)
