
from libnagato4.dialog.Label import AbstractLabel

MARKUP = ""\
    "<span size='large' underline='single'>WARNING!!</span>\n"\
    "\n"\
    "We are going to close this tab. but...\n"\
    "This tab is untitled,\n"\
    "and THERE IS NO DATA ON YOUR DISC YET.\n"\
    "\n"\
    "if you select 'Close Without Save',\n"\
    " all your changes should be lost.\n"\
    "\n"


class NagatoLabel(AbstractLabel):

    CSS = "dialog-warning"

    def _get_markup(self):
        return MARKUP
