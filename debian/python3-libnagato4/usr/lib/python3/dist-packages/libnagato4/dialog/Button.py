
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit

HEIGHT = Unit(6)


class AbstractButton(Gtk.Button, NagatoObject):

    TITLE = "define title here."
    RESPONSE = Gtk.ResponseType.CANCEL
    CSS = None

    def _on_clicked(self, button):
        self._raise("YUKI.N > response", self.RESPONSE)

    def _on_initialize(self):
        self.set_size_request(-1, HEIGHT)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, self.TITLE, relief=Gtk.ReliefStyle.NONE)
        self._on_initialize()
        if self.CSS is not None:
            self._raise("YUKI.N > css", (self, self.CSS))
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
