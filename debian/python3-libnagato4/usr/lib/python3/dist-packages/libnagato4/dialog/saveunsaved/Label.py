
from libnagato4.dialog.Label import AbstractLabel

MARKUP = ""\
    "<span size='large' underline='single'>WARNING !!</span>\n"\
    "\n"\
    "We are going to close this tab, but...\n"\
    "YOUR CHANGES ARE NOT SAVED.\n"\
    "\n"\
    "if you select 'Close Without Save'\n"\
    "changes from <b>{}</b> will be permanently lost.\n"\
    "\n"\
    "Are you sure you want to close this tab ?\n"\
    "\n"


class NagatoLabel(AbstractLabel):

    CSS = "dialog-warning"

    def _get_markup(self):
        yuki_unsaved_until = self._enquiry("YUKI.N > unsaved until")
        return MARKUP.format(yuki_unsaved_until)
