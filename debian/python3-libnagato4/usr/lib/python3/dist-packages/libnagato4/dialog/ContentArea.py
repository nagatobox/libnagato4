
from libnagato4.Object import NagatoObject


class NagatoContentArea(NagatoObject):

    def _yuki_n_add_to_dialog(self, widget):
        self._content_area.add(widget)

    def __init__(self, parent):
        self._parent = parent
        yuki_dialog = self._enquiry("YUKI.N > dialog")
        self._content_area = yuki_dialog.get_content_area()
        self._content_area.set_border_width(0)
        self._raise("YUKI.N > css", (self._content_area, yuki_dialog.CSS))
        self._raise("YUKI.N > loopback add content", self)
