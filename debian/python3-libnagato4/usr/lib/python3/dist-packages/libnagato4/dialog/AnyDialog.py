
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class AbstractAnyDialog(Gtk.Dialog, NagatoObject):

    def _yuki_n_response(self, response):
        self.response(response)

    def _inform_dialog(self):
        return self

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Dialog.__init__(self, "", Gtk.Window(), Gtk.ResponseType.CANCEL)
        self._on_initialize()
