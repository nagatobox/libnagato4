
from gi.repository import Gtk
from libnagato4.dialog.Button import AbstractButton


class NagatoCancelButton(AbstractButton):

    RESPONSE = Gtk.ResponseType.CANCEL
    TITLE = "Cancel"
    CSS = "button-normal-on-warning"
