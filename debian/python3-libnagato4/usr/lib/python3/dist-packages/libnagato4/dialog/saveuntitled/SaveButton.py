
from libnagato4.dialog.Button import AbstractButton
from libnagato4.dialog import ResponseType


class NagatoSaveButton(AbstractButton):

    RESPONSE = ResponseType.SAVE
    TITLE = "Save"
    CSS = "button-normal-on-warning"
