
from gi.repository import Gdk
from libnagato4.widget.LeftButton import NagatoLeftButton
from libnagato4.Ux import Unit

EDGE = Gdk.WindowEdge


class NagatoLeftButton(NagatoLeftButton):

    EDGES = [
        [EDGE.NORTH_WEST, EDGE.NORTH, EDGE.NORTH_EAST],
        [EDGE.WEST, None, EDGE.EAST],
        [EDGE.SOUTH_WEST, EDGE.SOUTH, EDGE.SOUTH_EAST]
        ]
    EDGE_START = Unit(1)
    EDGE_END = Unit(1)
    EDGE_TOP = Unit(1)
    EDGE_BOTTOM = Unit(1)
