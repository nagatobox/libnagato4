
from libnagato4.dialog.Dialog import AbstractDialog
from libnagato4.dialog.warning.Image import NagatoImage
from libnagato4.dialog.contentsnotsaved.Label import NagatoLabel
from libnagato4.dialog.saveuntitled.ButtonBox import NagatoButtonBox
from libnagato4.dialog.MessageSender import NagatoMessageSender


class NagatoDialog(AbstractDialog):

    CSS = "dialog-warning"

    @classmethod
    def get_response_as_message(cls, parent):
        yuki_dialog = cls(parent)
        yuki_response = yuki_dialog.run()
        yuki_message_sender = NagatoMessageSender(parent)
        yuki_message_sender.send_message(yuki_response)
        yuki_dialog.destroy()

    def _yuki_n_loopback_add_content(self, parent):
        NagatoImage(parent)
        NagatoLabel(parent)
        NagatoButtonBox(parent)
