
from libnagato4.widget.EventControl import AbstractEventControl
from libnagato4.dialog.eventcontrol.Cursor import NagatoCursor
from libnagato4.dialog.eventcontrol.LeftButton import NagatoLeftButton


class NagatoEventControl(AbstractEventControl):

    EVENT_SOURCE_QUERY = "YUKI.N > dialog"

    def _on_initialize(self):
        NagatoCursor(self)
        NagatoLeftButton(self)
