
from libnagato4.Ux import Unit
from libnagato4.dialog.AnyDialog import AbstractAnyDialog
from libnagato4.dialog.ContentArea import NagatoContentArea
from libnagato4.dialog.Decorations import HaruhiDecorations
from libnagato4.dialog.eventcontrol.EventControl import NagatoEventControl


class AbstractDialog(AbstractAnyDialog):

    SIZE = Unit(40), Unit(40)
    CSS = "define css class here."

    @classmethod
    def get_response(cls, parent):
        yuki_dialog = cls(parent)
        yuki_response = yuki_dialog.run()
        yuki_dialog.destroy()
        return yuki_response

    def _yuki_n_loopback_add_content(self, parent):
        raise NotImplementedError

    def _on_initialize(self):
        NagatoContentArea(self)
        HaruhiDecorations(self)
        NagatoEventControl(self)
        self.show_all()
