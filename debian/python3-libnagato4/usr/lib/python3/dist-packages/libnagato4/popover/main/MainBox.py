
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.config.ToConfig import NagatoToConfig
from libnagato4.popover.main.ToggleMaximize import NagatoToggleMaximize
from libnagato4.popover.main.ToggleFullscreen import NagatoToggleFullscreen
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.main.About import NagatoAbout
from libnagato4.popover.main.Quit import NagatoQuit


class NagatoMainBox(NagatoBox):

    NAME = "main"

    def _on_initialize(self):
        self._raise("YUKI.N > loopback add popover item", self)
        NagatoToConfig(self)
        NagatoSeparator(self)
        NagatoToggleMaximize(self)
        NagatoToggleFullscreen(self)
        NagatoSeparator(self)
        NagatoAbout(self)
        NagatoQuit(self)
