
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit


class NagatoLabel(Gtk.Label, NagatoObject):

    def _set_text(self):
        yuki_percentage = self._enquiry("YUKI.N > rate")
        yuki_format = self._enquiry("YUKI.N > label format")
        yuki_text = yuki_format.format(yuki_percentage)
        self.set_text(yuki_text)

    def receive_transmission(self, user_data):
        self._set_text()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_margin_start(Unit(2))
        self._set_text()
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > register data object", self)
        self._raise("YUKI.N > css", (self, "popover-button"))
