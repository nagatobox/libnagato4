
from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem
from libnagato4.popover.editor.CheckIcon import HaruhiCheckIcon


class AbstractBooleanConfig(NagatoItem):

    GROUP = "editor"
    KEY = "define config key here"
    DEFAULT = False
    MATCH_TARGET = True

    def _get_settings(self):
        yuki_query = self.GROUP, self.KEY, self.DEFAULT
        return self._enquiry("YUKI.N > settings", yuki_query)

    def _on_clicked(self, button):
        yuki_settings = not self._get_settings()
        yuki_user_data = self.GROUP, self.KEY, yuki_settings
        self._raise("YUKI.N > settings", yuki_user_data)
        self._check_icon.set_icon(yuki_settings)

    def _on_map(self, *args):
        self._check_icon.set_icon(self._get_settings())

    def _on_initialize(self):
        self._check_icon = HaruhiCheckIcon(self.MATCH_TARGET)
        self.set_image(self._check_icon)
        self.set_image_position(Gtk.PositionType.LEFT)
