
from libnagato4.popover.item.Item import NagatoItem


class NagatoOpen(NagatoItem):

    TITLE = "Open"
    MESSAGE = "YUKI.N > action"
    USER_DATA = "open"

    def _on_initialize(self):
        self.props.tooltip_text = "Ctrl+O"
