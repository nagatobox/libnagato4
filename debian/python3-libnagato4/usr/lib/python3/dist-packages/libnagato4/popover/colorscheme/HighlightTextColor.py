
from libnagato4.popover.colorscheme.SelectColor import NagatoSelectColor


class NagatoHighlightTextColor(NagatoSelectColor):

    TITLE = "Highlight Text Color"
    USER_DATA = "css", "highlight_text_color"
