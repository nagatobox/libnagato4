
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.main.BackToMain import NagatoBackToMain
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.config.ColorSchemes import NagatoColorSchemes
from libnagato4.popover.config.HeaderSettings import AsakuraHeaderSettings
from libnagato4.popover.config.BackgroundSettings import (
    AsakuraBackgroundSettings
    )


class NagatoConfigBox(NagatoBox):

    NAME = "config"

    def _on_initialize(self):
        NagatoBackToMain(self)
        NagatoSeparator(self)
        NagatoColorSchemes(self)
        NagatoSeparator(self)
        AsakuraHeaderSettings(self)
        NagatoSeparator(self)
        AsakuraBackgroundSettings(self)
