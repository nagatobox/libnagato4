
from libnagato4.popover.scale.Label import NagatoLabel
from libnagato4.popover.scale.DrawingArea import NagatoDrawingArea


class AsakuraWidgets:

    def __init__(self, parent):
        NagatoLabel(parent)
        NagatoDrawingArea(parent)
