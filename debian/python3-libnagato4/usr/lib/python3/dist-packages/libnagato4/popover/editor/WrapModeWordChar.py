
from libnagato4.popover.editor.WrapMode import AbstractWrapMode


class NagatoWrapModeWordChar(AbstractWrapMode):

    TITLE = "Graphemes"
    MATCH_TARGET = 3
