
from libnagato4.popover.item.Item import NagatoItem


class NagatoNew(NagatoItem):

    TITLE = "New"
    MESSAGE = "YUKI.N > action"
    USER_DATA = "new"

    def _on_initialize(self):
        self.props.tooltip_text = "Ctrl+N"
