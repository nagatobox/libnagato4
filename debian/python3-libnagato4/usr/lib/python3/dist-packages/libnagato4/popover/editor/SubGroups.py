
from libnagato4.popover.editor.StyleSchemeBox import NagatoStyleSchemeBox
from libnagato4.popover.editor.WrapModeBox import NagatoWrapModeBox
from libnagato4.popover.editor.IndentBox import NagatoIndentBox
from libnagato4.popover.editor.ViewBox import NagatoViewBox


class AsakuraSubGroups:

    def __init__(self, parent):
        NagatoStyleSchemeBox(parent)
        NagatoWrapModeBox(parent)
        NagatoIndentBox(parent)
        NagatoViewBox(parent)
