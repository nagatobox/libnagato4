
from gi.repository import Gtk

ICON_SIZE = Gtk.IconSize.MENU
ICON_NAME = "object-select-symbolic"


class HaruhiCheckIcon(Gtk.Image):

    def set_icon(self, settings):
        if settings == self._match_target:
            self.set_from_icon_name(ICON_NAME, ICON_SIZE)
        else:
            self.set_from_icon_name(None, ICON_SIZE)

    def __init__(self, match_target):
        Gtk.Image.__init__(self)
        self._match_target = match_target
