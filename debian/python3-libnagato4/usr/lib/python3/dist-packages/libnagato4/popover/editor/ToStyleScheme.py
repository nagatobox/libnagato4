
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToStyleScheme(NagatoToStack):

    TITLE = "Style Scheme"
    WHERE_TO_GO = "style_scheme"
