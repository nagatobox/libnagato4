
from libnagato4.popover.item.Item import NagatoItem


class NagatoToggle(NagatoItem):

    TITLE = "Hide Header"
    MESSAGE = "YUKI.N > config"
    USER_DATA = "header", "revealed", "no"
