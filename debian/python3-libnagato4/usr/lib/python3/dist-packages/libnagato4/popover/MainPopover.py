
from libnagato4.popover.MultiStack import NagatoMultiStack
from libnagato4.popover.main.MainBox import NagatoMainBox
from libnagato4.popover.config.ConfigBox import NagatoConfigBox
from libnagato4.popover.config.HeaderStyleBox import NagatoHeaderStyleBox
from libnagato4.popover.colorscheme.ColorSchemeBox import NagatoColorSchemeBox


class NagatoMainPopover(NagatoMultiStack):

    def _yuki_n_loopback_add_popover_groups(self, parent):
        NagatoMainBox(parent)
        NagatoConfigBox(parent)
        NagatoHeaderStyleBox(parent)
        NagatoColorSchemeBox(parent)
        self._raise("YUKI.N > loopback add popover group", parent)
