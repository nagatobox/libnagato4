
from libnagato4.popover.editor.BooleanConfig import AbstractBooleanConfig


class NagatoRemoveTrailing(AbstractBooleanConfig):

    TITLE = "Remove Trailing When Saved"
    KEY = "remove_trailing"
    DEFAULT = False
