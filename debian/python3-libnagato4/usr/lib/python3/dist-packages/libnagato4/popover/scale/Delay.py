
from gi.repository import GLib
from libnagato4.Object import NagatoObject

DELAY = 100


class NagatoDelay(NagatoObject):

    def _on_idle(self, user_data):
        yuki_real_time = GLib.get_real_time()/1000
        yuki_diff = yuki_real_time-self._last_time
        if yuki_diff > DELAY:
            self._raise("YUKI.N > progress", user_data)
        return False

    def receive_transmission(self, user_data):
        self._last_time = GLib.get_real_time()/1000
        yuki_rate = self._enquiry("YUKI.N > rate")
        GLib.timeout_add(DELAY, self._on_idle, yuki_rate)

    def __init__(self, parent):
        self._parent = parent
        self._last_time = 0  # meaningless default number
        self._raise("YUKI.N > register data object", self)
