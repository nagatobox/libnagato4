
from libnagato4.popover.item.Item import NagatoItem


class NagatoSave(NagatoItem):

    TITLE = "Save"
    MESSAGE = "YUKI.N > action"
    USER_DATA = "save"

    def _on_initialize(self):
        self.props.tooltip_text = "Ctrl+S"
