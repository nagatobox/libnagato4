
from gi.repository import GLib
from libnagato4.Ux import Unit

VALUE_RGBA = 1, 1, 1, 1
OFFSET = Unit(2)
RADIUS = Unit(1)


class HaruhiArc:

    def paint(self, cairo_context, height, value):
        cairo_context.arc(OFFSET+value, height/2, RADIUS, 0, 2*GLib.PI)
        cairo_context.fill()
