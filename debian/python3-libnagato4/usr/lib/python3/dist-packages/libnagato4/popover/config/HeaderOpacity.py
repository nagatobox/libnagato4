
from libnagato4.Ux import Unit
from libnagato4.popover.scale.Scale import AbstractScale


class NagatoHeaderOpacity(AbstractScale):

    SIZE = Unit(30), Unit(3)
    MESSAGE = "YUKI.N > settings"
    CONFIG_QUERY = "css", "header_opacity"
    LABEL_FORMAT = "Opacity: {:.0%}"
