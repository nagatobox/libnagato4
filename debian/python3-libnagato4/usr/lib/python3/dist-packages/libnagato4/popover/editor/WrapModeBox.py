
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.editor.BackToEditor import NagatoBackToEditor
from libnagato4.popover.editor.WrapModeNone import NagatoWrapModeNone
from libnagato4.popover.editor.WrapModeChar import NagatoWrapModeChar
from libnagato4.popover.editor.WrapModeWord import NagatoWrapModeWord
from libnagato4.popover.editor.WrapModeWordChar import NagatoWrapModeWordChar


class NagatoWrapModeBox(NagatoBox):

    NAME = "wrap_mode"

    def _on_initialize(self):
        NagatoBackToEditor(self)
        NagatoSeparator(self)
        NagatoWrapModeNone(self)
        NagatoWrapModeChar(self)
        NagatoWrapModeWord(self)
        NagatoWrapModeWordChar(self)
