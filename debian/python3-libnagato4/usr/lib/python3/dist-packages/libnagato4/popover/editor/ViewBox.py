
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.editor.BackToEditor import NagatoBackToEditor
from libnagato4.popover.editor.ShowLineNumbers import NagatoShowLineNmbers
from libnagato4.popover.editor.HighlightCurrent import NagatoHighlightCurrent
from libnagato4.popover.editor.HighlightBrackets import NagatoHighlightBrackets
from libnagato4.popover.editor.ShowRightMargin import NagatoShowRightMargin
from libnagato4.popover.editor.RightMargin import NagatoRightMargin


class NagatoViewBox(NagatoBox):

    NAME = "view"

    def _on_initialize(self):
        NagatoBackToEditor(self)
        NagatoSeparator(self)
        NagatoShowLineNmbers(self)
        NagatoHighlightCurrent(self)
        NagatoHighlightBrackets(self)
        NagatoShowRightMargin(self)
        NagatoRightMargin(self)
