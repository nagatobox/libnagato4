
from libnagato4.popover.item.MatchConfig import AbstractMatchConfig


class AbstractWrapMode(AbstractMatchConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "wrap_mode"
    SETTING_DEFAULT = 0
