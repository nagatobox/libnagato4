
from gi.repository import Gtk
from gi.repository import Gdk
from libnagato4.popover.item.Item import NagatoItem
from libnagato4.chooser.color.Dialog import NagatoDialog
from libnagato4.chooser.context.HeaderColor import NagatoContext


class NagatoSelectColor(NagatoItem):

    TITLE = "Select Background Color"
    MESSAGE = "YUKI.N > settings"
    USER_DATA = "css", "header_background_color"

    def _select_color(self):
        yuki_rgba = Gdk.RGBA()
        yuki_rgba.parse(self._enquiry(self.MESSAGE, self.USER_DATA+("",)))
        yuki_context = NagatoContext.new(self, self.TITLE, yuki_rgba)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return
        yuki_rgb_string = yuki_context.get_rgba_string()
        self._raise(self.MESSAGE, self.USER_DATA+(yuki_rgb_string,))

    def _on_clicked(self, button):
        self._select_color()
