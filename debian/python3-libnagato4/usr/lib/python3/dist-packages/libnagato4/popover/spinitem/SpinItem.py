
from libnagato4.widget.HBox import AbstractHBox
from libnagato4.popover.spinitem.StaticLabel import NagatoStaticLabel
from libnagato4.popover.spinitem.Button import NagatoButton
from libnagato4.dispatch.SettingsDispatch import HaruhiSettigsDispatch


class AbstractSpinItem(AbstractHBox, HaruhiSettigsDispatch):

    SETTING_GROUP = "define group here."
    SETTING_KEY = "define key here."
    SETTING_DEFAULT = 0
    LABEL = "define title here."
    RANGE = 0, 100, 1

    def _yuki_n_value_changed(self, value):
        yuki_data = self.SETTING_GROUP, self.SETTING_KEY, value
        self._raise("YUKI.N > settings", yuki_data)

    def _on_settings_changed(self, value):
        self._button.set_value(value)

    def _on_initialize(self):
        self._raise("YUKI.N > css", (self, "popover-button"))
        NagatoStaticLabel.new_with_label(self, self.LABEL)
        self._button = NagatoButton.new_with_range(self, *self.RANGE)
        self._button.set_value(self._get_current_settings())
        self._bind_settings_dispatch()
        self._raise("YUKI.N > add to box", self)
