
from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem
from libnagato4.chooser.colorscheme.Dialog import NagatoDialog
from libnagato4.chooser.context.ColorScheme import NagatoContext


class NagatoColorSchemes(NagatoItem):

    TITLE = "Color Schemes"

    def _change_settings(self, changed_settings):
        for yuki_key, yuki_value in changed_settings.items():
            self._raise("YUKI.N > settings", ("css", yuki_key, yuki_value))

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        yuki_context = NagatoContext(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response == Gtk.ResponseType.APPLY:
            self._change_settings(yuki_context.get_changed())
