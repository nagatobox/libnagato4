
from libnagato4.popover.colorscheme.SelectColor import NagatoSelectColor


class NagatoHighlightColor(NagatoSelectColor):

    TITLE = "Highlight Color"
    USER_DATA = "css", "highlight_color"
