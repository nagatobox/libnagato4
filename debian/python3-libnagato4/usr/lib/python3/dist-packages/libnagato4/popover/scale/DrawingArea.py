
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.popover.scale.Painters import NagatoPainters
from libnagato4.popover.scale.Event import NagatoEvent


class NagatoDrawingArea(Gtk.DrawingArea, NagatoObject):

    def _inform_drawing_area(self):
        return self

    def receive_transmission(self, user_data):
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self)
        self.set_hexpand(True)
        NagatoPainters(self)
        NagatoEvent(self)
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > register data object", self)
