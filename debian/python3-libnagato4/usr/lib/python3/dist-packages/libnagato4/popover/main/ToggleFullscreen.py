
from libnagato4.popover.item.Item import NagatoItem


class NagatoToggleFullscreen(NagatoItem):

    TITLE = "Toggle Fullscreen"
    MESSAGE = "YUKI.N > action"
    USER_DATA = "toggle fullscreen"

    def _on_initialize(self):
        self.props.tooltip_text = "F11"
