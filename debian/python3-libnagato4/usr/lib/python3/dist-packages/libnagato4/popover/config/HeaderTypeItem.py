
from libnagato4.popover.item.MatchConfig import AbstractMatchConfig


class NagatoHeaderTypeItem(AbstractMatchConfig):

    SETTING_GROUP = "header"
    SETTING_KEY = "type"
    SETTING_DEFAULT = "bold"

    @classmethod
    def new_for_type(cls, parent, type_):
        yuki_item = cls(parent)
        yuki_item.set_scheme(type_)

    def _on_settings_changed(self, value):
        self._check_icon.set_icon(value)

    def set_scheme(self, type_):
        self.set_label(type_)
        self.MATCH_TARGET = type_
        yuki_setting = self._get_current_settings()
        self._check_icon.set_match_target(self.MATCH_TARGET, yuki_setting)
