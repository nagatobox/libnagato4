
from libnagato4.popover.editor.BooleanConfig import AbstractBooleanConfig


class NagatoSpacesAsTabs(AbstractBooleanConfig):

    TITLE = "Insert Spaces Instead of Tabs"
    KEY = "insert_spaces_instead_of_tabs"
    DEFAULT = True
