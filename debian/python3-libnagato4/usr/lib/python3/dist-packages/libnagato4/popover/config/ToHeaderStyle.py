
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToHeaderStyle(NagatoToStack):

    TITLE = "Style"
    WHERE_TO_GO = "header_style"
