
from libnagato4.popover.item.BackToStack import NagatoBackToStack


class NagatoBackToEditor(NagatoBackToStack):

    TITLE = "Back"
    WHERE_TO_GO = "editor"
