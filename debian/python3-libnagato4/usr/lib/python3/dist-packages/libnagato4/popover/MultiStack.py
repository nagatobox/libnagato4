
from libnagato4.popover.Popover import NagatoPopover
from libnagato4.popover.Stack import NagatoStack


class NagatoMultiStack(NagatoPopover):

    MAIN_STACK_NAME = "main"

    def _yuki_n_loopback_add_popover_groups(self, parent):
        raise NotImplementedError

    def _on_closed(self, popover):
        self._stack.set_visible_child_name(self.MAIN_STACK_NAME)

    def _on_initialize(self):
        self._stack = NagatoStack(self)
        self.connect("closed", self._on_closed)
