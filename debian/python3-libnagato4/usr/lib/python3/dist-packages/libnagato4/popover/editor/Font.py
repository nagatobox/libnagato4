

from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem
from libnagato4.chooser.font.Dialog import NagatoDialog
from libnagato4.chooser.context.Font import NagatoContext
from libnagato4.chooser.font import PangoFont


class NagatoFont(NagatoItem):

    TITLE = "Select Font"

    def _select(self):
        yuki_context = NagatoContext(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return
        yuki_font_desc = yuki_context["selection"][0]
        yuki_font_css = PangoFont.to_css(yuki_font_desc)
        yuki_settings = "css", "editor_font_name", yuki_font_css
        self._raise("YUKI.N > settings", yuki_settings)

    def _on_clicked(self, button):
        self._raise("YUKI.N > popdown")
        self._select()
