
from libnagato4.popover.item.Item import NagatoItem


class NagatoSaveAs(NagatoItem):

    TITLE = "Save As"
    MESSAGE = "YUKI.N > action"
    USER_DATA = "save as"

    def _on_initialize(self):
        self.props.tooltip_text = "Shift+Ctrl+S"
