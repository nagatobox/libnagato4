

from libnagato4.popover.item.StaticLabel import NagatoStaticLabel
from libnagato4.popover.header.Foreground import NagatoForeground
from libnagato4.popover.header.Background import NagatoBackground
from libnagato4.popover.header.HighlightColor import NagatoHighlightColor
from libnagato4.popover.header.HighlightTextColor import (
    NagatoHighlightTextColor
    )


class AsakuraColorScheme:

    def __init__(self, parent):
        NagatoStaticLabel.new_with_label(parent, "Color Scheme: ")
        NagatoBackground(parent)
        NagatoForeground(parent)
        NagatoHighlightColor(parent)
        NagatoHighlightTextColor(parent)
