
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.main.BackToMain import NagatoBackToMain
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.editor.SubGroups import AsakuraSubGroups
from libnagato4.popover.editor.Items import AsakuraItems


class NagatoEditorBox(NagatoBox):

    NAME = "editor"

    def _on_initialize(self):
        AsakuraSubGroups(self)
        NagatoBackToMain(self)
        NagatoSeparator(self)
        AsakuraItems(self)
