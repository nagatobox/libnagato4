
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.popover.item.ItemEvent import HaruhiItemEvent


class NagatoItem(Gtk.Button, NagatoObject, HaruhiItemEvent):

    TITLE = "DON'T LOOK AT ME"

    def _on_initialize(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, self.TITLE, relief=Gtk.ReliefStyle.NONE)
        self.set_alignment(1, 0.5)
        self._bind_events()
        self._raise("YUKI.N > css", (self, "popover-button"))
        self._on_initialize()
        self._raise("YUKI.N > add to box", self)
