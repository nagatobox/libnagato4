
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.popover.scale.Widgets import AsakuraWidgets


class NagatoBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_map(self, *args):
        self._raise("YUKI.N > box mapped")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_homogeneous(True)
        yuki_size = self._enquiry("YUKI.N > size")
        self.set_size_request(*yuki_size)
        AsakuraWidgets(self)
        self._raise("YUKI.N > add to box", self)
        self.connect("map", self._on_map)
