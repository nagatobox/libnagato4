
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToColorScheme(NagatoToStack):

    TITLE = "Color Scheme"
    WHERE_TO_GO = "color_scheme"
