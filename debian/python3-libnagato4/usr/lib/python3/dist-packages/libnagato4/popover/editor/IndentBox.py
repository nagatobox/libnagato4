
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.editor.BackToEditor import NagatoBackToEditor
from libnagato4.popover.editor.AutoIndent import NagatoAutoIndent
from libnagato4.popover.editor.SpacesAsTabs import NagatoSpacesAsTabs
from libnagato4.popover.editor.IndentSize import NagatoIndentSize


class NagatoIndentBox(NagatoBox):

    NAME = "indent"

    def _on_initialize(self):
        NagatoBackToEditor(self)
        NagatoSeparator(self)
        NagatoAutoIndent(self)
        NagatoSpacesAsTabs(self)
        NagatoIndentSize(self)
