
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.widget import Margin


class NagatoSeparator(Gtk.Separator, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Separator.__init__(self)
        self.set_can_focus(False)
        Margin.set_with_unit(self, "margin", "margin", "margin", "margin")
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > css", (self, "popover-separator"))
