
from libnagato4.popover.item.BooleanConfigItem import NagatoBooleanConfigItem


class NagatoShowIcon(NagatoBooleanConfigItem):

    TITLE = "Show Icon"
    SETTING_GROUP = "header"
    SETTING_KEY = "show_icon"
    SETTING_DEFAULT = True
    MATCH_TARGET = True

    def _on_clicked(self, button):
        self._toggle_settings()
        return True
