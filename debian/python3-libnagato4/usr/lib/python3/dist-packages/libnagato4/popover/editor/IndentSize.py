
from libnagato4.popover.spinitem.SpinItem import AbstractSpinItem


class NagatoIndentSize(AbstractSpinItem):

    SETTING_GROUP = "editor"
    SETTING_KEY = "indent_size"
    SETTING_DEFAULT = 4
    LABEL = "Indent Size"
    RANGE = 0, 100, 1
