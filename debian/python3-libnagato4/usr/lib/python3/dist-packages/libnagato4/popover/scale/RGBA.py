
from gi.repository import Gdk
from libnagato4.Object import NagatoObject

QUERY = "css", "primary_fg_color", "rgb(128,128,128)"


class NagatoRGBA(NagatoObject):

    def reset(self, cairo_context):
        yuki_rgba_config = self._enquiry("YUKI.N > settings", QUERY)
        yuki_rgba = Gdk.RGBA()
        yuki_rgba.parse(yuki_rgba_config)
        Gdk.cairo_set_source_rgba(cairo_context, yuki_rgba)

    def __init__(self, parent):
        self._parent = parent
