
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoBox(Gtk.Box, NagatoObject):

    NAME = "define box name here."

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._on_initialize()
        self._raise("YUKI.N > add named", (self, self.NAME))
        self._raise("YUKI.N > css", (self, "popover-list"))
