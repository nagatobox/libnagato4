
from libnagato4.popover.editor.WrapMode import AbstractWrapMode


class NagatoWrapModeWord(AbstractWrapMode):

    TITLE = "Word"
    MATCH_TARGET = 2
