
from libnagato4.popover.item.CheckIconItem import NagatoCheckIconItem


class NagatoBooleanConfigItem(NagatoCheckIconItem):

    def _toggle_settings(self):
        yuki_settings = not self._get_current_settings()
        yuki_user_data = self.SETTING_GROUP, self.SETTING_KEY, yuki_settings
        self._raise("YUKI.N > settings", yuki_user_data)
