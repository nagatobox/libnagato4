
from gi.repository import Gtk
from libnagato4.widget import Margin
from libnagato4.Object import NagatoObject
from libnagato4.popover.spinitem.ClickCanceller import HaruhiClickCenceller


class NagatoButton(Gtk.SpinButton, NagatoObject):

    @classmethod
    def new_with_range(cls, parent, minimum, maximum, step):
        yuki_button = cls(parent)
        yuki_button.set_range(minimum, maximum)
        yuki_button.set_increments(step, -1*step)
        return yuki_button

    def _on_value_changed(self, spin_button):
        self._raise("YUKI.N > value changed", spin_button.get_value())

    def __init__(self, parent):
        self._parent = parent
        Gtk.SpinButton.__init__(self)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        HaruhiClickCenceller(self)
        self.connect("value-changed", self._on_value_changed)
        self._raise("YUKI.N > css", (self, "popover-spin-button"))
        self._raise("YUKI.N > add to box", self)
