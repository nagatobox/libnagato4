
from libnagato4.popover.editor.BooleanConfig import AbstractBooleanConfig


class NagatoShowRightMargin(AbstractBooleanConfig):

    TITLE = "Show Right Margin"
    KEY = "show_right_margin"
    DEFAULT = False
