
from libnagato4.popover.item.MatchConfig import AbstractMatchConfig


class NagatoStyleSchemeItem(AbstractMatchConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "style_scheme_id"
    SETTING_DEFAULT = "tango"

    @classmethod
    def new_for_scheme(cls, parent, scheme):
        yuki_scheme_item = cls(parent)
        yuki_scheme_item.set_scheme(scheme)

    def _on_settings_changed(self, value):
        self._check_icon.set_icon(value)

    def set_scheme(self, scheme):
        self.set_label(scheme.get_name())
        self.props.tooltip_text = scheme.get_description()
        self.MATCH_TARGET = scheme.get_id()
        yuki_setting = self._get_current_settings()
        self._check_icon.set_match_target(self.MATCH_TARGET, yuki_setting)
