
from libnagato4.popover.editor.BooleanConfig import AbstractBooleanConfig


class NagatoSmartBackspace(AbstractBooleanConfig):

    TITLE = "Smart Backspace"
    KEY = "smart_backspace"
    DEFAULT = True
