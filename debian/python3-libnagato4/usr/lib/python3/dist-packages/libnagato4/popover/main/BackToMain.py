
from libnagato4.popover.item.BackToStack import NagatoBackToStack


class NagatoBackToMain(NagatoBackToStack):

    TITLE = "Back"
    WHERE_TO_GO = "main"
