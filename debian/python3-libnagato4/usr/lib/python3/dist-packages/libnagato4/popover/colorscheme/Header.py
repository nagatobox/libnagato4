
from libnagato4.popover.item.StaticLabel import NagatoStaticLabel
from libnagato4.popover.colorscheme.HeaderColor import NagatoHeaderColor
from libnagato4.popover.colorscheme.HeaderTextColor import (
    NagatoHeaderTextColor
    )


class AsakuraHeader:

    def __init__(self, parent):
        NagatoStaticLabel.new_with_label(parent, "Header: ")
        NagatoHeaderColor(parent)
        NagatoHeaderTextColor(parent)
