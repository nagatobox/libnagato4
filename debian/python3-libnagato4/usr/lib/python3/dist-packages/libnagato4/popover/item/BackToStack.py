
from gi.repository import Gtk
from libnagato4.popover.item.Item import NagatoItem

ICON_NAME = "pan-start-symbolic"
ICON_SIZE = Gtk.IconSize.MENU
TRANSITION = Gtk.StackTransitionType.SLIDE_RIGHT


class NagatoBackToStack(NagatoItem):

    TITLE = "DEFINE TITLE HERE"
    MESSAGE = "YUKI.N > switch stack to"
    WHERE_TO_GO = "define where to go"

    def _on_clicked(self, button):
        self._raise(self.MESSAGE, (self.WHERE_TO_GO, TRANSITION))

    def _on_initialize(self):
        yuki_image = Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE)
        self.set_image(yuki_image)
        self.set_image_position(Gtk.PositionType.LEFT)
