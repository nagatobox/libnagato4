
from libnagato4.popover.item.Item import NagatoItem


class NagatoAbout(NagatoItem):

    TITLE = "About"
    MESSAGE = "YUKI.N > about"
