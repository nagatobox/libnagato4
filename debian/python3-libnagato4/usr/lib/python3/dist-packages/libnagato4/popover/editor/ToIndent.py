
from libnagato4.popover.item.ToStack import NagatoToStack


class NagatoToIndent(NagatoToStack):

    TITLE = "Indent"
    WHERE_TO_GO = "indent"
