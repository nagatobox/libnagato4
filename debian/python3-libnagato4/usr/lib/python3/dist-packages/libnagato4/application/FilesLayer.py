
from libnagato4.Object import NagatoObject
from libnagato4.file.queue.Queue import NagatoQueue
from libnagato4.application.RecentLayer import NagatoRecentLayer


class NagatoFilesLayer(NagatoObject):

    def _inform_arg_file(self):
        return self._queue.pop_file()

    def __init__(self, parent):
        self._parent = parent
        self._queue = NagatoQueue(self)
        self._queue.queue()
        NagatoRecentLayer(self)
