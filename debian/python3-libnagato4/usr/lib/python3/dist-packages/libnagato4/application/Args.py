
import argparse
from libnagato4.Object import NagatoObject
from libnagato4.application.ArgsYaml import NagatoArgsYaml
from libnagato4.application.ArgsVersion import NagatoArgsVersion


class NagatoArgs(NagatoObject):

    def __getitem__(self, key):
        if key not in dir(self._args):
            return None
        return getattr(self._args, key)

    def _yuki_n_add_argument(self, argument_item):
        yuki_names = argument_item["names"]
        yuki_options = argument_item["options"]
        if "type" in yuki_options:
            yuki_options["type"] = type(yuki_options["type"])
        self._parser.add_argument(*yuki_names, **yuki_options)

    def __init__(self, parent):
        self._parent = parent
        yuki_description = self._enquiry("YUKI.N > data", "short-description")
        self._parser = argparse.ArgumentParser(description=yuki_description)
        NagatoArgsYaml(self)
        NagatoArgsVersion(self)
        self._args = self._parser.parse_args()
