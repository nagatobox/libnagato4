
from libnagato4.Object import NagatoObject
from libnagato4.application.ActionLayer import NagatoActionLayer
from libnagato4.application.RecentPaths import NagatoRecentPaths


class NagatoRecentLayer(NagatoObject):

    def _yuki_n_recent_path(self, user_data):
        yuki_group, yuki_path = user_data
        self._recent_paths.set_path(yuki_group, yuki_path)

    def _inform_recent_paths(self, group):
        return self._recent_paths.get_recent_paths(group)

    def __init__(self, parent):
        self._parent = parent
        self._recent_paths = NagatoRecentPaths(self)
        NagatoActionLayer(self)
