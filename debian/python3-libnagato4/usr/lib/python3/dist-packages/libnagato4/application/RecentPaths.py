
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoRecentPaths(NagatoObject):

    def get_recent_paths(self, group):
        yuki_paths = []
        yuki_query = "recent", group, []
        for yuki_path in self._enquiry("YUKI.N > settings", yuki_query):
            if GLib.file_test(yuki_path, GLib.FileTest.EXISTS):
                yuki_paths.append(yuki_path)
        self._raise("YUKI.N > settings", ("recent", group, yuki_paths))
        return yuki_paths

    def set_path(self, group, path):
        yuki_paths = self.get_recent_paths(group)
        if path in yuki_paths:
            yuki_paths.remove(path)
        yuki_paths.insert(0, path)
        yuki_data = "recent", group, yuki_paths[:20]
        self._raise("YUKI.N > settings", yuki_data)

    def __init__(self, parent):
        self._parent = parent
