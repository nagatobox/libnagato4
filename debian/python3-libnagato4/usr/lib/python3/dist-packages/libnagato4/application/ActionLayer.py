
from libnagato4.Object import NagatoObject
from libnagato4.dialog.about.About import NagatoAbout
from libnagato4.Transmitter import HaruhiTransmitter


class NagatoActionLayer(NagatoObject):

    def _inform_enable_global_key_shortcuts(self):
        return self._global_key_shortcuts_enabled

    def _yuki_n_enable_global_key_shortcuts(self, enabled):
        self._global_key_shortcuts_enabled = enabled

    def _yuki_n_about(self):
        NagatoAbout.get_response(self)

    def _yuki_n_action(self, user_data):
        self._transmitter.transmit(user_data)

    def _yuki_n_register_action_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._global_key_shortcuts_enabled = True
        self._transmitter = HaruhiTransmitter()
        self._raise("YUKI.N > loopback set main window", self)
