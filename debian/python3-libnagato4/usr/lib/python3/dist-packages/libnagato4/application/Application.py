
from libnagato4.Object import NagatoObject
from libnagato4.application.ResourcesLayer import NagatoResourcesLayer


class AbstractApplication(NagatoObject):

    def _yuki_n_loopback_set_main_window(self, parent):
        pass

    def __init__(self, parent):
        self._parent = parent
        NagatoResourcesLayer(self)
