
from libnagato4.Object import NagatoObject


class NagatoArgsVersion(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_version = self._enquiry("YUKI.N > data", "version")
        yuki_argument_item = {
            "names": ["--version"],
            "options": {"action": "version", "version": yuki_version}
        }
        self._raise("YUKI.N > add argument", yuki_argument_item)
