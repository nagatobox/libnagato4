
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.application.SharedResource import HaruhiSharedResource
from libnagato4.application.LocalResource import NagatoLocalResource

CSS = "application.css"
ADDITIONAL_CSS = ".additional_application.css"


class NagatoCssLocalFile(NagatoObject):

    def set_to_local(self, css):
        GLib.file_set_contents(self._local_path, css.encode())

    def get_from_resource(self):
        return self._css

    def __init__(self, parent):
        self._parent = parent
        yuki_shared = HaruhiSharedResource(CSS)
        yuki_local = NagatoLocalResource(self, ADDITIONAL_CSS)
        self._css = yuki_shared.get_content()+yuki_local.get_content()
        self._local_path = self._enquiry("YUKI.N > local file", CSS)
