
from gi.repository import GLib


class HaruhiSharedResource:

    def get_content(self):
        if self._path is None:
            return ""
        _, yuki_bytes = GLib.file_get_contents(self._path)
        return yuki_bytes.decode("utf-8")

    def _get_system_path(self, name):
        for yuki_dir in GLib.get_system_data_dirs():
            yuki_names = [yuki_dir, "libnagato4", "data", name]
            yuki_path = GLib.build_filenamev(yuki_names)
            if GLib.file_test(yuki_path, GLib.FileTest.EXISTS):
                return yuki_path
        return None

    def __init__(self, name):
        yuki_dir = GLib.get_user_data_dir()
        yuki_names = [yuki_dir, "libnagato4", name]
        yuki_path = GLib.build_filenamev(yuki_names)
        if GLib.file_test(yuki_path, GLib.FileTest.EXISTS):
            self._path = yuki_path
        else:
            self._path = self._get_system_path(name)
