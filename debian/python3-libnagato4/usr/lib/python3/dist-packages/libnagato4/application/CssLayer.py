
from libnagato4.Object import NagatoObject
from libnagato4.application.CssReplacements import NagatoCssReplacements
from libnagato4.application.FilesLayer import NagatoFilesLayer
from libnagato4.application.RecentLayer import NagatoRecentLayer


class NagatoCssLayer(NagatoObject):

    def _yuki_n_css(self, user_data):
        yuki_widget, yuki_css_class = user_data
        yuki_style_context = yuki_widget.get_style_context()
        yuki_style_context.add_class(yuki_css_class)

    def __init__(self, parent):
        self._parent = parent
        NagatoCssReplacements(self)
        if self._enquiry("YUKI.N > args", "files") is not None:
            NagatoFilesLayer(self)
        else:
            NagatoRecentLayer(self)
