
from gi.repository import GLib
from libnagato4.Object import NagatoObject


class NagatoLocalResource(NagatoObject):

    def get_content(self):
        if not GLib.file_test(self._path, GLib.FileTest.EXISTS):
            return ""
        _, yuki_bytes = GLib.file_get_contents(self._path)
        return yuki_bytes.decode("utf-8")

    def __init__(self, parent, name):
        self._parent = parent
        self._path = self._enquiry("YUKI.N > resources", name)
