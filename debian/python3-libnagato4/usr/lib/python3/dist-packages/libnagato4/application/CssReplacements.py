
from libnagato4.Object import NagatoObject
from libnagato4.application.CssProvider import NagatoCssProvider
from libnagato4.application.CssLocalFile import NagatoCssLocalFile
from libnagato4.application.CssYaml import NagatoCssYaml


class NagatoCssReplacements(NagatoObject):

    def _reload(self):
        yuki_css = self._local_file.get_from_resource()
        yuki_css = self._yaml.replace_all(yuki_css)
        self._local_file.set_to_local(yuki_css)
        self._css_provider.set_to_application()

    def receive_transmission(self, user_data):
        yuki_group, yuki_key, yuki_vaue = user_data
        if yuki_group == "css":
            self._reload()

    def __init__(self, parent):
        self._parent = parent
        self._css_provider = NagatoCssProvider(self)
        self._yaml = NagatoCssYaml(self)
        self._local_file = NagatoCssLocalFile(self)
        self._reload()
        self._raise("YUKI.N > register settings object", self)
