
from libnagato4.Object import NagatoObject
from libnagato4.application.Args import NagatoArgs
from libnagato4.application.CssLayer import NagatoCssLayer


class NagatoArgsLayer(NagatoObject):

    def _inform_args(self, key):
        return self._args[key]

    def __init__(self, parent):
        self._parent = parent
        self._args = NagatoArgs(self)
        NagatoCssLayer(self)
