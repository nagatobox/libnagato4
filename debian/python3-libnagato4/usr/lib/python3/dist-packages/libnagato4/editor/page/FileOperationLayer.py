
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagato4.editor.page.Language import NagatoLanguage


class NagatoFileOperationLayer(NagatoObject):

    def _yuki_n_load_finished(self, path):
        self._transmitter.transmit(("load finished", path))
        self._raise("YUKI.N > recent path", ("main", path))

    def _yuki_n_save_finished(self, path):
        self._transmitter.transmit(("save finished", path))
        self._raise("YUKI.N > recent path", ("main", path))

    def _yuki_n_file_operation_progress(self, description):
        self._transmitter.transmit(("progress", description))

    def _yuki_n_register_file_operation_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        NagatoLanguage(self)
