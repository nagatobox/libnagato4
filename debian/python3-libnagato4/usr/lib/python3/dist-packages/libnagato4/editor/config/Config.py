
from libnagato4.Object import NagatoObject
from libnagato4.dispatch.SettingsDispatch import HaruhiSettigsDispatch


class AbstractConfig(NagatoObject, HaruhiSettigsDispatch):

    METHOD = "define method here"

    def _on_settings_changed(self, value):
        for yuki_page in self._enquiry("YUKI.N > pages"):
            self.set_current_value(yuki_page, value)

    def set_current_value(self, page, value=None):
        if value is None:
            value = self._get_current_settings()
        yuki_method = getattr(page.view, self.METHOD)
        yuki_method(value)

    def __init__(self, parent):
        self._parent = parent
        self._bind_settings_dispatch()
