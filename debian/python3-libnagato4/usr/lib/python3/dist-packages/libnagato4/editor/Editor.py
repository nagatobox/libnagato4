
from libnagato4.Object import NagatoObject
from libnagato4.editor.PageRegistry import NagatoPageRegistry


class AbstractEditor(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        NagatoPageRegistry(self)
