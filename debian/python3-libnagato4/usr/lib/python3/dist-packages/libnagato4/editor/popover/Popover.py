
from libnagato4.popover.MultiStack import NagatoMultiStack
from libnagato4.editor.popover.main.Main import NagatoMain
from libnagato4.editor.popover.recent.RecentBox import NagatoRecentBox
from libnagato4.editor.page import GdkRectangle


class NagatoPopover(NagatoMultiStack):

    def _get_left_adjustment(self, view):
        yuki_left_gutter = view.get_gutter(3)
        try:
            return yuki_left_gutter.get_window().get_width()
        except AttributeError:
            return 0

    def popup_for_event(self, view, event):
        self.set_relative_to(view)
        yuki_rectangle = GdkRectangle.get_from_event(event)
        yuki_rectangle.x = yuki_rectangle.x+self._get_left_adjustment(view)
        self.set_pointing_to(yuki_rectangle)
        self.show_all()

    def _yuki_n_loopback_add_popover_groups(self, parent):
        NagatoMain(parent)
        NagatoRecentBox(parent)
