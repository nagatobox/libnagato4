
from libnagato4.dispatch.ActionDispatch import NagatoActionDispatch
from libnagato4.editor.action.SaveAs import NagatoSaveAs


class NagatoPageActions(NagatoActionDispatch):

    ACTIONS = ["save", "save as", "close current tab"]

    def _raise_message_to_current_tab(self, message):
        yuki_current_page = self._enquiry("YUKI.N > current page")
        yuki_current_page.raise_message(message)

    def _save(self):
        self._raise_message_to_current_tab("YUKI.N > save")

    def _save_as(self):
        yuki_save_as = NagatoSaveAs(self)
        if yuki_save_as.try_save_as():
            self._raise_message_to_current_tab("YUKI.N > save")

    def _close_current_tab(self):
        self._raise_message_to_current_tab("YUKI.N > try close tab")
