
from libnagato4.editor.config.Config import AbstractConfig


class NagatoSmartBackspace(AbstractConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "smart_backspace"
    SETTING_DEFAULT = True
    METHOD = "set_smart_backspace"
