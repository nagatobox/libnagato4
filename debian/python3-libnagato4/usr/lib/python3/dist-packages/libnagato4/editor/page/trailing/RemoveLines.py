

class HaruhiRemoveLines:

    def _get_start_iter(self, end_iter):
        yuki_start_iter = end_iter.copy()
        while yuki_start_iter.backward_line():
            if not yuki_start_iter.ends_line():
                yuki_start_iter.forward_to_line_end()
                break
        return yuki_start_iter

    def remove(self, buffer_):
        yuki_end_iter = buffer_.get_end_iter()
        if not yuki_end_iter.starts_line():
            return
        yuki_start_iter = self._get_start_iter(yuki_end_iter)
        buffer_.delete(yuki_start_iter, yuki_end_iter)
