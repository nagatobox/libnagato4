
from libnagato4.editor.config.Config import AbstractConfig


class NagatoSpacesAsTabs(AbstractConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "insert_spaces_instead_of_tabs"
    SETTING_DEFAULT = True
    METHOD = "set_insert_spaces_instead_of_tabs"
