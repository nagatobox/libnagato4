
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.fileoperation.Dispatch import HaruhiDispatch


class NagatoTimeDifference(NagatoObject, HaruhiDispatch):

    def get_readable(self):
        return self._saved_time.format("%T")

    def _save_finished(self, path):
        self._saved_time = GLib.DateTime.new_now_local()

    def __init__(self, parent):
        self._parent = parent
        self._saved_time = GLib.DateTime.new_now_local()
        self._bind_dispatch()
