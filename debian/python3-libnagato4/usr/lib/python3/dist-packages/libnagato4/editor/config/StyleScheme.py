
from gi.repository import GtkSource
from libnagato4.Object import NagatoObject
from libnagato4.dispatch.SettingsDispatch import HaruhiSettigsDispatch


class NagatoStyleScheme(NagatoObject, HaruhiSettigsDispatch):

    SETTING_GROUP = "editor"
    SETTING_KEY = "style_scheme_id"
    SETTING_DEFAULT = "tango"

    def _set_scheme_for_id(self, buffer_, scheme_id):
        yuki_scheme = self._manager.get_scheme(scheme_id)
        buffer_.set_style_scheme(yuki_scheme)

    def _on_settings_changed(self, value):
        for yuki_page in self._enquiry("YUKI.N > pages"):
            self._set_scheme_for_id(yuki_page.buffer, value)

    def set_current_value(self, page):
        yuki_id = self._get_current_settings()
        self._set_scheme_for_id(page.buffer, yuki_id)

    def __init__(self, parent):
        self._parent = parent
        self._manager = GtkSource.StyleSchemeManager.get_default()
        self._bind_settings_dispatch()
