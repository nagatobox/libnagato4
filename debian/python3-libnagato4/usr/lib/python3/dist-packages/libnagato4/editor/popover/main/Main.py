
from libnagato4.popover.Box import NagatoBox
from libnagato4.editor.popover.EditBox import NagatoEditBox
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.popover.action.New import NagatoNew
from libnagato4.popover.action.Open import NagatoOpen
from libnagato4.popover.action.Save import NagatoSave
from libnagato4.popover.action.SaveAs import NagatoSaveAs
from libnagato4.editor.popover.CloseCurrentTab import NagatoCloseCurrentTab
from libnagato4.editor.popover.recent.ToRecent import NagatoToRecent
from libnagato4.popover.editor.ShowStatusBar import NagatoShowStatusBar


class NagatoMain(NagatoBox):

    NAME = "main"

    def _on_initialize(self):
        NagatoEditBox(self)
        NagatoSeparator(self)
        NagatoNew(self)
        NagatoOpen(self)
        NagatoToRecent(self)
        NagatoSave(self)
        NagatoSaveAs(self)
        NagatoSeparator(self)
        NagatoCloseCurrentTab(self)
        NagatoShowStatusBar(self)
