
from libnagato4.editor.Config import AbstractConfig
from libnagato4.editor.config.StyleScheme import NagatoStyleScheme
from libnagato4.editor.config.HighlightBrackets import NagatoHighlightBrackets


class NagatoBufferConfig(AbstractConfig):

    def _on_initialize(self):
        self._configs.append(NagatoStyleScheme(self))
        self._configs.append(NagatoHighlightBrackets(self))
