
from gi.repository import Gtk
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.UndoLayer import NagatoUndoLayer


class NagatoClipboardLayer(NagatoObject):

    def _yuki_n_cut(self):
        self._buffer.cut_clipboard(self._clipboard, True)

    def _yuki_n_paste(self):
        self._buffer.paste_clipboard(self._clipboard, None, True)

    def _yuki_n_copy(self):
        self._buffer.copy_clipboard(self._clipboard)

    def __init__(self, parent):
        self._parent = parent
        self._buffer = self._enquiry("YUKI.N > buffer")
        yuki_display = Gdk.Display.get_default()
        self._clipboard = Gtk.Clipboard.get_default(yuki_display)
        NagatoUndoLayer(self)
