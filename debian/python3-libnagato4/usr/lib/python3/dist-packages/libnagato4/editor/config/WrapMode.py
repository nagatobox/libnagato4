
from libnagato4.editor.config.Config import AbstractConfig


class NagatoWrapMode(AbstractConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "wrap_mode"
    SETTING_DEFAULT = 0
    METHOD = "set_wrap_mode"
