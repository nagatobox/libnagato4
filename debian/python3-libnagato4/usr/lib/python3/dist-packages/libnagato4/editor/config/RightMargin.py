
from libnagato4.editor.config.Config import AbstractConfig


class NagatoRightMargin(AbstractConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "right_margin_position"
    SETTING_DEFAULT = 80
    METHOD = "set_right_margin_position"
