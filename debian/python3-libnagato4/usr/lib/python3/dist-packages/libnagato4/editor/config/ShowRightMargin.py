
from libnagato4.editor.config.Config import AbstractConfig


class NagatoShowRightMargin(AbstractConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "show_right_margin"
    SETTING_DEFAULT = False
    METHOD = "set_show_right_margin"
