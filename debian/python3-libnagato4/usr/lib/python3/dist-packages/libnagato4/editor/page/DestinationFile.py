
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagato4.chooser.context.SaveFile import NagatoContext


class NagatoDestinationFile(NagatoObject):

    def _get_from_dialog(self):
        yuki_context = NagatoContext.new(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return False
        self._raise("YUKI.N > file location", yuki_context.get_gio_file())
        return True

    def try_set_location(self):
        if self._enquiry("YUKI.N > has file location"):
            return True
        return self._get_from_dialog()

    def __init__(self, parent):
        self._parent = parent
