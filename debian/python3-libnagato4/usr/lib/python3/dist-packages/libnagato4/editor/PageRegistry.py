
from libnagato4.Object import NagatoObject
from libnagato4.editor.notebook.Notebook import NagatoNotebook
from libnagato4.editor.registry.Configs import AsakuraConfigs
from libnagato4.editor.ContentCloser import NagatoContentCloser


class NagatoPageRegistry(NagatoObject):

    def _yuki_n_register_page(self, page):
        yuki_id = page.request_information("YUKI.N > page id")
        self._dict[yuki_id] = page
        self._configs.register(page)

    def _yuki_n_remove_page_for_id(self, id_):
        del self._dict[id_]

    def _inform_pages(self):
        return self._dict.values()

    def __init__(self, parent):
        self._parent = parent
        self._dict = {}
        self._configs = AsakuraConfigs(self)
        NagatoContentCloser(self)
        NagatoNotebook(self)
