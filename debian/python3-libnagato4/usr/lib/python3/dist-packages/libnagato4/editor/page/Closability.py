
from libnagato4.editor.page.TimeDifference import NagatoTimeDifference
from libnagato4.editor.page.SaveState import NagatoSaveState


class AsakuraClosability:

    def get_unsaved_until(self):
        if self._save_state.get_saved():
            return None
        else:
            return self._time_difference.get_readable()

    def get_is_closable(self):
        return self._save_state.get_saved()

    def __init__(self, parent):
        self._save_state = NagatoSaveState(parent)
        self._time_difference = NagatoTimeDifference(parent)
