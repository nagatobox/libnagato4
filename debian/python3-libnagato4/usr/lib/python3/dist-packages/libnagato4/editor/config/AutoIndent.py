
from libnagato4.editor.config.Config import AbstractConfig


class NagatoAutoIndent(AbstractConfig):

    SETTING_GROUP = "editor"
    SETTING_KEY = "auto_indent"
    SETTING_DEFAULT = True
    METHOD = "set_auto_indent"
