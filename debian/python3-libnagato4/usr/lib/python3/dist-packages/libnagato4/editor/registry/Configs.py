
from libnagato4.editor.BufferConfig import NagatoBufferConfig
from libnagato4.editor.ViewConfig import NagatoViewConfig


class AsakuraConfigs:

    def register(self, page_registration_point):
        self._view_config.append(page_registration_point)
        self._buffer_config.append(page_registration_point)

    def __init__(self, parent):
        self._view_config = NagatoViewConfig(parent)
        self._buffer_config = NagatoBufferConfig(parent)
