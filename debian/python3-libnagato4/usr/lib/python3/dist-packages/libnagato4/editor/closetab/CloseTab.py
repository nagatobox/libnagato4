
from libnagato4.Object import NagatoObject
from libnagato4.editor.closetab.Untitled import NagatoUntitled


class NagatoCloseTab(NagatoObject):

    def _is_single_tab(self):
        if self._enquiry("YUKI.N > number of pages") == 1:
            self._raise("YUKI.N > confirm to close")
        else:
            self._raise("YUKI.N > closing confirmed")

    def try_close_tab(self):
        if self._enquiry("YUKI.N > tab closable"):
            self._is_single_tab()
        else:
            self._untitled.try_save()

    def __init__(self, parent):
        self._parent = parent
        self._untitled = NagatoUntitled(self)
