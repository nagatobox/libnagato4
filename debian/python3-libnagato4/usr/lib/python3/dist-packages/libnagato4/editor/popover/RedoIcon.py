
from libnagato4.popover.item.IconItem import AbstractIconItem


class NagatoRedoIcon(AbstractIconItem):

    ICON_NAME = "edit-redo-symbolic"
    MESSAGE = "YUKI.N > redo"

    def _on_initialize(self):
        self.set_hexpand(True)
        self.props.tooltip_text = "Redo Ctrl+Shift+Z"
