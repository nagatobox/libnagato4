
from libnagato4.widget.VBox import AbstractVBox
from libnagato4.editor.popover.recent.Item import NagatoItem


class NagatoItems(AbstractVBox):

    def _remove_all(self):
        for yuki_child in self.get_children():
            yuki_child.destroy()

    def _refresh(self):
        self._remove_all()
        for yuki_path in self._enquiry("YUKI.N > recent paths", "main"):
            NagatoItem.new_for_path(self, yuki_path)
        self.show_all()

    def _on_map(self, box):
        self._refresh()

    def _on_initialize(self):
        self.connect("map", self._on_map)
        self._raise("YUKI.N > add to box", self)
