
from libnagato4.popover.item.IconItem import AbstractIconItem


class NagatoCutIcon(AbstractIconItem):

    ICON_NAME = "edit-cut-symbolic"
    MESSAGE = "YUKI.N > cut"

    def _on_initialize(self):
        self.set_hexpand(True)
        self.props.tooltip_text = "Cut Ctrl+X"
