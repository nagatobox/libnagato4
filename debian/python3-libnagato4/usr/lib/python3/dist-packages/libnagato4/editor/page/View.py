
from gi.repository import GtkSource
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.RegistrationPoint import NagatoRegistrationPoint
from libnagato4.editor.page.ViewEvent import NagatoViewEvent
from libnagato4.editor.page.Cursor import NagatoCursor


class NagatoView(GtkSource.View, NagatoObject):

    def _inform_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        yuki_buffer = self._enquiry("YUKI.N > buffer")
        GtkSource.View.__init__(self, buffer=yuki_buffer)
        self._raise("YUKI.N > add to scrolled window", self)
        NagatoViewEvent(self)
        NagatoCursor.new_for_view_as_parent(self)
        NagatoRegistrationPoint(self)
