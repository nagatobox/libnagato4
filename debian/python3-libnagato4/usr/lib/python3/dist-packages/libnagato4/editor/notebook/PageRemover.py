
from libnagato4.Object import NagatoObject


class NagatoPageRemover(NagatoObject):

    def remove_for_page_id(self, target_id):
        yuki_notebook = self._enquiry("YUKI.N > notebook")
        yuki_index = 0
        for yuki_child in yuki_notebook.get_children():
            yuki_id = yuki_child.request_information("YUKI.N > page id")
            if yuki_id == target_id:
                yuki_notebook.remove_page(yuki_index)
                break
            yuki_index += 1

    def __init__(self, parent):
        self._parent = parent
