
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.View import NagatoView

POLICY = Gtk.PolicyType.AUTOMATIC


class NagatoScrolledWindow(Gtk.ScrolledWindow, NagatoObject):

    def raise_message(self, message, user_data=None):
        self._raise(message, user_data)

    def request_information(self, message, user_data=None):
        return self._enquiry(message, user_data)

    def _yuki_n_add_to_scrolled_window(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        self.set_policy(POLICY, POLICY)
        self.set_vexpand(True)
        NagatoView(self)
        self._raise("YUKI.N > add to box", self)
