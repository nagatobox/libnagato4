
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.editor.notebook.NotebookState import HaruhiNotebookState
from libnagato4.editor.notebook.PagesLayer import NagatoPagesLayer


class NagatoNotebook(Gtk.Notebook, NagatoObject):

    def _inform_notebook(self):
        return self

    def _inform_number_of_pages(self):
        return self.get_n_pages()

    def _inform_current_page(self):
        yuki_page_index = self.get_current_page()
        return self.get_nth_page(yuki_page_index)

    def _inform_current_index(self):
        return self.get_current_page()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Notebook.__init__(self)
        self._state = HaruhiNotebookState(self)
        NagatoPagesLayer(self)
        self._raise("YUKI.N > attach main widget", self)
