
from libnagato4.Object import NagatoObject
from libnagato4.dispatch.SettingsDispatch import HaruhiSettigsDispatch
from libnagato4.editor.page.trailing.RemoveLines import HaruhiRemoveLines
from libnagato4.editor.page.trailing.RemoveSpaces import HaruhiRemoveSpaces


class NagatoRemoveTrailing(NagatoObject, HaruhiSettigsDispatch):

    SETTING_GROUP = "editor"
    SETTING_KEY = "remove_trailing"
    SETTING_DEFAULT = False

    def _on_settings_changed(self, value):
        self._remove_trailing = value

    def remove(self):
        if self._remove_trailing:
            yuki_buffer = self._enquiry("YUKI.N > buffer")
            self._remove_spaces.remove(yuki_buffer)
            self._remove_lines.remove(yuki_buffer)

    def __init__(self, parent):
        self._parent = parent
        self._bind_settings_dispatch()
        self._remove_trailing = self._get_current_settings()
        self._remove_lines = HaruhiRemoveLines()
        self._remove_spaces = HaruhiRemoveSpaces()
