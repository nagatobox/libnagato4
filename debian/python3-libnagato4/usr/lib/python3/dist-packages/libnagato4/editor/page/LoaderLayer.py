
from libnagato4.Object import NagatoObject
from libnagato4.editor.page.SaverLayer import NagatoSaverLayer
from libnagato4.editor.page.FileLoader import NagatoFileLoader


class NagatoLoaderLayer(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        NagatoSaverLayer(self)
        NagatoFileLoader(self)
