
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit

FORMAT = "line {}/{}, column {}"


class NagatoLabel(Gtk.Label, NagatoObject):

    def set_data(self, user_data):
        yuki_lines, yuki_line, yuki_column = user_data
        yuki_text = FORMAT.format(yuki_line, yuki_lines, yuki_column)
        self.set_text(yuki_text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_size_request(-1, Unit(6))
        self._raise("YUKI.N > add to revealer", self)
