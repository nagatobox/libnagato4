
from libnagato4.widget.VBox import AbstractVBox
from libnagato4.editor.page.ScrolledWindow import NagatoScrolledWindow
from libnagato4.editor.page.statusbar.StatusBar import NagatoStatusBar


class NagatoPageBox(AbstractVBox):

    def raise_message(self, message, user_data=None):
        self._raise(message, user_data)

    def request_information(self, query, user_data=None):
        return self._enquiry(query, user_data)

    def _yuki_n_cursor_moved(self, user_data):
        self._status_bar.set_data(user_data)

    def _on_initialize(self):
        NagatoScrolledWindow(self)
        self._status_bar = NagatoStatusBar(self)
