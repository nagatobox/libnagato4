
from gi.repository import Gtk
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagato4.chooser.context.OpenFiles import NagatoContext


class NagatoOpen(NagatoObject):

    def open(self):
        yuki_context = NagatoContext(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return
        for yuki_path in yuki_context["selection"]:
            self.open_path(yuki_path)

    def open_path(self, path):
        yuki_gio_file = Gio.File.new_for_path(path)
        self._raise("YUKI.N > file loaded", yuki_gio_file)

    def __init__(self, parent):
        self._parent = parent
