
from libnagato4.popover.Box import NagatoBox
from libnagato4.popover.main.BackToMain import NagatoBackToMain
from libnagato4.popover.Separator import NagatoSeparator
from libnagato4.editor.popover.recent.Items import NagatoItems


class NagatoRecentBox(NagatoBox):

    NAME = "recent"

    def _on_initialize(self):
        NagatoBackToMain(self)
        NagatoSeparator(self)
        NagatoItems(self)
