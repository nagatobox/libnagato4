
from libnagato4.popover.item.IconItem import AbstractIconItem


class NagatoUndoIcon(AbstractIconItem):

    ICON_NAME = "edit-undo-symbolic"
    MESSAGE = "YUKI.N > undo"

    def _on_initialize(self):
        self.set_hexpand(True)
        self.props.tooltip_text = "Undo Ctrl+Z"
