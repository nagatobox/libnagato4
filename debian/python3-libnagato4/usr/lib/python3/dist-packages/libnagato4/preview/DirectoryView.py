
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.preview.DirectoryIcon import NagatoDirectoryIcon
from libnagato4.preview.DirectoryLabel import NagatoDirectoryLabel


class NagatoDirectoryView(Gtk.Box, NagatoObject):

    def load_path_async(self, fullpath):
        self._label.reset(fullpath)
        self._raise("YUKI.N > switch stack to", "directory-view")

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        NagatoDirectoryIcon(self)
        self._label = NagatoDirectoryLabel(self)
        self._raise("YUKI.N > add to stack named", (self, "directory-view"))
        self._raise("YUKI.N > css", (self, "bookmark-treeview"))
