
import cairo
from gi.repository import Gdk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject

FORMAT = cairo.Format.ARGB32


class NagatoPixbuf(NagatoObject):

    def _get_surface(self, size, rate):
        yuki_width, yuki_height = int(size.width*rate), int(size.height*rate)
        return cairo.ImageSurface(FORMAT, yuki_width, yuki_height)

    def _paint_background(self, cairo_context, size, rate):
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.rectangle(0, 0, size.width*rate, size.height*rate)
        cairo_context.fill()

    def _get_pixbuf(self, surface, size, rate):
        yuki_geometries = 0, 0, size.width*rate, size.height*rate
        return Gdk.pixbuf_get_from_surface(surface, *yuki_geometries)

    def get_from_page(self, page):
        yuki_width = self._enquiry("YUKI.N > allocated width")-Unit(3)
        yuki_size = page.get_size()
        yuki_rate = max(Unit(16), yuki_width)/yuki_size.width
        yuki_surface = self._get_surface(yuki_size, yuki_rate)
        yuki_cairo_context = cairo.Context(yuki_surface)
        self._paint_background(yuki_cairo_context, yuki_size, yuki_rate)
        yuki_cairo_context.scale(yuki_rate, yuki_rate)
        page.render_for_printing(yuki_cairo_context)
        return self._get_pixbuf(yuki_surface, yuki_size, yuki_rate)

    def __init__(self, parent):
        self._parent = parent
