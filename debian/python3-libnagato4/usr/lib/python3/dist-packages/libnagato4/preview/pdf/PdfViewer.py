
from libnagato4.Object import NagatoObject
from libnagato4.preview.pdf.Model import NagatoModel
from libnagato4.preview.pdf.ScrolledWindow import NagatoScrolledWindow


class NagatoPdfViewer(NagatoObject):

    def _inform_path(self):
        return self._path

    def _inform_model(self):
        return self._model

    def _inform_allocated_width(self):
        return max(1, self._scrolled_window.get_allocated_width())

    def load_path_async(self, path):
        self._path = path
        self._model.reset_path(path)
        self._raise("YUKI.N > switch stack to", "pdf-viewer")

    def __init__(self, parent):
        self._parent = parent
        self._model = NagatoModel(self)
        self._scrolled_window = NagatoScrolledWindow(self)
