
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject

NAME = "dialog-warning-symbolic"
SIZE = Gtk.IconSize.DIALOG


class NagatoBadFileIcon(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_image = Gtk.Image.new_from_icon_name(NAME, SIZE)
        yuki_image.set_margin_top(Unit(2))
        yuki_image.set_margin_bottom(Unit(2))
        self._raise("YUKI.N > add to box", yuki_image)
