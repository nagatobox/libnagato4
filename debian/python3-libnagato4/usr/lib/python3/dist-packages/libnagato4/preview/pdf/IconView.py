
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.preview.pdf import Column


class NagatoIconView(Gtk.IconView, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        Gtk.IconView.__init__(self)
        self.set_model(self._enquiry("YUKI.N > model"))
        self.set_pixbuf_column(Column.PIXBUF)
        self.set_selection_mode(Gtk.SelectionMode.NONE)
        self._raise("YUKI.N > add to scrolled window", self)

