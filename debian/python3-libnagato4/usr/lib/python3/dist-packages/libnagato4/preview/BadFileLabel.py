
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import Pango
from libnagato4.Object import NagatoObject

MARKUP = ""\
    "<span size='large' underline='single'>WARNING !!</span>\n\n"\
    "{}\n\nis corrupt or has an incorrect extension."


class NagatoBadFileLabel(Gtk.Label, NagatoObject):

    def reset(self, fullpath):
        yuki_basename = GLib.filename_display_basename(fullpath)
        self.set_markup(MARKUP.format(yuki_basename))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_justify(Gtk.Justification.CENTER)
        self.props.wrap = True
        self.props.wrap_mode = Pango.WrapMode.WORD
        self.set_vexpand(True)
        self._raise("YUKI.N > add to box", self)
