
from gi.repository import Gio
from gi.repository import GtkSource
from libnagato4.Object import NagatoObject
from libnagato4.preview.TextLoader import HaruhiTextLoader
from libnagato4.preview.TextViewer import NagatoTextViewer


class NagatoTextModel(NagatoObject):

    def _inform_buffer(self):
        return self._buffer

    def load_path_async(self, fullpath):
        self._raise("YUKI.N > switch stack to", "text-viewer")
        self._file.set_location(Gio.File.new_for_path(fullpath))
        HaruhiTextLoader(self._buffer, self._file)
        yuki_language = self._language_manager.guess_language(fullpath)
        if yuki_language is not None:
            self._buffer.set_language(yuki_language)

    def __init__(self, parent):
        self._parent = parent
        self._language_manager = GtkSource.LanguageManager.get_default()
        self._file = GtkSource.File()
        self._buffer = GtkSource.Buffer.new()
        yuki_style_scheme_manager = GtkSource.StyleSchemeManager.get_default()
        yuki_style_scheme = yuki_style_scheme_manager.get_scheme("oblivion")
        self._buffer.set_style_scheme(yuki_style_scheme)
        NagatoTextViewer(self)
