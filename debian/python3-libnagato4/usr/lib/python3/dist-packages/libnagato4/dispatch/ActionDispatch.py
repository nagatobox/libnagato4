
from libnagato4.Object import NagatoObject
from libnagato4.dispatch.ActionDecoder import HaruhiActionDecoder


class NagatoActionDispatch(NagatoObject):

    ACTIONS = ["register", "action", "names", "as", "list"]
    HEADER = "_"

    def receive_transmission(self, action):
        yuki_method_name, yuki_data = self._action_decoder.decode(action)
        if yuki_method_name is None:
            return
        yuki_method = getattr(self, yuki_method_name)
        yuki_method() if yuki_data is None else yuki_method(yuki_data)

    def _on_initialize(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        self._action_decoder = HaruhiActionDecoder(self)
        self._raise("YUKI.N > register action object", self)
        self._on_initialize()
