

class HaruhiSettigsDispatch:

    SETTING_GROUP = "define settings group here."
    SETTING_KEY = "define settings key here."
    SETTING_DEFAULT = "define default value here"

    def _get_current_settings(self):
        yuki_query = self.SETTING_GROUP, self.SETTING_KEY, self.SETTING_DEFAULT
        return self._enquiry("YUKI.N > settings", yuki_query)

    def _on_settings_changed(self, value):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        yuki_group, yuki_key, yuki_value = user_data
        if yuki_group == self.SETTING_GROUP and yuki_key == self.SETTING_KEY:
            self._on_settings_changed(yuki_value)

    def _bind_settings_dispatch(self):
        self._raise("YUKI.N > register settings object", self)
