
from gi.repository import GLib
from gi.repository import WebKit2
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit


class NagatoPreview(WebKit2.WebView, NagatoObject):

    def load_path_async(self, path):
        GLib.idle_add(self.load_uri, GLib.filename_to_uri(path))

    def _on_button_press(self, web_view, event):
        if event.button == 3:
            return True

    def __init__(self, parent):
        self._parent = parent
        WebKit2.WebView.__init__(self)
        self.connect("button-press-event", self._on_button_press)
        yuki_settings = WebKit2.Settings.new()
        yuki_settings.set_default_charset("utf-8")
        yuki_context = self.get_context()
        yuki_context.set_cache_model(WebKit2.CacheModel.DOCUMENT_VIEWER)
        self.set_settings(yuki_settings)
        self.set_size_request(Unit(30), Unit(20))
        self._raise("YUKI.N > add to stack named", (self, "preview"))
