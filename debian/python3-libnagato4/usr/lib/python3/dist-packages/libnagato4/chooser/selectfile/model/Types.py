
# NOTE:
# int should convert to GObject.TYPE_INT not as python int

from gi.repository import Gio
from gi.repository import GObject

TYPES = Gio.Icon, str, str, str, str, bool, int, str, GObject.TYPE_LONG, str

GIO_ICON = 0
FULLPATH = 1
DISPLAY_NAME = 2
SORT_NAME = 3
CONTENT_TYPE = 4
IS_DIRECTORY = 5
AGE = 6
AGE_READABLE = 7
SIZE = 8
SIZE_READABLE = 9
