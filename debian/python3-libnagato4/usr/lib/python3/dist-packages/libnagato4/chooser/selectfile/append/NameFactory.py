
from gi.repository import GLib


def make(directory, is_directory, file_info):
    yuki_header = "DIRECTORY_" if is_directory else "FILE_"
    yuki_display_name = file_info.get_display_name()
    yuki_sort_name = GLib.utf8_strup(yuki_header+yuki_display_name, -1)
    yuki_fullpath = GLib.build_filenamev([directory, yuki_display_name])
    return yuki_fullpath, yuki_display_name, yuki_sort_name
