
from libnagato4.Object import NagatoObject


class AbstractContext(NagatoObject):

    def __getitem__(self, key):
        return self._props[key]

    def __setitem__(self, key, value):
        self._props[key] = value

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        self._props = {}
        self._on_initialize()
