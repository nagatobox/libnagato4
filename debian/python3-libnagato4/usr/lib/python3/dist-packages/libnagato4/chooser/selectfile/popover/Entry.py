
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagato4.widget import Margin


class NagatoEntry(Gtk.Entry, NagatoObject):

    def _on_changed(self, *args):
        self._raise("YUKI.N > changed", self.get_text())

    def _on_activate(self, *args):
        self._raise("YUKI.N > try make directory")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(self)
        self.props.placeholder_text = "Input Directory Name Here."
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self.set_size_request(Unit(40), Unit(2))
        self.connect("activate", self._on_activate)
        self.connect("changed", self._on_changed)
        self._raise("YUKI.N > add to box", self)
