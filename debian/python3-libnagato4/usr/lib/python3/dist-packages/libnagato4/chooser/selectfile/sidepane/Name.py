
from gi.repository import Gtk
from gi.repository import Pango
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.sidepane import Types
from libnagato4.treeview.renderer.Text import HaruhiRenderer


class NagatoName(Gtk.TreeViewColumn, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer.new(Pango.EllipsizeMode.MIDDLE)
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=yuki_renderer,
            text=Types.BASENAME
            )
        self.set_expand(True)
        self._raise("YUKI.N > insert column", self)
