
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.model.Initializer import NagatoInitializer
from libnagato4.chooser.selectfile.model.Observer import NagatoObserver


class NagatoObservers(NagatoObject):

    def receive_transmission(self, directory):
        self._raise("YUKI.N > set loaded", False)
        self._raise("YUKI.N > clear model")
        self._initializer.move_directory(directory)
        self._observer.move_directory(directory)
        self._raise("YUKI.N > set loaded", True)

    def __init__(self, parent):
        self._parent = parent
        self._initializer = NagatoInitializer(self)
        self._observer = NagatoObserver(self)
        self._raise("YUKI.N > register navigation object", self)
