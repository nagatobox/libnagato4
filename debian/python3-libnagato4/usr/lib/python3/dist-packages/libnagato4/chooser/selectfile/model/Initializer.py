
from gi.repository import Gio
from libnagato4.Object import NagatoObject


class NagatoInitializer(NagatoObject):

    def move_directory(self, directory):
        yuki_gio_file = Gio.File.new_for_path(directory)
        for yuki_file_info in yuki_gio_file.enumerate_children("*", 0):
            if yuki_file_info.get_attribute_boolean("access::can-read"):
                self._raise("YUKI.N > append", (directory, yuki_file_info))

    def __init__(self, parent):
        self._parent = parent
