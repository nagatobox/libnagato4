
from libnagato4.Object import NagatoObject
from libnagato4.chooser.colorscheme import ColorDefault
from libnagato4.chooser.colorscheme.ColorBox import NagatoColorBox
from libnagato4.chooser.colorscheme.ColorRGBA import NagatoColorRGBA


class NagatoColorConfig(NagatoObject):

    def _yuki_n_rgba_changed(self, rgba_text):
        self._rgba.set_color(rgba_text)

    def _inform_rgba(self):
        return self._rgba.RGBA

    def _inform_color_config(self, key):
        return self._config[key]

    def __init__(self, parent, key):
        self._parent = parent
        self._rgba = NagatoColorRGBA(self, key)
        self._config = {"key": key, "title": ColorDefault.TITLES[key]}
        NagatoColorBox(self)
