
from gi.repository import Gio
from libnagato4.Object import NagatoObject

DISPATCH = {
    Gio.FileMonitorEvent.MOVED_IN: "_created",
    Gio.FileMonitorEvent.CREATED: "_created",
    Gio.FileMonitorEvent.RENAMED: "_created"
    }


class NagatoApendEvent(NagatoObject):

    def _created(self, directory, file_alfa, file_bravo=None):
        if file_alfa.query_exists(None):
            yuki_file_info = file_alfa.query_info("*", 0)
            self._raise("YUKI.N > append", (directory, yuki_file_info))

    def dispatch(self, directory, file_alfa, file_bravo, event):
        if event in DISPATCH:
            yuki_method = getattr(self, DISPATCH[event])
            yuki_method(directory, file_alfa, file_bravo)

    def __init__(self, parent):
        self._parent = parent
