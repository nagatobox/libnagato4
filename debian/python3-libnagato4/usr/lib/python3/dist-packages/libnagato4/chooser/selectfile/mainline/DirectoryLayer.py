
from libnagato4.Object import NagatoObject
from libnagato4.Transmitter import HaruhiTransmitter
from libnagato4.chooser.selectfile.mainline.DirectorySelection import (
    NagatoDirectorySelection
    )
from libnagato4.chooser.selectfile.mainline.BaseModelLayer import (
    NagatoBaseModelLayer
    )


class NagatoDirectoryLayer(NagatoObject):

    def _yuki_n_directory_moved(self, directory):
        self._transmitter.transmit(directory)

    def _yuki_n_register_navigation_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = HaruhiTransmitter()
        NagatoDirectorySelection.try_new(self)
        NagatoBaseModelLayer(self)
        yuki_directory = self._enquiry("YUKI.N > chooser config", "directory")
        self._transmitter.transmit(yuki_directory)
