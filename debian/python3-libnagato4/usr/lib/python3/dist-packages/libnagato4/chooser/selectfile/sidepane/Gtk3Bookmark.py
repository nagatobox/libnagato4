
from gi.repository import Gio
from gi.repository import GLib

NAMES = [GLib.get_user_config_dir(), "gtk-3.0", "bookmarks"]
PATH = GLib.build_filenamev(NAMES)


def _read_line():
    yuki_gio_file = Gio.File.new_for_path(PATH)
    yuki_stream = Gio.DataInputStream.new(yuki_gio_file.read(None))
    while True:
        yuki_line, yuki_length = yuki_stream.read_line_utf8(None)
        if yuki_line is None:
            break
        yield yuki_line


def read():
    yuki_list = []
    for yuki_line in _read_line():
        yuki_data = yuki_line.split(" ", 1)
        yuki_path, _ = GLib.filename_from_uri(yuki_data[0])
        if yuki_path not in yuki_list:
            yuki_list.append(yuki_path)
    return yuki_list.copy()
