
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.header.Popover import NagatoPopover

ICON_NAME = "view-more-symbolic"
ICON_SIZE = Gtk.IconSize.LARGE_TOOLBAR


class NagatoMenuButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, button):
        self._popover.popup_for_directory(self, self._directory)

    def receive_transmission(self, directory):
        self._directory = directory

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        yuki_image = Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE)
        self.set_image(yuki_image)
        self.props.tooltip_text = "Menu"
        self._popover = NagatoPopover(self)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > css", (self, "toolbar-button"))
        self._raise("YUKI.N > register navigation object", self)
