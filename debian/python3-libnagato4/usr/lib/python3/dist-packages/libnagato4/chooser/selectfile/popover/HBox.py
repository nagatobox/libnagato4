
from libnagato4.widget.HBox import AbstractHBox
from libnagato4.chooser.selectfile.popover.Activatable import NagatoActivatable
from libnagato4.chooser.selectfile.popover.Entry import NagatoEntry
from libnagato4.chooser.selectfile.popover.Button import NagatoButton


class NagatoHBox(AbstractHBox):

    def _yuki_n_try_make_directory(self):
        self._activatable.make_directry()

    def _yuki_n_created(self):
        self._entry.set_text("")
        self._raise("YUKI.N > popdown")

    def _yuki_n_changed(self, text):
        yuki_activatable = self._activatable.get_activatable(text)
        self._button.set_sensitive(yuki_activatable)

    def _on_initialize(self):
        self._activatable = NagatoActivatable(self)
        self._entry = NagatoEntry(self)
        self._button = NagatoButton(self)
        self._raise("YUKI.N > add to box", self)
