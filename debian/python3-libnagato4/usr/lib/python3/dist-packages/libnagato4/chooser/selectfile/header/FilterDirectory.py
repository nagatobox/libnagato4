
from gi.repository import Gio
from libnagato4.Object import NagatoObject


class NagatoFilterDirectory(NagatoObject):

    def _get_cannonical(self, path):
        yuki_gio_file = Gio.File.new_for_path(path)
        return yuki_gio_file.get_path()

    def try_move_directory(self, directory):
        yuki_directory = self._get_cannonical(directory)
        if yuki_directory != self._directory:
            self._raise("YUKI.N > directory moved", yuki_directory)
        self._raise("YUKI.N > new filter", "")

    def set_directory(self, directory):
        self._directory = self._get_cannonical(directory)
        self._raise("YUKI.N > new filter", "")

    def matches(self, directory):
        yuki_directory = self._get_cannonical(directory)
        return yuki_directory == self._directory

    def __init__(self, parent):
        self._parent = parent
        self._directory = ""
