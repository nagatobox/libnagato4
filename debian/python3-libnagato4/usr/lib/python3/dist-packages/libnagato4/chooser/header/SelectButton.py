
from gi.repository import Gtk
from libnagato4.dialog.Button import AbstractButton
from libnagato4.Ux import Unit
from libnagato4.widget import Margin

HEIGHT = Unit(6)


class NagatoSelectButton(AbstractButton):

    TITLE = "Select"
    CSS = "button-safe"

    def _on_clicked(self, button):
        self._raise("YUKI.N > response", Gtk.ResponseType.APPLY)

    def receive_transmission(self, user_data):
        yuki_query = "YUKI.N > chooser config", "selectable"
        yuki_selectable = self._enquiry(*yuki_query)
        self.set_sensitive(yuki_selectable)

    def _on_initialize(self):
        self.set_size_request(-1, HEIGHT)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self.set_sensitive(False)
        self._raise("YUKI.N > register selection object", self)
