
from libnagato4.chooser.selectfile.column.Icon import NagatoIcon
from libnagato4.chooser.selectfile.column.Name import NagatoName
from libnagato4.chooser.selectfile.column.Size import NagatoSize
from libnagato4.chooser.selectfile.column.Type import NagatoType
from libnagato4.chooser.selectfile.column.Age import NagatoAge


class AsakuraColumns:

    def __init__(self, parent):
        NagatoIcon(parent)
        NagatoName(parent)
        NagatoSize(parent)
        NagatoType(parent)
        NagatoAge(parent)
