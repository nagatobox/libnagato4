
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.model import Types
from libnagato4.chooser.selectfile.filter.Mime import NagatoMime
from libnagato4.chooser.selectfile.filter.Keywords import NagatoKeywords


class NagatoFilters(NagatoObject):

    def matches(self, tree_row):
        if not self._mime.matches(tree_row[Types.CONTENT_TYPE]):
            return False
        return self._keywords.matches(tree_row[Types.DISPLAY_NAME])

    def set_filter(self, filter_):
        self._keywords.set_keywrds(filter_)

    def __init__(self, parent):
        self._parent = parent
        self._mime = NagatoMime(self)
        self._keywords = NagatoKeywords(self)
