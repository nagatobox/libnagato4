
from libnagato4.file import XdgUserDir
from libnagato4.chooser.context.Context import AbstractContext
from libnagato4.chooser.context.MimeFilter import HaruhiMimeFilter


class AbstractOpenFile(AbstractContext):

    def _get_directory(self):
        yuki_query = "YUKI.N > data", "default-directory"
        return getattr(XdgUserDir, self._enquiry(*yuki_query))

    def select(self, selection):
        yuki_valid_selection = self._mime_filter.filter_for_paths(selection)
        self._props["selection"] = yuki_valid_selection
        self._props["selectable"] = len(yuki_valid_selection) > 0

    def _initialize_properties(self):
        yuki_mime_type = self._enquiry("YUKI.N > data", "mime-types")
        self._mime_filter = HaruhiMimeFilter(yuki_mime_type)
        self._props["mime-types"] = yuki_mime_type
        self._props["directory"] = XdgUserDir.DOCUMENTS
