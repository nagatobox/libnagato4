

class HaruhiAttributes:

    def _set_dialog_attributes(self, dialog):
        dialog.set_decorated(False)
        dialog.set_skip_taskbar_hint(True)
        dialog.set_skip_pager_hint(True)
        dialog.set_keep_above(True)

    def __init__(self, dialog):
        self._set_dialog_attributes(dialog)
        yuki_content_area = dialog.get_content_area()
        yuki_content_area.set_border_width(0)
