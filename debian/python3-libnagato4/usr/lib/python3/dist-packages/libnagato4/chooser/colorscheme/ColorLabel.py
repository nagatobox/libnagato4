
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoColorLabel(Gtk.Label, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_title = self._enquiry("YUKI.N > color config", "title")
        Gtk.Label.__init__(self, yuki_title)
        self.set_hexpand(True)
        self._raise("YUKI.N > add to box", self)
