
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.model import Types
from libnagato4.treeview.renderer.Icon import HaruhiRenderer
from libnagato4.treeview.column.Zebra import HaruhiZebra


class NagatoIcon(Gtk.TreeViewColumn, NagatoObject, HaruhiZebra):

    def _addtional_data_func(self, renderer, tree_row):
        renderer.set_property("gicon", tree_row[Types.GIO_ICON])

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer()
        Gtk.TreeViewColumn.__init__(
            self,
            title="",
            cell_renderer=yuki_renderer
            )
        self._raise("YUKI.N > insert column", self)
        self._bind_zebra_column(yuki_renderer)
