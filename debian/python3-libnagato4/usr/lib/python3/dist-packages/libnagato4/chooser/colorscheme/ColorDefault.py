
COLORS = {
    "primary_fg_color": "White",
    "primary_bg_color": "MediumSlateBlue",
    "secondary_fg_color": "White",
    "secondary_bg_color": "#60728B",
    "highlight_fg_color": "Whte",
    "highlight_bg_color": "Orange"
}

TITLES = {
    "primary_fg_color": "Primary FG Color",
    "primary_bg_color": "Primary BG Color",
    "secondary_fg_color": "Secondary FG Color",
    "secondary_bg_color": "Secandary BG Color",
    "highlight_fg_color": "Highlight FG Color",
    "highlight_bg_color": "Highlight BG Color"
}
