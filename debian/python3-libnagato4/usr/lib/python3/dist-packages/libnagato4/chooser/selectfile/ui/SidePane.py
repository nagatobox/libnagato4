
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.sidepane.Stack import NagatoStack
from libnagato4.chooser.selectfile.sidepane.Revealer import NagatoRevealer


class NagatoSidePane(Gtk.Box, NagatoObject):

    def _yuki_n_preview_shown(self):
        self._revealer.set_reveal_child(True)

    def _yuki_n_back_to_bookmark(self):
        self._revealer.set_reveal_child(False)
        self._stack.back_to_bookmark()

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def show_preview(self, fullpath):
        self._stack.show_preview(fullpath)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._revealer = NagatoRevealer(self)
        self._stack = NagatoStack(self)
        self._raise("YUKI.N > add to paned left", self)
