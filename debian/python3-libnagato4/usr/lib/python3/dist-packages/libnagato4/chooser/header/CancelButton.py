
from gi.repository import Gtk
from libnagato4.dialog.Button import AbstractButton
from libnagato4.Ux import Unit
from libnagato4.widget import Margin

HEIGHT = Unit(6)


class NagatoCancelButton(AbstractButton):

    TITLE = "Cancel"
    CSS = "button-safe"

    def _on_clicked(self, button):
        self._raise("YUKI.N > response", Gtk.ResponseType.CANCEL)

    def _on_initialize(self):
        self.set_size_request(-1, HEIGHT)
        Margin.set_with_unit(self, 1, 1, 1, 1)
