
from gi.repository import Gtk
from gi.repository import GLib
from libnagato4.Object import NagatoObject

ICON_NAME = "go-up-symbolic"
ICON_SIZE = Gtk.IconSize.LARGE_TOOLBAR


class NagatoUpButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, *args):
        self._raise("YUKI.N > directory moved", self._directory)

    def receive_transmission(self, directory):
        if directory == "/":
            self.set_sensitive(False)
        else:
            self.set_sensitive(True)
            self._directory = GLib.path_get_dirname(directory)
            self.props.tooltip_text = "Up to "+self._directory

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE)
        yuki_image = Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE)
        self.set_image(yuki_image)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
        self._raise("YUKI.N > register navigation object", self)
