
from gi.repository import Gtk
from libnagato4.chooser.Dialog import AbstractDialog
from libnagato4.chooser.font.FontChooser import NagatoFontChooser


class NagatoDialog(AbstractDialog):

    @classmethod
    def run_with_context(cls, parent, context):
        yuki_dialog = NagatoDialog(parent)
        yuki_response = yuki_dialog.get_response(context)
        yuki_dialog.destroy()
        return yuki_response

    def _yuki_n_loopback_add_chooser_widget(self, parent):
        self._font_chooser = NagatoFontChooser(parent)

    def _yuki_n_response(self, response):
        if response == Gtk.ResponseType.APPLY:
            self._font_chooser.set_selection()
        self.response(response)
