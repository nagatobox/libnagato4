
from libnagato4.Ux import Unit
from libnagato4.widget.HBox import AbstractHBox
from libnagato4.chooser.selectfile.filename.Label import NagatoLabel
from libnagato4.chooser.selectfile.filename.Entry import NagatoEntry


class NagatoBox(AbstractHBox):

    def _on_initialize(self):
        NagatoLabel(self)
        NagatoEntry(self)
        self.set_margin_bottom(Unit(1))
        self.set_size_request(-1, Unit(5))
        self._raise("YUKI.N > css", (self, "toolbar"))
        self._raise("YUKI.N > add to box", self)
