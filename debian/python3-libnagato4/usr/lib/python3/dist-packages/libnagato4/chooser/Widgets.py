
from libnagato4.Object import NagatoObject
from libnagato4.chooser.header.EventBox import NagatoEventBox


class NagatoWidgets(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        NagatoEventBox(self)
        self._raise("YUKI.N > loopback add chooser widget", self)
