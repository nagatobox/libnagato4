
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.filename import Validation


class NagatoEvent(NagatoObject):

    def _check_validation(self, user_data=None):
        yuki_data = self._directory, self._enquiry("YUKI.N > entry text")
        yuki_gio_files = Validation.get_valid_files(*yuki_data)
        self._raise("YUKI.N > selection changed", yuki_gio_files)

    def receive_transmission(self, directory):
        self._directory = directory
        self._check_validation()

    def __init__(self, parent):
        self._parent = parent
        yuki_entry = self._enquiry("YUKI.N > entry")
        yuki_entry.connect("changed", self._check_validation)
        self._raise("YUKI.N > register navigation object", self)
