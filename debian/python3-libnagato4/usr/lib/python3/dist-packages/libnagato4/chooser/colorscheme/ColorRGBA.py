
from gi.repository import Gdk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.colorscheme import ColorDefault


class NagatoColorRGBA(NagatoObject):

    def set_color(self, color):
        self._rgba.parse(color)

    def __init__(self, parent, key):
        self._parent = parent
        yuki_query = "css", key, ColorDefault.COLORS[key]
        yuki_color = self._enquiry("YUKI.N > settings", yuki_query)
        self._rgba = Gdk.RGBA()
        self._rgba.parse(yuki_color)

    @property
    def RGBA(self):
        return self._rgba
