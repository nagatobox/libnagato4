
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagato4.widget import Margin
from libnagato4.chooser.context.Color import NagatoContext
from libnagato4.chooser.color.Dialog import NagatoDialog


class NagatoColorButton(Gtk.Button, NagatoObject):

    def _on_clicked(self, *args):
        yuki_rgba = self._enquiry("YUKI.N > rgba")
        yuki_context = NagatoContext.new(self, "Select Color", yuki_rgba)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response == Gtk.ResponseType.APPLY:
            yuki_rgba = yuki_context.get_rgba_string()
            self._raise("YUKI.N > rgba changed", yuki_rgba)
            yuki_key = self._enquiry("YUKI.N > color config", "key")
            self._raise("YUKI.N > selection changed", (yuki_key, yuki_rgba))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        self.connect("clicked", self._on_clicked)
        self.set_size_request(Unit(8), -1)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self._raise("YUKI.N > add to box", self)
