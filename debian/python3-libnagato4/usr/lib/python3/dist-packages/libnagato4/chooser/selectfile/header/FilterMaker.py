
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.header.FilterDirectory import (
    NagatoFilterDirectory
    )


class NagatoFilterMaker(NagatoObject):

    def _make(self, text, filter_):
        yuki_matches = self._directory.matches(GLib.path_get_dirname(text))
        yuki_filter = filter_ if yuki_matches else ""
        self._raise("YUKI.N > new filter", yuki_filter)

    def set_directory(self, directory):
        self._directory.set_directory(directory)

    def make(self, text):
        yuki_filter = GLib.path_get_basename(text)
        yuki_has_filter = not yuki_filter.startswith(".")
        if GLib.file_test(text, GLib.FileTest.IS_DIR) and yuki_has_filter:
            self._directory.try_move_directory(text)
        else:
            self._make(text, yuki_filter)

    def __init__(self, parent):
        self._parent = parent
        self._directory = NagatoFilterDirectory(self)
