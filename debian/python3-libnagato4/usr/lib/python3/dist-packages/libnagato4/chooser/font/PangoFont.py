
from gi.repository import Pango

TEMPLATE = '{} {} {}px "{}","Sans Serif Mono"'
STYLES = {0: "Normal", 1: "Oblique", 2: "Italic"}


def to_css(pango_description):
    yuki_style = STYLES[int(pango_description.get_style())]
    yuki_weight = str(int(pango_description.get_weight()))
    # convert point to px.
    yuki_size = int(pango_description.get_size())/Pango.SCALE*(96/72)
    yuki_family = pango_description.get_family()
    return TEMPLATE.format(yuki_style, yuki_weight, yuki_size, yuki_family)
