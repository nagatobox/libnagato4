
from libnagato4.Object import NagatoObject
from libnagato4.chooser import Types


class NagatoDirectorySelection(NagatoObject):

    @classmethod
    def try_new(cls, parent):
        yuki_type = parent._enquiry("YUKI.N > chooser config", "type")
        if yuki_type == Types.SELECT_DIRECTORY:
            cls(parent)

    def receive_transmission(self, directory):
        self._raise("YUKI.N > selection changed", directory)

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register navigation object", self)
