
from gi.repository import Gtk
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.sidepane import Types
from libnagato4.treeview.renderer.Icon import HaruhiRenderer


class NagatoIcon(Gtk.TreeViewColumn, NagatoObject):

    def _cell_data_func(self, column, renderer, model, tree_iter, user_data):
        renderer.set_property("height", Unit(5))
        yuki_name = model[tree_iter][Types.ICON_NAME]
        renderer.set_property("icon-name", yuki_name)

    def __init__(self, parent):
        self._parent = parent
        yuki_renderer = HaruhiRenderer()
        Gtk.TreeViewColumn.__init__(self, cell_renderer=yuki_renderer)
        self._raise("YUKI.N > insert column", self)
        self.set_cell_data_func(yuki_renderer, self._cell_data_func)
