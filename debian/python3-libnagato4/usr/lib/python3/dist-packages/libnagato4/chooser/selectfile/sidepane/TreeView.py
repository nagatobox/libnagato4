
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.chooser.selectfile.sidepane.Icon import NagatoIcon
from libnagato4.chooser.selectfile.sidepane.Name import NagatoName


class NagatoTreeView(Gtk.TreeView, NagatoObject):

    def _yuki_n_insert_column(self, column):
        self.append_column(column)

    def _on_changed(self, tree_selection):
        yuki_model, yuki_tree_paths = tree_selection.get_selected_rows()
        yuki_tree_path = yuki_tree_paths[0]
        yuki_row = yuki_model[yuki_tree_path]
        self._raise("YUKI.N > directory moved", yuki_row[2])

    def __init__(self, parent):
        self._parent = parent
        Gtk.TreeView.__init__(self, model=self._enquiry("YUKI.N > model"))
        self.set_headers_visible(False)
        self.set_can_focus(False)
        self.set_tooltip_column(2)
        yuki_selection = self.get_selection()
        yuki_selection.connect("changed", self._on_changed)
        NagatoIcon(self)
        NagatoName(self)
        self._raise("YUKI.N > css", (self, "bookmark-treeview"))
        self._raise("YUKI.N > add to stack named", (self, "bookmark"))
