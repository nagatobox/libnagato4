
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagato4.chooser.colorscheme.ColorLabel import NagatoColorLabel
from libnagato4.chooser.colorscheme.ColorButton import NagatoColorButton
from libnagato4.chooser.colorscheme.ColorImage import NagatoColorImage


class NagatoColorBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1, Unit(8))
        self._raise("YUKI.N > add to box", self)
        NagatoColorLabel(self)
        yuki_button = NagatoColorButton(self)
        yuki_button.set_image(NagatoColorImage(self))
