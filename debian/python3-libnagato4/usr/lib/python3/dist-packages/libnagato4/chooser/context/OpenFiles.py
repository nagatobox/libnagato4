
from gi.repository import Gtk
from libnagato4.chooser import Types
from libnagato4.chooser.context.AnyOpenFile import AbstractOpenFile


class NagatoContext(AbstractOpenFile):

    def _on_initialize(self):
        self._initialize_properties()
        self._props["selectable"] = False
        self._props["title"] = "Select Files"
        self._props["type"] = Types.SELECT_FILES
        self._props["selection"] = []
        self._props["selection-mode"] = Gtk.SelectionMode.MULTIPLE
