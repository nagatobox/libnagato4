
from gi.repository import Gio
from gi.repository import GLib

ATTRIBUTE = "standard::content-type"
FLAG = Gio.FileQueryInfoFlags.NONE


class HaruhiMimeFilter:

    def _check_mime_type(self, gio_file):
        yuki_file_info = gio_file.query_info(ATTRIBUTE, FLAG)
        yuki_mime_type = yuki_file_info.get_attribute_as_string(ATTRIBUTE)
        for yuki_filter in self._mime_types.values():
            if GLib.regex_match_simple(yuki_filter, yuki_mime_type, 0, 0):
                self._list.append(gio_file.get_path())
                break

    def filter_for_paths(self, paths):
        self._list.clear()
        for yuki_path in paths:
            yuki_gio_file = Gio.File.new_for_path(yuki_path)
            if yuki_gio_file.query_exists(None):
                self._check_mime_type(yuki_gio_file)
        return self._list.copy()

    def filter(self, gio_files):
        self._list.clear()
        for yuki_gio_file in gio_files:
            if yuki_gio_file.query_exists(None):
                self._check_mime_type(yuki_gio_file)
        return self._list.copy()

    def __init__(self, mime_types):
        self._mime_types = mime_types
        self._list = []
