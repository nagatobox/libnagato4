
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.dialog.quit.Quit import NagatoQuit


class NagatoMainLoopCloser(NagatoObject):

    def force_close(self):
        Gtk.main_quit()

    def close_for_dialog_respose(self):
        if NagatoQuit.get_response(self) != Gtk.ResponseType.CLOSE:
            return True
        Gtk.main_quit()

    def __init__(self, parent):
        self._parent = parent
