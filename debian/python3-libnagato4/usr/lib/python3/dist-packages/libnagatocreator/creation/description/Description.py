
from libnagatocreator.creation.description.Short import NagatoShort
from libnagatocreator.creation.description.Long import NagatoLong
from libnagatocreator.creation.description.LongTab import NagatoLongTab


class AsakuraDescription:

    def __init__(self, parent):
        NagatoShort(parent)
        NagatoLong(parent)
        NagatoLongTab(parent)
