
from gi.repository import GLib
from libnagatocreator.creation.replacement.Replacement import (
    AbstractReplacement
    )

FORMART = "{}, %d {} %Y"
WEEK = ["error", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
MONTH = [
    "error",
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ]


class NagatoDateFull(AbstractReplacement):

    def _get_old(self):
        return "RELEASE_DATE_FULL"

    def _get_new(self):
        yuki_date_time = GLib.DateTime.new_now_local()
        yuki_week = WEEK[yuki_date_time.get_day_of_week()]
        yuki_month = MONTH[yuki_date_time.get_month()]
        yuki_format = FORMART.format(yuki_week, yuki_month)
        return yuki_date_time.format(yuki_format)
