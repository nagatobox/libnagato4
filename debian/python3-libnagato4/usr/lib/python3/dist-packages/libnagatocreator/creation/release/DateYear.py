
from gi.repository import GLib
from libnagatocreator.creation.replacement.Replacement import (
    AbstractReplacement
    )


class NagatoDateYear(AbstractReplacement):

    def _get_old(self):
        return "RELEASE_DATE_YEAR"

    def _get_new(self):
        yuki_date_time = GLib.DateTime.new_now_local()
        return str(yuki_date_time.get_year())
