
from gi.repository import Gio
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatocreator.creation.replacement.Text import NagatoReplacements


class NagatoText(NagatoObject):

    def _get_mode(self, target_path):
        if target_path.endswith("debian/rules"):
            return int("0777", 8)
        else:
            return int("0666", 8)

    def set_file(self, source_path, target_path):
        _, yuki_contents = GLib.file_get_contents(source_path)
        yuki_string = yuki_contents.decode("utf-8")
        for yuki_old, yuki_new in self._replacements:
            yuki_string = yuki_string.replace(yuki_old, yuki_new)
        yuki_content = yuki_string.encode("utf-8")
        yuki_mode = self._get_mode(target_path)
        GLib.file_set_contents_full(target_path, yuki_content, yuki_mode)

    def __init__(self, parent):
        self._parent = parent
        self._replacements = NagatoReplacements(self)
