
from libnagatocreator.creation.author.Name import NagatoName
from libnagatocreator.creation.author.MailAddress import NagatoMailAddress


class AsakuraAuthor:

    def __init__(self, parent):
        NagatoName(parent)
        NagatoMailAddress(parent)
