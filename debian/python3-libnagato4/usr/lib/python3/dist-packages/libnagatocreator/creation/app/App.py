
from libnagatocreator.creation.app.Name import NagatoName
from libnagatocreator.creation.app.Id import NagatoId
from libnagatocreator.creation.app.WebSite import NagatoWebSite
from libnagatocreator.creation.app.LibraryName import NagatoLibraryName


class AsakuraApp:

    def __init__(self, parent):
        NagatoId(parent)
        NagatoName(parent)
        NagatoWebSite(parent)
        NagatoLibraryName(parent)
