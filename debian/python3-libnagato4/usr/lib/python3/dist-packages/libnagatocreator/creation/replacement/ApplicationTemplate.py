
from libnagato4.Object import NagatoObject
from libnagatocreator.model import Keys


class NagatoApplicationTemplate(NagatoObject):

    def _get_old(self):
        return self._enquiry("YUKI.N > resources", ".application_template")

    def _get_new(self):
        yuki_query = Keys.PAGE_ID_APP, Keys.APP_DIRECTORY
        return self._enquiry("YUKI.N > model data", yuki_query)

    def __init__(self, parent):
        self._parent = parent
        yuki_old = self._get_old()
        yuki_new = self._get_new()
        self._raise("YUKI.N > add replacement", (yuki_old, yuki_new))
