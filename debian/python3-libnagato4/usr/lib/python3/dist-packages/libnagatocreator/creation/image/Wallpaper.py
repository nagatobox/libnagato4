
from gi.repository import Gio
from gi.repository import GLib
from libnagatocreator.model import Keys
from libnagatocreator.creation.image.Copy import AbstractCopy


class NagatoWallpaper(AbstractCopy):

    SOURCE_KEY = Keys.VISUAL_WALLPAPER_URI

    def _get_child_path(self):
        yuki_query = Keys.PAGE_ID_APP, Keys.APP_NAME
        yuki_app_name = self._enquiry("YUKI.N > model data", yuki_query)
        yuki_library = "lib"+yuki_app_name.replace("-", "")
        yuki_names = [yuki_library, "resources", "images", "wallpaper.jpg"]
        return GLib.build_filenamev(yuki_names)

    def copy(self):
        yuki_target = self._get_target(self._get_child_path())
        yuki_source = self._get_source()
        yuki_source.copy(yuki_target, Gio.FileCopyFlags.NONE)
