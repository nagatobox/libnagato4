
from libnagatocreator.model import Keys
from libnagatocreator.creation.replacement.Replacement import (
    AbstractReplacement
    )


class NagatoMailAddress(AbstractReplacement):

    def _get_old(self):
        return "AUTHOR_MAIL_ADDRESS"

    def _get_new(self):
        yuki_query = Keys.PAGE_ID_AUTHOR, Keys.AUTHOR_MAIL_ADDRESS
        return self._enquiry("YUKI.N > model data", yuki_query)
