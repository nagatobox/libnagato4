
from libnagato4.Object import NagatoObject
from libnagatocreator.creation.directory.Directory import NagatoDirectory
from libnagatocreator.creation.text.Text import NagatoText
from libnagatocreator.creation.image.Image import NagatoImage


class NagatoCreate(NagatoObject):

    def _yuki_n_text_file_found(self, user_data):
        yuki_target_directory, yuki_source_path = user_data
        if yuki_source_path.endswith("pyc"):
            return
        self._text.set_file(yuki_target_directory, yuki_source_path)

    def create_project(self):
        yuki_name = ".application_template"
        yuki_prototype_root = self._enquiry("YUKI.N > resources", yuki_name)
        self._directory.parse(yuki_prototype_root)
        self._image.copy()

    def __init__(self, parent):
        self._parent = parent
        self._directory = NagatoDirectory(self)
        self._text = NagatoText(self)
        self._image = NagatoImage(self)
