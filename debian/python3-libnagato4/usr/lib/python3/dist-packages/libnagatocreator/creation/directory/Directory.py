
from gi.repository import Gio
from libnagato4.Object import NagatoObject
from libnagatocreator.creation.directory.Path import NagatoPath


class NagatoDirectory(NagatoObject):

    def _dispatch(self, source_dir, file_info):
        yuki_targets = self._path.get_targets(source_dir, file_info)
        yuki_source_path, yuki_target_path = yuki_targets
        yuki_content_type = file_info.get_content_type()
        if yuki_content_type == "inode/directory":
            yuki_gio_file = Gio.File.new_for_path(yuki_target_path)
            yuki_gio_file.make_directory()
            self.parse(yuki_source_path)
        elif not yuki_content_type.startswith("image"):
            self._raise("YUKI.N > text file found", yuki_targets)

    def parse(self, source_dir):
        yuki_gio_file = Gio.File.new_for_path(source_dir)
        for yuki_file_info in yuki_gio_file.enumerate_children("*", 0):
            self._dispatch(source_dir, yuki_file_info)

    def __init__(self, parent):
        self._parent = parent
        self._path = NagatoPath(self)
