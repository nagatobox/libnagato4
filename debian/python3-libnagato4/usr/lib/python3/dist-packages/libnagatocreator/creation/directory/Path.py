
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatocreator.creation.replacement.Path import NagatoReplacements


class NagatoPath(NagatoObject):

    def _get_source(self, source_directory, file_info):
        yuki_names = [source_directory, file_info.get_display_name()]
        return GLib.build_filenamev(yuki_names)

    def _get_target(self, source_path):
        yuki_target = source_path
        yuki_replacements = NagatoReplacements(self)
        for yuki_old, yuki_new in yuki_replacements:
            yuki_target = yuki_target.replace(yuki_old, yuki_new, 1)
        return yuki_target

    def get_targets(self, source_directory, file_info):
        yuki_source = self._get_source(source_directory, file_info)
        yuki_target = self._get_target(yuki_source)
        return yuki_source, yuki_target

    def __init__(self, parent):
        self._parent = parent
