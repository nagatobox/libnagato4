
from libnagatocreator.creation.app.App import AsakuraApp
from libnagatocreator.creation.author.Author import AsakuraAuthor
from libnagatocreator.creation.description.Description import (
    AsakuraDescription
    )
from libnagatocreator.creation.release.Release import AsakuraRelease
from libnagatocreator.creation.replacement.Replacements import (
    AbstractReplacements
    )


class NagatoReplacements(AbstractReplacements):

    def _on_start_iteration(self):
        AsakuraApp(self)
        AsakuraAuthor(self)
        AsakuraDescription(self)
        AsakuraRelease(self)
