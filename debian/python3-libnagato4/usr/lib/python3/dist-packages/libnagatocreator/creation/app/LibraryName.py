
from libnagatocreator.model import Keys
from libnagatocreator.creation.replacement.Replacement import (
    AbstractReplacement
    )


class NagatoLibraryName(AbstractReplacement):

    def _get_old(self):
        return "LIBRARY_NAME"

    def _get_new(self):
        yuki_query = Keys.PAGE_ID_APP, Keys.APP_NAME
        yuki_app_name = self._enquiry("YUKI.N > model data", yuki_query)
        return "lib"+yuki_app_name.replace("-", "")
