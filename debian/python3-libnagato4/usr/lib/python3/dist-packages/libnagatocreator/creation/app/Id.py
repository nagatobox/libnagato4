
from libnagatocreator.model import Keys
from libnagatocreator.creation.replacement.Replacement import (
    AbstractReplacement
    )


class NagatoId(AbstractReplacement):

    def _get_old(self):
        return "APP_ID"

    def _get_new(self):
        yuki_query = Keys.PAGE_ID_APP, Keys.APP_ID
        return self._enquiry("YUKI.N > model data", yuki_query)
