
from libnagatocreator.model.Author import NagatoAuthor
from libnagatocreator.model.App import NagatoApp
from libnagatocreator.model.Visual import NagatoVisual
from libnagatocreator.model.Description import NagatoDescription


class AsakuraFactory:

    def __init__(self, parent):
        NagatoAuthor(parent)
        NagatoApp(parent)
        NagatoVisual(parent)
        NagatoDescription(parent)
