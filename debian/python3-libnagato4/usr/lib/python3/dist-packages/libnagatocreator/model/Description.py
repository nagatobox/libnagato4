
from libnagatocreator.model import Keys
from libnagatocreator.model.Model import AbstractModel


class NagatoDescription(AbstractModel):

    ID = Keys.PAGE_ID_DESCRIPTION

    def _on_initialize(self):
        self._data[Keys.DESCRIPTION_SHORT] = ""
        self._data[Keys.DESCRIPTION_LONG] = ""
