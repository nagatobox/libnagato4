
from libnagatocreator.model import Keys
from libnagatocreator.model.Model import AbstractModel


class NagatoApp(AbstractModel):

    ID = Keys.PAGE_ID_APP

    def _on_initialize(self):
        self._data[Keys.APP_DIRECTORY] = ""
        self._data[Keys.APP_NAME] = ""
        self._data[Keys.APP_ID] = ""
        self._data[Keys.APP_WEB_SITE] = ""
