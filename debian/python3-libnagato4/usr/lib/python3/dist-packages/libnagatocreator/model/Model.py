
from libnagato4.Object import NagatoObject


class AbstractModel(NagatoObject):

    ID = "YUKI.N > do not look at me."

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value
        yuki_data = self.ID, key, value, self.get_appliable()
        self._raise("YUKI.N > model changed", yuki_data)

    def get_appliable(self):
        return "" not in self._data.values()

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        self._data = {}
        self._raise("YUKI.N > append model", (self.ID, self))
        self._on_initialize()
