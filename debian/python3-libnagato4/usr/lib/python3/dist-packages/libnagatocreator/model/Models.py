
from libnagato4.Object import NagatoObject
from libnagatocreator.model import Keys
from libnagatocreator.model.Factory import AsakuraFactory


class NagatoModels(dict, NagatoObject):

    def set_data(self, user_data):
        yuki_group, yuki_key, yuki_value = user_data
        yuki_model = self[yuki_group]
        yuki_model[yuki_key] = yuki_value

    def get_data(self, user_data):
        yuki_group, yuki_key = user_data
        yuki_model = self[yuki_group]
        return yuki_model[yuki_key]

    def get_appliable(self, group):
        yuki_model = self[group]
        return yuki_model.get_appliable()

    def _yuki_n_append_model(self, user_data):
        yuki_id, yuki_model = user_data
        self[yuki_id] = yuki_model

    def __init__(self, parent):
        self._parent = parent
        AsakuraFactory(self)
