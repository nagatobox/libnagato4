
from libnagato4.Object import NagatoObject
from libnagatocreator.model import Keys
from libnagatocreator.mainline.FileCopyLayer import NagatoFileCopyLayer

GROUP = Keys.PAGE_ID_AUTHOR
KEYS = [
    Keys.AUTHOR_NAME,
    Keys.AUTHOR_MAIL_ADDRESS,
    Keys.AUTHOR_ID_HEADER,
    Keys.AUTHOR_WEB_SITE
    ]


class NagatoSaveLayer(NagatoObject):

    def _yuki_n_save_model_data(self):
        for yuki_key in KEYS:
            yuki_query = GROUP, yuki_key
            yuki_value = self._enquiry("YUKI.N > model data", yuki_query)
            self._raise("YUKI.N > settings", (GROUP, yuki_key, yuki_value))

    def __init__(self, parent):
        self._parent = parent
        NagatoFileCopyLayer(self)
