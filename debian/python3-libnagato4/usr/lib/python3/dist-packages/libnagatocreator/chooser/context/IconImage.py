
from gi.repository import Gtk
from libnagato4.file import XdgUserDir
from libnagato4.chooser import Types
from libnagato4.chooser.context.Context import AbstractContext
from libnagato4.chooser.context.MimeFilter import HaruhiMimeFilter


class NagatoContext(AbstractContext):

    MIME_TYPES = {"SVG Files": "image/svg*"}

    def get_selected(self):
        return self._props["selection"][0]

    def select(self, selection):
        yuki_valid_selection = self._mime_filter.filter_for_paths(selection)
        self._props["selection"] = yuki_valid_selection
        self._props["selectable"] = len(yuki_valid_selection) > 0

    def _on_initialize(self):
        self._props["selectable"] = False
        self._props["title"] = "Select Background Image"
        self._props["type"] = Types.SELECT_FILE
        self._props["selection"] = []
        self._props["selection-mode"] = Gtk.SelectionMode.BROWSE
        self._props["directory"] = XdgUserDir.PICTURES
        self._props["mime-types"] = self.MIME_TYPES
        self._mime_filter = HaruhiMimeFilter(self.MIME_TYPES)
