
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoSpacer(Gtk.Label, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self.set_vexpand(True)
        self._raise("YUKI.N > add to box", self)
