
from gi.repository import Gtk

SIZE = Gtk.IconSize.SMALL_TOOLBAR
ICON_NAME = "folder-symbolic"


class HaruhiButtonImage:

    def __init__(self, button):
        yuki_image = Gtk.Image.new_from_icon_name(ICON_NAME, SIZE)
        button.set_image(yuki_image)
        button.set_image_position(Gtk.PositionType.LEFT)
