
from libnagatocreator.model import Keys
from libnagatocreator.page.app.Entry import AbstractEntry


class NagatoWebSiteEntry(AbstractEntry):

    def _yuki_n_new_basename(self, basename):
        yuki_query = Keys.PAGE_ID_AUTHOR, Keys.AUTHOR_WEB_SITE
        yuki_header = self._enquiry("YUKI.N > model data", yuki_query)
        yuki_text = "{}/{}".format(yuki_header, basename)
        self.set_text(yuki_text)
