
from libnagato4.Object import NagatoObject


class NagatoPathInitializer(NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_page_id = self._enquiry("YUKI.N > page id")
        yuki_default = self._enquiry("YUKI.N > default resource")
        yuki_key = self._enquiry("YUKI.N > default key")
        yuki_path = self._enquiry("YUKI.N > resources", yuki_default)
        yuki_data = yuki_page_id, yuki_key, yuki_path
        self._raise("YUKI.N > model data", yuki_data)
