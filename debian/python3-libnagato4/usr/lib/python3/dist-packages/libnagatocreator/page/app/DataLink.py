
from gi.repository import GLib
from libnagato4.Object import NagatoObject
from libnagatocreator.model import Keys


class NagatoDataLink(NagatoObject):

    def receive_transmission(self, user_data):
        yuki_group, yuki_key, yuki_value, _ = user_data
        if yuki_group == Keys.PAGE_ID_APP and yuki_key == Keys.APP_DIRECTORY:
            yuki_basename = GLib.path_get_basename(yuki_value)
            self._raise("YUKI.N > new basename", yuki_basename)

    def __init__(self, parent):
        self._parent = parent
        self._raise("YUKI.N > register model object", self)
