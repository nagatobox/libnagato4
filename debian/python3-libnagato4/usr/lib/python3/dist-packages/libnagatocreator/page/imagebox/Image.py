
from gi.repository import Gtk
from gi.repository import GdkPixbuf
from libnagato4.Ux import Unit
from libnagato4.Object import NagatoObject
from libnagatocreator.model import Keys

MAX_SIZE = Unit(16)


class NagatoImage(Gtk.Image, NagatoObject):

    def _refresh_image(self, path):
        yuki_args = path, MAX_SIZE, MAX_SIZE, True
        yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(*yuki_args)
        self.set_from_pixbuf(yuki_pixbuf)

    def receive_transmission(self, user_data):
        yuki_group, yuki_key, yuki_value, _ = user_data
        yuki_default_key = self._enquiry("YUKI.N > default key")
        if yuki_group == Keys.PAGE_ID_VISUAL and yuki_key == yuki_default_key:
            self._refresh_image(yuki_value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self)
        self._raise("YUKI.N > add to button", self)
        self._raise("YUKI.N > register model object", self)
