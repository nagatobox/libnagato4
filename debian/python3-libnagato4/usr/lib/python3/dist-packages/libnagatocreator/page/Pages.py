
from libnagatocreator.page.author.AuthorPage import NagatoAuthorPage
from libnagatocreator.page.app.AppPage import NagatoAppPage
from libnagatocreator.page.visual.VisualPage import NagatoVisualPage
from libnagatocreator.page.description.DescriptionPage import (
    NagatoDescriptionPage
    )


class AsakuraPages:

    def __init__(self, parent):
        NagatoAuthorPage(parent)
        NagatoAppPage(parent)
        NagatoVisualPage(parent)
        NagatoDescriptionPage(parent)
