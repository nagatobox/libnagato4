
from libnagatocreator.model import Keys
from libnagatocreator.page.entrybox.Model import AbstractModel


class NagatoWebSiteEntry(AbstractModel):

    CSS = "config-box-odd"
    LABEL_TEXT = "Web site"
    DEFAULT_KEY = Keys.AUTHOR_WEB_SITE
