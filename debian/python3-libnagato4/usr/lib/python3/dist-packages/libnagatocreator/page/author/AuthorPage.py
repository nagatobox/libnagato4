
from libnagatocreator.model import Keys
from libnagatocreator.page.Page import AbstractPage
from libnagatocreator.page.author.HeaderLabel import NagatoHeaderLabel
from libnagatocreator.page.author.NameEntry import NagatoNameEntry
from libnagatocreator.page.author.EmailEntry import NagatoEmailEntry
from libnagatocreator.page.author.WebSiteEntry import NagatoWebSiteEntry
from libnagatocreator.page.author.IdHeaderEntry import NagatoIdHeaderEntry
from libnagatocreator.page.Spacer import NagatoSpacer
from libnagatocreator.page.author.ButtonBox import NagatoButtonBox


class NagatoAuthorPage(AbstractPage):

    PAGE_ID = Keys.PAGE_ID_AUTHOR

    def _yuki_n_loopback_set_widgets(self, parent):
        NagatoHeaderLabel(parent)
        NagatoNameEntry(parent)
        NagatoEmailEntry(parent)
        NagatoWebSiteEntry(parent)
        NagatoIdHeaderEntry(parent)
        NagatoSpacer(parent)
        NagatoButtonBox(parent)
