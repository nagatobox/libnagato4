
from libnagatocreator.page.itembox.Model import AbstractModel as TFEI
from libnagatocreator.page.imagebox.Button import NagatoButton


class AbstractModel(TFEI):

    DEFAULT_RESOURCE = "define name for resource here."

    def _inform_default_resource(self):
        return self.DEFAULT_RESOURCE

    def _yuki_n_image_clicked(self):
        raise NotImplementedError

    def _yuki_n_loopback_set_value_widget(self, parent):
        NagatoButton(parent)
