
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagatocreator.page.imagebox.Image import NagatoImage
from libnagato4.widget import Margin
from libnagatocreator.page.imagebox.PathInitializer import (
    NagatoPathInitializer
    )


class NagatoButton(Gtk.Button, NagatoObject):

    def _yuki_n_add_to_button(self, image):
        self.set_image(image)

    def _on_clicked(self, button):
        self._raise("YUKI.N > image clicked")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self._raise("YUKI.N > css", (self, "action-button"))
        NagatoImage(self)
        NagatoPathInitializer(self)
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > add to box", self)
