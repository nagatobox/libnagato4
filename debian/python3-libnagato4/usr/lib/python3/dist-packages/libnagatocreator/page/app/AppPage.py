
from libnagatocreator.model import Keys
from libnagatocreator.page.Page import AbstractPage
from libnagatocreator.page.app.HeaderLabel import NagatoHeaderLabel
from libnagatocreator.page.app.DirectoryEntry import NagatoDirectoryEntry
from libnagatocreator.page.app.NameBox import NagatoNameBox
from libnagatocreator.page.app.IdBox import NagatoIdBox
from libnagatocreator.page.app.WebSiteBox import NagatoWebSiteBox
from libnagatocreator.page.Spacer import NagatoSpacer
from libnagatocreator.page.app.ButtonBox import NagatoButtonBox


class NagatoAppPage(AbstractPage):

    PAGE_ID = Keys.PAGE_ID_APP

    def _yuki_n_loopback_set_widgets(self, parent):
        NagatoHeaderLabel(parent)
        NagatoDirectoryEntry(parent)
        NagatoNameBox(parent)
        NagatoIdBox(parent)
        NagatoWebSiteBox(parent)
        NagatoSpacer(parent)
        NagatoButtonBox(parent)
