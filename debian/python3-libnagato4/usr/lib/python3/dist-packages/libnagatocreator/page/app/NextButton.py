
from gi.repository import Gtk
from libnagato4.widget import Margin
from libnagato4.Object import NagatoObject
from libnagatocreator.model import Keys
from libnagatocreator.page.button.Appliable import ShamisenAppliable


class NagatoNextButton(Gtk.Button, NagatoObject, ShamisenAppliable):

    def _on_clicked(self, button):
        self._raise("YUKI.N > switch stack to", Keys.PAGE_ID_VISUAL)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, "Next (Visual)")
        self.connect("clicked", self._on_clicked)
        self._raise("YUKI.N > css", (self, "action-button"))
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self._bind_appliable()
        self._raise("YUKI.N > add to box", self)
