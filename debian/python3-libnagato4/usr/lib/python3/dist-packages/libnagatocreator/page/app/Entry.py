
from libnagatocreator.page.itembox.Entry import AbstractEntry as TFEI
from libnagatocreator.page.app.DataLink import NagatoDataLink


class AbstractEntry(TFEI):

    def _yuki_n_new_basename(self, basename):
        raise NotImplementedError

    def _on_initialize(self):
        NagatoDataLink(self)
