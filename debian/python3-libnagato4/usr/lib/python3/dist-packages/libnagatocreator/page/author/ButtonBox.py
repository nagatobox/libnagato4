
from libnagatocreator.page.ButtonBox import AbstractButtonBox
from libnagatocreator.page.ButtonBoxSpacer import NagatoButtonBoxSpacer
from libnagatocreator.page.author.NextButton import NagatoNextButton


class NagatoButtonBox(AbstractButtonBox):

    def _on_initialize(self):
        NagatoButtonBoxSpacer(self)
        NagatoNextButton(self)
