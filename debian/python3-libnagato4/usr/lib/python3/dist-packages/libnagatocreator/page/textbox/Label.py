
from gi.repository import Gtk
from libnagato4.Object import NagatoObject
from libnagato4.Ux import Unit
from libnagato4.widget import Margin


class NagatoLabel(Gtk.Label, NagatoObject):

    def __init__(self, parent):
        self._parent = parent
        yuki_label_text = self._enquiry("YUKI.N > label text")
        Gtk.Label.__init__(self, yuki_label_text)
        self.set_size_request(-1, Unit(3))
        Margin.set_with_unit(self, 1, 1, 1, 1)
        self.set_xalign(0.05)
        self._raise("YUKI.N > add to box", self)
