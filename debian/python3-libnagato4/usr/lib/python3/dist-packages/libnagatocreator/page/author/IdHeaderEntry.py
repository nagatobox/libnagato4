
from libnagatocreator.model import Keys
from libnagatocreator.page.entrybox.Model import AbstractModel


class NagatoIdHeaderEntry(AbstractModel):

    CSS = "config-box-even"
    LABEL_TEXT = "id header"
    DEFAULT_KEY = Keys.AUTHOR_ID_HEADER
