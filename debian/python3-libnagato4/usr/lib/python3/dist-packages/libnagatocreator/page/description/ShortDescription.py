
from libnagatocreator.model import Keys
from libnagatocreator.page.textbox.Model import AbstractModel
from libnagatocreator.page.entrybox.Entry import NagatoEntry


class NagatoShortDescription(AbstractModel):

    CSS = "config-box-odd"
    LABEL_TEXT = "Short Description"
    DEFAULT_KEY = Keys.DESCRIPTION_SHORT

    def _yuki_n_loopback_add_text_widget(self, parent):
        NagatoEntry(parent)
