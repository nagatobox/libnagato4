
from gi.repository import Gtk
from libnagato4.Object import NagatoObject


class NagatoBox(Gtk.Box, NagatoObject):

    def _yuki_n_add_to_box(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.set_hexpand(True)
        self.set_vexpand(True)
        yuki_id = self._enquiry("YUKI.N > page id")
        self._raise("YUKI.N > add named", (self, yuki_id))
        self._raise("YUKI.N > loopback set widgets", self)
