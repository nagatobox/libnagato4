
from gi.repository import Gtk
from libnagato4.chooser.selectfile.Dialog import NagatoDialog
from libnagatocreator.model import Keys
from libnagatocreator.page.imagebox.Model import AbstractModel
from libnagatocreator.chooser.context.IconImage import NagatoContext


class NagatoIconBox(AbstractModel):

    LABEL_TEXT = "Application Icon"
    CSS = "config-box-odd"
    DEFAULT_KEY = Keys.VISUAL_ICON_URI
    DEFAULT_RESOURCE = "images/default_icon.svg"

    def _yuki_n_image_clicked(self):
        yuki_context = NagatoContext(self)
        yuki_response = NagatoDialog.run_with_context(self, yuki_context)
        if yuki_response != Gtk.ResponseType.APPLY:
            return
        yuki_path = yuki_context.get_selected()
        yuki_data = Keys.PAGE_ID_VISUAL, self.DEFAULT_KEY, yuki_path
        self._raise("YUKI.N > model data", yuki_data)
