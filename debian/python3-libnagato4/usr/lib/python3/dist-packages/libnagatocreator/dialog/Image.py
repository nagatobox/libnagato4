
from libnagato4.dialog.Image import AbstractImage
from libnagatocreator.model import Keys


class NagatoImage(AbstractImage):

    def _set_image(self):
        yuki_query = Keys.PAGE_ID_VISUAL, Keys.VISUAL_ICON_URI
        yuki_file = self._enquiry("YUKI.N > model data", yuki_query)
        self.set_from_file(yuki_file)
