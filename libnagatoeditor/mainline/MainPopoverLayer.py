
from libnagato4.mainline.MainPopoverLayer import AbstractMainPopoverLayer
from libnagatoeditor.mainline.HeaderLayer import NagatoHeaderLayer
from libnagatoeditor.popover.MainItems import AsakuraMainItems
from libnagatoeditor.popover.SubGroups import AsakuraSubGroups


class NagatoMainPopoverLayer(AbstractMainPopoverLayer):

    def _yuki_n_loopback_add_popover_item(self, parent_box):
        AsakuraMainItems(parent_box)

    def _yuki_n_loopback_add_popover_group(self, parent_stack):
        AsakuraSubGroups(parent_stack)

    def _on_initialize(self):
        NagatoHeaderLayer(self)
