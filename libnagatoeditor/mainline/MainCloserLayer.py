
from libnagato4.maincloser.MainCloser import AbstractMainCloser
from libnagatoeditor.ui.HeaderNotifyLayer import NagatoHeaderNotifyLayer


class NagatoMainCloserLayer(AbstractMainCloser):

    def _yuki_n_loopback_set_content_closer(self, content_closer):
        self._content_closer = content_closer

    def _yuki_n_confirm_to_close(self):
        self._standard_sequence()

    def _on_delete(self, widget, event):
        if self._content_closer.closable_all():
            return self._standard_sequence()
        else:
            return self._content_closer.try_close_all()

    def _on_initialize(self):
        NagatoHeaderNotifyLayer(self)
